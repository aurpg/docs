---
title: "ORIANA"
url: "orianas"
group: "1st Age"
order: 1100
---
# ORIANNA

The Haula'fin are a proud, noble race of peoples scattered across the world with numerous cultures and bloodlines. They're well known for some of the greatest discoveries in history, like **soup**, and the coveted origins of **bacon**.

And then, there's the **Darukai**. Mighty beast men that come from every corner of the world - the ultimate specimens of power, cunning, instinct, and even intelligence; Beautiful tribal cultures and protective care for the land, they tower over most mortal races by a meter at least! **Rajira**, **Shift**, **Greenskin**, **Ni'vana**, **Brahamathian**, **Gnasha**, and the **Hissith** make up the impressive collection of cultures and tribes.

So one can imagine when certain tribes of **Haula'fin** began to get adopted into their ranks. These massive mountains of might, muscle, and scales towering over them like spires reaching to the sun. Of course, they were never mistreated for their diminutive stature, but it did make some of the day to day activities of civilization a bit obtuse.

Nothing was ever designed to fit Haula'fin unless it was catering to the Gobu'rhin community. And those guys were already pretty reclusive, so that was a big _nope_, usually. A young Haula'fin woman named **Oriana** grew especially consternated at the frequent need to ask for ladders, stools, boxes, and worst of all - shoulders.

Everyone loved to carry the little pint-sized-people, and they generally did not mind it. But when Oriana was literally starting to see clothes that had handles built into them, she knew something needed to be done. And so away she went - well, her attendant **Khalos** picked her up and carried her up the large steps into her laboratory, and away **they** went.

The problem wasn't really being short, the problem was everyone being too big. All of these fancy **Cloudbreathers** should just stop growing and they wouldn't need to deal with this. Their fancy long pants, stupid heels, ...giant horns. Life was complicated. The fact that the Darukai were pretty small villages and settlements didn't aide the problem, because even if one collective adopted a fashion that helped the short-stack community, that didn't necessarily cascade it over to the others.

Oriana's husband, **Sativola**, watched in horror as his wife went pretty much bat-shit crazy in her research. She was kind and reserved of course; A polite hostess, sweet to everyone in the village, keeping her menial day job as a **Rajira** tailor ... oh, but when night fell... she donned the goggles, the books came open, fangs were bared and _oh god the amount of explosions he had to make excuses for_.

_"Yes, yes, we discovered ... an exploding ...plant. It was an accident. I'm sorry to have woken you up."_ Sativola sheepishly made up lies about his wife on a nightly basis, as she was not ...a very good artificer. She was more of the _"housewife"_ kind of personality - but insanity could push a person to strange talents.

**FINALLY THEY WERE COMPLETE!** _"What the hell, Oriana! Those were your mother's earrings!"_ _"It's okay, she's dead. She won't mind."_ _"That's really not the point. You can't just start scribing runes onto your most precious jewelry because you run out of plates to use!"_ _"Look, just trust me on this. This will change the entire world. Everyone is going to love this."_

Gathering a few _trusted_ neighbors (is that even what you call other people in a little tribal village?), Oriana explained the premise of her experiment and pleaded for a volunteer. Eventually, an orange and black striped Rajira named **Tabbitha** stepped up to accept the challenge...

Putting the earrings on (hesitantly), the audience stared in amazement as Tabbitha began to shrink - shrink - shrink down to match the same size as her Haula'fin friend! Everyone stared in disbelief! It had actually worked! But ...

_"So ...you're going to just convince everyone to wear magical earrings that make them shorter?"_ _"There are a few holes in the plan. We'll figure it out."_ And then the tiny Rajira laughed at her lifelong friend, reaching to take the earrings off.

... and she kept trying to take the earrings off. This was the start of an awkward friendship that would usher in Tabbitha's own unique set of adventures around the world as the tiniest little Rajira to ever exist. But that's a different story.

The earrings would not come off - eventually, they forcibly broke the bands apart. But even at that, Tabbitha did not return to her original size. She was stuck at Haula'fin level for the remainder of her life.

It was at this point that Oriana realized her plan was probably not going to catch on overnight, and tucked away her research for another day. But, she did have a Darukai friend her own size now, at least! ... Involuntarily... by accident.

Later on, in the `3rd Age`, a brilliant Artificer and Runescribe named **Otho Bladud** would discover Oriana's notes and perfect her technique, creating earrings that allowed the wearer to shrink down to Haula'fin size without the ...unfortunate permanence. It's also said that these helped him to perfect his work on a project called **Redimensional Rails**, whatever the hell those were.