---
title: "QYRETH"
url: "qyreth"
group: "primera"
order: 1010
---
# QYRETH


The way the various realms are divided has never been entirely clear, nor have the gods seen fit to fully reveal it. It's known that the mortal world is called the **Midground**, and while a proper name for the land the gods hail from is unknown, it's usually referred to as the _upper_ realm.

But there are other worlds alongside ours, and while the relationships between them are not inherently clear, it's known that at least one is a plane of elements, and another is a strange void that lacks magic entirely.

One would then logically assume that if our realm is in the _middle_, there must be a _lower_ realm. Scholars posit that this isn't ..._completely_ wrong, but it's not precisely _right_ either.

Long, long before the `1st Age` - in the very early days of **Antiquity** itself, the world had not even discovered magic. No one is sure how, but ancient paintings and carvings suggest that there was a civilization that lived alongside the budding mortal races, almost as if it grew in their shadows.

The evidence of this is seen in the constant presence of silhouette figures across primal artwork. A very repeated theme of **some** manner of an entity that was at least recognized to have an embodiment frequently accompanied almost all of the oldest records.

As written records began to emerge, evidence mounted even further than other realms might intersect our own. There was sometimes lore stored about people who went insane because they could hear conversations of people that were not there.

Some called them **ghosts**, some called them **demons**, still others called them **aliens**. Whatever the case, the phenomena continued all the way into the `1st Age`. When magic and literature began to bloom further, it became more plausible to start researching these _'realms'_.

Scholars, mages, monks, and mediums all tried to call out to these figures. They decided to listen for replies but never received one. The specters were chalked up to ghost stories and extraterrestrials; The ramblings of the insane - until the `2nd Age`. Magic begins to cover the entire globe, more and more people were pouring their souls into the aether and discovery of the other realms was starting to proliferate.

A **Rajira** woman named **Felurs**, a panther-like being with incredible agility and prowess, lived an isolated life away from the other Darukai tribes. Some manner of shame had caused her to take an oath of solitude and meditation, self-exiled from her people. Alone in the wilds of **Astria**, Felurs spent decades just reflecting on the world and the events that led to her decisions. It's not known what travesty she committed that brought upon her imposed excommunication, but despite the frequent attempts by her own family to get her to return, she adamantly refused.

Felurs held no interest in magic, nor the realms, and especially not these odd entities that had been researched for so long. She didn't even know such things could exist; Her life was simple and mundane - daily struggles to survive, followed by long hours of meditation. But perhaps it was the pure ignorance of all things magical that allowed her to finally hear a voice that had never reached the realm.

_"I'm lonely, too."_ Was all she heard - like a whisper in the winds. It was so faint, and so gentle, but so genuine and honest. Felurs did not know what to make of the experience and attributed it to simply becoming delusional from isolation. Again, days later, as she sat meditating ... the voice spoke out again - always the same words. For two years, this happened every three days without fail. It became so routine that one day when she finally did not hear the voice ... Felurs became concerned.

She ventured into the nearby forest, which seemed the most likely place for someone to disappear to; Eventually, her instincts led her to what looked like a swamp of ink-black miasma. She did not understand why, but she felt compelled to wade into it.

Felurs was never seen again; However, about `30 years` later, a woman calling herself **Qyreth** managed to happen upon the Darukai village where Felurs had come from, and the beast-like people there found the scent she carried to be disturbingly familiar.

Qyreth rarely spoke, but whenever she did, there was something so very other-worldly about her voice. Some remarked it as evil, or demonic - and still others called it alluring. Despite appearing human, she managed to garner friendship with the Darukai and settled in this small village.

To this day, no one ever discovered what happened to Felurs, and the miasma she wandered into never appeared again. Qyreth lived in relative peace with the village and had many children, and everything but her voice seemed pretty reasonable. But sometimes ...

...Odd things would happen. When the village was threatened, the opposing factions would up and vanish overnight. When they ran out of food, they were suddenly visited by slightly intimidated village folk offering a bountiful surplus and aide. It was as if this little Darukai village had a Guardian Angel; Or a Guardian Devil.