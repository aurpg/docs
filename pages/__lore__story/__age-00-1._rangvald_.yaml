---
title: "RANGVALD"
url: "rangvald"
group: "archaea"
order: 1020
---
# RANGVALD

During the `2nd Age` of the world, magic was starting to flourish a bit more; While it had not quite reached the level it would eventually obtain in the `3rd Age`, it was a dominant force with a growing community and a great deal of research being poured into it.

However, with the introduction of magical forces came beasts that were emboldened by it, or attracted to it. But even before magic, the world had its share of incredible terrors to deal with. During the **Daybreak of Antiquity**, the time before the `1st Age`, the terrible **Vorpal Dragon, Varsogg** and his seeming-allies **The Forgotten** were a menace to all the races ... but locally, each civilization had a classification of threats that were collectively called the **Leviathans**.

The Leviathans were, at first, just the name given to huge creatures that came out of the sea. Though it's not wholly appropriate, the term eventually came to blanket the entire world's populous of animals that were considered unnervingly huge or powerful. The name was assigned to any manner of beast capable of wiping out a village or settlement on its own, and there even came to be rankings of their ability.

In the vast, freezing land of **Skors'sommr**, a man named **Rangvald** was known as something of a _hermit_ by the people of the neighboring villages. Rangvald did not make any of the settlements on the continent his own - in fact, he lived out in the frozen wilds by himself. Rangvald was not really called a peaceful man, but he was not what one would refer to as hostile either.

Rangvald seemed to be the kind of person that craved some measure of challenge or serenity, or both. Magic, fascinating as it was, did not seem to catch his fancy; He found a thrill in using his body to accomplish tasks and appeared to favor a lifestyle among the most dangerous foe he could see; Nature itself.

For quite a while, he lived in isolation with relative ease. Villagers would frequently deride his choice of lifestyle, but when danger threatened the villages, he was always there to help - so no one honestly tried to stop him, or accused him of not contributing to the society. Despite the isolation, he still had something of value to trade when he came to town, and he seemed like a pretty exceptional hunter.

But, as is with all good stories, fate does not come at a moment of our choosing; On a frigid **December** night, for some reason unexplained, the stars refused to appear ...and the moon as well. The world bathed in horrible darkness, and Rangvald opened his eyes from meditation by a small campfire as the animals he had come to have harmony with seemed restless.

It's said that the reason recorded history is divided at the `1st Age` because that is when the great Earth Mage, **Hollister**, gathered an army and fought the **Vorpal Dragon, Varsogg**. For this reason, that moment is earmarked as the dividing line between the period when Varsogg threatened all the living without refute, and the time when the mortal realm had the power to stand up to him.

In this time before such recorded history, there were other records of the terrible events. The time before the `1st Age` was measured by these phenomena where the sky turned completely black, and all creation seemed to stop; They were known as the **Abandonments**; Thought to be moments when the gods turned their back on their own creations. The period between each Abandonment was different, but it was always inevitable that the occurrence was the start of many, many trials for the Midground.

It seemed all light would be lost from the world, and Rangvald's campfire was snuffed in an instant; All light, across the whole of the planet, vanished without a word. A floating sphere of pure, utter darkness drifting in space. Rangvald closed his eyes and listened to the growls and snarls of wild beasts under the sway of malevolence.

The people of the Midground knew what came next; It was always the same. On the night of the Abandonment, the world went dark only before half the planet could look up to see two large red slits opening up; Giant eyes, as if the size of the moon itself. The Abandonment was the moments when **Varsogg**'s power grew to its height, and the already colossal dragon grew dozens of times over, and could literally cover the whole planet in its wings. His hateful breath spreading over the land like a poisonous miasma that took hold of any heart weak enough to be swayed.

And so the Vorpal Dragon's eyes opened again, and stared down at the world he seemed to so hate; It was unclear why Varsogg never outright destroyed the planet, for everyone could discern he had the power to do so.

Black shroud covered half the globe, and every beast the Forgotten could take hold of became a harbinger of malice. The seasoned wildman could feel the breath of something sinister on the back of his neck, like a creature preparing to sink its teeth into a waiting meal.

Without hesitation, his reflexes shot into overdrive, and as if controlled by some manner of zen Rangvald's body twisted like a hurricane to deliver a fierce blow to the creature behind! Animal-like senses seeming to fire off like mines, the faint tickle of venomous tongues starting to grace the fur of his boots caused another leap into the air - as if tethered to his arm, he tore an object out of the darkness; Fingers wrapped blindly around an object he could entirely never mistake - his own trusty fishing spear.

Lost in this void with no sight, Rangvald had only his senses of smell, touch, taste, and sound to fight with. His bear-like strength serving to propel blow after blow while he competed in a circle, defending his camp, protecting his life, and refusing to let the Vorpal Dragon have a victim. For `68 hours`, Rangvald fought enemies he could never see, unable to discern the damage done, unable to replenish his strength, and unable to run away.

At last, on the `72nd Hour` of the Abandonment, the sky lit up with hopeful golden light! In the sky, **Yldrerth**, the **Mystic Dragon**, had taken wing to hurl itself towards the grinning sight of the ludicrous lizard looming over the planet. Rangvald just paused in exhaustion to stare at the sign that they had not been completely forsaken ... and around him lay `81` slain **Jormundr** - a `3rd Class Leviathan` that terrorized Skors'sommr. Massive serpents with bodies as thick as a house, fangs like swords, and the ability to melt bones with their venom. Jormundr were among the most feared beasts to roam the world but were typically only found at sea.

With the light of Yldrerth filling the sky, the remaining assailants seemed to retreat, and Rangvald let out a sigh of relief before turning to look around for the biggest Jormundr out of his kills ... he wanted a new weapon out of this ordeal.