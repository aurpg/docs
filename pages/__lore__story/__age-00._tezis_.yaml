---
title: "Tezis"
url: "Tezis"
group: "primera"
order: 1010
---
# TEZIS

It's known that the realm of mortals is called the **Midground**, and while most refer to the world of the gods as a _upper_ realm, the whole naming convention suggests there is something even _lower_ than this mortal world.

Naturally, one tends to think that's some kind of twisted hell or lake of fire; Some sort of pit of damnation from which no life escapes, but it's become increasingly clear over the ages that the planes are not so linear in structure.

The most educated scholars, at least since the `1st Age`, have held the belief that the universe could be viewed at more like a **tree** than a stack of plates; With both vertical and horizontal axis to the way, realms connect. This is further reinforced by the belief that the great tree, **Audrassil**.

The concept of a world-tree is nothing spectacularly new, as many cultures seem to 'root' their origins to such an idea. The sages of Honoshia seemed to really expand on it during the `1st Age` - citing an incredible spire of vines and beautiful flora that runs through **Skors'sommr** as the evidence that there is **some** kind of entity that connects the realms together.

Though it seems to have vanished from the world, the story of **Audrassil** stems from as many myths and legends as the gods themselves have spawned. It is believed that somewhere in the world exists a land that pre-dates even the time before **Antiquity** and contains sacred life that was protected from the **Forgotten** when they were unleashed on the world. While some believe this sacred land is tucked away in **Sar-Sadoth**, as it is sealed from the world by a barrier, an extensive collection of studious researchers actually think the land exists somewhere else. The **Sondra Scriptures** refer to this place as **Chiralis**.

The most reasonable guess is that the sacred place has ties to **Nahauzca**. This is evidenced by the incredible wellspring of power that granted **Ahrima** her ascension to celestial status, but such swells of magic have been recorded at every age and across the entire world... while most of the expanded knowledge and study happened during the `1st Age`, there is evidence that people knew of it before then.

Whenever there is power, there will be those who covet it, and then those who worship it. Before the `1st Age`, a weak-willed sorcerer named **Tezis** sought to make a name for himself by tapping into the streams of mana that covered creation - even though mankind was only beginning to understand what it really was. Tezis saw the way others prayed to gods, and how some prayed to demons; Thinking himself wise, he went on to posit that maybe other beings existed that could elevate his status.

Tezis wasn't necessarily **wrong**, but he didn't correctly represent the most brilliant minds the developing world of sorcery had to offer at the time. Tezis siphoned power from the wells of mana he could find within reasonable traveling distance, and collected them into a phial; He believed that whatever magnanimous entity he reached out to would likely subsist on energy, so having a huge mass of it on hand was only the smart thing to do.

With magic in such infancy, Tezis so dumb, and the **Forgotten** able to easily whisper into the desires of man, a shadowy entity named **Vrioj** took this opportunity to suggest some considerably poor instructions to Tezis in his dreams. This went on for a few years, while the sorcerer gathered the power necessary to complete his goal.

With the influence of such malice at work, the otherwise innocent; albeit selfish, wizard set about his plans one freezing February morning. With `8` garish urns covered in nonsensical writing that he was convinced was some powerful dialect, Tezis sat in the middle of the snow, butt-naked and chanting in muddled tongue-twisters that sounded akin to an occultist's hazing ritual.

Vrioj hadn't given Tezis utter nonsense; The ritual he performed was very much real, and it's unclear what branch of the realms it reached into ... but upon completion of his chant, Tezis was vaporized in a plume of blue smoke, only a massive crater of charred black rock remaining where he had been. The urns were gone, and the vial of mana shattered like glass. In total, the cost of materials was only about `10 copper` - they were all so cheaply made, so no one who happened upon the remains really paid it any mind.

A decade later, the village Tezis hailed from began to find itself terrorized by a strange beast with a bright, azure mane. No one survived its attacks, but across the countryside, it threatened, it came to be called the **Namedrinker** - titled such because it always appeared in the next village with individual memories of the person who it slaughtered last in the previous one. (_Though one has to question how anyone figured that out if it left no survivors._)

The Namedrinker rarely appeared in recorded history, but at least one time in each age, its name came up in some story. And every time ... it was always seen using a technique or ability that was thought to be lost forever.