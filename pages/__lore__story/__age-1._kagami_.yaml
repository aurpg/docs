---
title: "KAGAMI"
url: "kagami"
group: "1st Age"
order: 1100
---
# KAGAMI


There is a standing belief that the gods were once ordinary people, and through some act of coincidence or bravery, performed an exploit that allowed them to ascend to some kind of entity beyond mortality.

The **Dawn of the `5th Age`** further cemented this belief, as there were awakening memories of a universe past, and a story of a little girl at the end of time who somehow survived the end of all things.

It's not completely clear what elevates someone to the state of a deity, and it is even less clear where the first, **Mordos**, originates. The paradox of considering the gods to be ascended mortals defies the logic of everything in creation myths and religion.

However, although this all remains a mystery, it has not stopped people from discovering relics that carry indistinguishable traces of particular deities. The suggestion that they walked among us as mortals give many people hope for what kind of potential the worlds have, and in others, it instills terror that they are in fact as incompetent as the rest of us.

A little girl in **Honoshia** named **Kagami** was known locally as a bit of a problem child. She was a homeless beggar, and at only age `8` had already become quite the pest to various families in the region. Kagami would often make fairly overt attempts to _endear_ herself to people in hopes of being fondly treated.

It was not rocket-science; She had lost her parents when the terrible **Vorpal Dragon, Varsogg** had laid complete and utter waste to the hillside where she lived. Almost everyone had died; The **Daybreak** of the world was a rough time; The period of Antiquity that came before even the `1st Age`. Plagued by dragon attacks, Varsogg and the **Forgotten** essentially ruled the world with the power to terrorize as they saw fit.

And so, Kagami wandered, as orphans were wont to do. She sought meaning in this terrible chaos, she sought a place to call home, warm meals, clothes; The average needs of any living person. Kagami had a unique kind of spirit that captivated others, and it served her quite well to get by meal to meal.

She had lost everything, but the girl didn't seem to act as if anything was gone. She had a perky sort of disposition; The kind that just brought a smile to anyone's face, and the fact that she almost never stopped smiling herself ...well, it was pretty infectious, especially for such a dark time in the world.

Kagami was most well known for flitting around the edges of villages with her arms stretched out, claiming that she was looking for treasure dropped by travelers going in and out of the towns. With barely a burlap sack to her name, the girl somehow found people to let her use their baths every day, she always managed to get at least one meal, and even seemed to trick the village medic into treating her wounds without compensation.

And one day it seemed that she might have actually found her sought-after treasure. The curious girl spotted a glint of gold in the sand and darted off towards it like a squirrel. It was gone by the time she got there, and she seemed a bit perturbed ...hands on her knees as she squatted in the dirt with a disappointed pout. But then the corner of her eye caught another little sheen.

Like following a trail of sparkles, she kept being one step too slow to catch this moving shine, but finally, as the sun was starting to drop, Kagami took a reckless dive towards the glinting in the sand and laughed playfully as she lifted up an awkward looking bug that was carrying a bronze coin.

The coin seemed pretty worthless, and the bug was just a standard beetle. At first, one might imagine the girl to be disappointed that it was merely a mite of bronze, but Kagami didn't see the world like everyone else. She was **so** overcome with fascination at the most mundane slab of metal to ever exist that she literally giggled in a fit so loud it beckoned village protectors because they mistook it for screams.

She rolled around in the dirt with her little bronze coin; It had no face, it had no value, it was scuffed and jagged and looked ready to snap apart. But Kagami treasured that worthless metal like it was the most expensive item in all creation.

Everything just seemed better, when she clutched her little coin. She would rush by a dank alley filled with bugs and feces, and pause to look over and spot the happy family of cats that was managing to find a crate. She rushed between little huts, and always found that one place where the seams in their design were just **so** perfect.

It was like optimism had become a force of nature and just flooded from her every emote. As she grew up, this only become a stronger part of her being. At age `47`, Kagami finally managed to take to the seas, intent on exploring the world. She spent years saving up for the most humble little ship, and set out alone - only her special coin to keep her company.

She encountered a boy on the continent of **Nahauzca** that was growing into quite the talented young man, with powerful abilities to control the earth, and a burning determination to fulfill a promise to a **Sprite** he had met as a child. The boy pretty quickly took to Kagami's personality, and her infectious optimism was not lost on him.

Kagami served as something of a mother-figure to this adolescent, and when she discovered he lacked a name, dubbed him **Hollister**. Hollister would join Kagami on her travels around the world, seeming equally pleased by her fascination in every detail they discovered. Hollister, too, had only a copper ring to his name, and so the two seemed to have an almost fated origin to discuss.

Hollister told Kagami of the Sprite Princess, **Phiale**, and his feeling that no one should ever be forgotten entirely from the world. He talked of the fantastic**starlight** she had shone with, and fancifully showed off the glittering trail of light his otherwise worthless ring could create. They grew together, adventured on, and eventually became lovers.

Hollister's starlit ring granted Kagami unnaturally long life, as well, and the two would go around the world gathering allies for their quest to return the fabled Starlight that could seal the Forgotten away! Her spirit and will please so many that it made bringing people to the cause easier than ever before, and eventually, they had amassed what could only be called an army of support.

No one is entirely clear how, but Hollister, his close friend **Aramil El'tanrian**, a warrior named **Solast** beyond Corsario, a woman named **Lysyth**, and of course, Kagami herself were met with an audience most important - Hollister had been contacted by **Mordos**, the known Lord of all the Realms.

The plot to destroy the Forgotten was underway, and with **Illnyvia** at his side, Mordos met this small party in secret, tucked away in a secluded valley somewhere in northwestern **Astria**. By this point, Kagami had become a brilliant tactician and was at the forefront of the plan to gather the races and lay ruination to the Vorpal Dragon. She stepped forward to present a map, at one point ...

And there, glinting in her hand, as if she always held it directly, was that copper coin. The God Mordos paused for a moment ...and just stared at her hand - and eerily reached out to take hold of it. Kagami dropped the map in shock, and the rest of the party took a defensive stance, thinking they had been betrayed.

Mordos unfolded Kagami's hand to reveal the coin ...and his head simply bowed while he wept in utter silence. A genuine smile crept across the Lord's lips as he closed the woman's fingers around the coin again, and spoke with great confidence. _"Yes, these are certainly the right ones."_