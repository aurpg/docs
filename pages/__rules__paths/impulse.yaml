---
title: "IMPULSE"
url: "impulse"
group: "kinetic"
---
# PATH OF IMPULSE

## `600` **RESEARCH**
### REQUIRES `0` IN **PATH OF IMPULSE**
---
component: command
name: YELL
nature: KINETIC
aspects:
- ANNOYANCE
paths:
- PATH OF GLADIATORS
- PATH OF IMPULSE
costs:
- 1 ENERGY
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- HOSTILE
- FRIEND
description: |
    Shout out when you take action to bolster your strength and focus; You can also distract some foes.
effect: |
    - `+[3]` **FORCE** and **SPELLFORCE**
---
component: command
name: WHINE
nature: KINETIC
aspects:
paths:
- PATH OF IMPULSE
costs:
chances:
- "[4+, #, #]"
requirements:
- 2 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- SELF
- MATCH
description: |
    Your flighty, whimsical, or feigned foppish nature has incurred a pentient for complaining and driving those around you to actual anguish.
effect: |
    - Become a source of drama and gain the benefit of a free extra dose when consuming an edible consumable that supports doses if `CHANCE` succeeds in a match.
---
component: command
name: UPSELL
nature: KINETIC
aspects:
- MINDFORCE
paths:
- PATH OF IMPULSE
costs:
- 4 ENERGY
requirements:
- 2 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- HOSTILE
- FRIEND
description: |
    Your cunning, or flighty nature gives a jittery countenance or perhaps a bold, rapid-fire charisma. Either way, you can talk quite quickly, throw out a lot of important words, feints, or over-acting to make people **think** you're good.
effect: |
    - Add `1` or `-1` to a single die in `CHANCE`. This may cause a `MATCH` but not a `CRITICAL` or `FAILURE`.
---
component: command
name: RESTLESS
nature: KINETIC
aspects:
-
paths:
- PATH OF IMPULSE
costs:
- 1 ENERGY
requirements:
- 2 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- SELF
description: |
    You're often anxious, irrational, impulsive, and flighty; It's hard to stand still and just wait for things to happen naturally.
effect: |
    - Replenish `[6, 9, 12]` `HEALTH`, and an additional `[4, 6, 8]` `HEALTH` for any `5+` in `CHANCE` or `SPEED` if it is rolled.
---
component: command
name: OLDEST TRICK
nature: KINETIC
aspects:
- MINDFORCE
paths:
- PATH OF IMPULSE
costs:
- 1 ENERGY
requirements:
- 2 INSTINCT
phases:
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- HOSTILE
description: |
    Just because something is a trope doesn't mean it's bad; The art of getting an opponent to look away is perhaps the simplest, but most fundamentally useful weapon someone can use.
effect: |
    - Reduce target's `HASTE` by `-2`, and then an additional `-3` for every even number in your `CHANCE`.
---
## `1200` **RESEARCH**
### REQUIRES `3` IN **PATH OF IMPULSE**
---
component: command
name: FIDGET
nature: KINETIC
paths:
- PATH OF IMPULSE
costs:
- 2 ENERGY
requirements:
- 3 ACROBATICS
phases:
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
description: |
    You're so ambulatory that staying calm actually requires you to exert energy. This is *pathetic*. Struggle with anxiety to hide in plain sight! Don't move ... just stay calm...

    Non-soval beasts will not see you, but they can smell you. You may hide in plain sight from them at your own risk.
effect: |
    - If targeted by a beast at random, it must roll `1d2`. On a `1`, it instead goes to the next person in the list.
---
component: command
name: GO LEFT
nature: KINETIC
aspects:
paths:
- PATH OF IMPULSE
costs:
- 3 ENERGY
- 30 MANA
- 1 TEMPO
requirements:
- 3 INSTINCT
phases:
- RESPONSE
tags:
- ONCE
- SELF
description: |
    Your movements occur almost on their own, instincts telling you when to zig, when to zag, and when to just get the fuck out of there.
effect: |
    - Perform `DODGE` as normal, and increase `SPEED` by `+4`, and also `+[2, 4, 6, 8]` for each `4` `INSTINCT` on your sheet.
---
component: command
name: FLOWING
nature: KINETIC
aspects:
-
paths:
- PATH OF IMPULSE
costs:
- 3 ENERGY
- 30 MANA
requirements:
- 3 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
rapid: true
description: |
    While in motion, you are not only more difficult to hit, but when you obtain a certain rhythm, your sequential movements each begin to feed into one another. This flowing movement is the sign of greater power, and amplifies many traits both offensively and defensively.
effect: |
    - Generate `1 TEMPO`.
---
component: command
name: RIPOSTE
nature: KINETIC
aspects:
-
paths:
- PATH OF IMPULSE
costs:
- 1 MOMENTUM
- 1 ETHERSURGE
- 1 TEMPO
- 1 DRAW
requirements:
- 3 INSTINCT
phases:
- RESPONSE
tags:
- ONCE
- HOSTILE
- MELEE
description: |
    When an enemy comes in to strike is when they are often the most vulnerable; Take advantage of this by lashing out in the moment of opportunity. Pull up a card with their name on it and dig your knives in - it's time to punish those who would dare strike out!
effect: |
    - Attack the target with `[1d32, 2d32, 3d32, 4d32]` `kinetic` `melee` damage. `FORCE` is applied to the final result.
---
component: command
name: BACON
nature: KINETIC
aspects:
- AESORIC
paths:
- PATH OF IMPULSE
costs:
- 3 ENERGY
- 30 MANA
requirements:
- 3 INSTINCT
phases:
- ACTION
- LINKED
tags:
- ONCE
- SELF
- DANCE
description: |
    The spirit of music and rhythm compells you to continue on whatever you were doing, but put some step in it! Uncontrollable urge to sing or dance give your body a beautiful beat with glistening blue and green haze. BEHOLD! Someone is doing it now!

    Witness how they sparkle, how they shine! Feast upon the coursing rivers of sweat as their heart races with the rhythm that only a soul can bare! They told her that dancing would never save the party! And now she's doing it! She's really going to show them w--Oh, well. Dinosaur to the face. Guess that one's over.

    But that's okay, it's probably not hungry anymore, and now it's your turn to try!
effect: |
    - Gain `2` `TEMPO` as normal, but any `ETHERSURGE` or `MOMENTUM` you had can be converted into `TEMPO` as well.
---
component: command
name: POCKETS
nature: KINETIC
aspects:
paths:
- PATH OF IMPULSE
costs:
- 2 ETHERSURGE
- 2 MOMENTUM
- 2 BOND
- 2 TEMPO
- 2 DRAW
requirements:
- 3 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
description: |
    Never to be caught unprepared, you have outfitted yourself with everything necessary to excel. While your hands may constantly be rummaging through that utility belt, it's going to have what you need at the right time.
effect: |
    - Convert `2` `MOMENTUM`, `ETHERSURGE`, `TEMPO`, `DRAW`, or `BOND` that you have accrued into a small wafer, spending the resource but capturing it within the wafer;
---
component: item
name: IMPULSIVE WAFER
class: 'item-container-md'
origin: CREATION
nature: MYSTIC
role: CONSUMABLE
species:
types:
- MYSTIC
- CREATION
mass:
doses: 1
statistics:
slots:
- CONSUMABLE
tags:
challenges:
description: |
    A peculiar wafer manifests from your energy or mana; It seems attuned to your power and energy, and throbs with strength as you hold it. Best not lose this.
effect: |
    - Break the wafer to absorb its power.
commands:
-   name: POP!
    css: command-nested
    description: |
        Crush the strange wafer to pop it like some manner of air-filled pocket, releasing the power inside..
    effect: |
        - Accrue `3` `MOMENTUM`, `ETHERSURGE`, `TEMPO`, `DRAW`, or `BOND`, even if you would exceed the maximum.
        - You may accrue a resource different than what you created the wafer with.
        - Multiple wafers do not stack, and dissolve after a few hours.
        - These wafers only work for the creator.
---
## `2400` **RESEARCH**
### REQUIRES `6` IN **PATH OF IMPULSE**
---
component: command
name: ARMCHAIR DEGREE
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF IMPULSE
costs:
- 100 PRESENCE
requirements:
- 4 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
- FRIEND
- HOSTILE
- PRESENCE
description: |
    You do not need a fancy degree, schooling, peer reviewed studies, or licensed qualifications. You went to the library and your kids don't eat anything with dihydrogen monoxide. You can use fancy equipment just fine, thank you very much.
effect: |
    - Attempt to use consumables with the `MEDICAL`, `COOKING`, or `ARTIFICER` tags, even if unqualified.
    - If the equipment has a challenge to pass, your modifiers against it are penalized by `50%`.
    - If you actually have the qualifications, then score a `CRITICAL` on `2` `6`'s instead of `3`.
---
component: command
name: SOLICIT
nature: KINETIC
aspects:
paths:
- PATH OF IMPULSE
costs:
- 300 PRESENCE
requirements:
- 4 INSTINCT
phases:
- ACTION
- RESPONSE
- TETHER
tags:
- ONCE
- SELF
- PRESENCE
rapid: true
description: |
    The merchant before you has closed his shop. He is not interested in your money, time, or even the quest you are on to save your long-lost sister from the dark clutches of a chicken cabal seeking to sacrifice her like rye. He eyes you with suspicion and tears swell.

    *"Do you know **who** I am?"* You implore, but it falls on deaf ears.

    And then like a glorious gleaming golden ray of indelible sunshine, the heavens glow as your clothe parts ways; A veritible dead-see of fabric to *very* firmly assert the query once more. *"DO YOU KNOW **WHO** I AM!?"*
effect: |
    - You are now considered `NAKED`, `BAREFOOT`, and `UNARMED` - despite not being any of those, and gain `+1` `ETHERSURGE`, `MOMENTUM`, `TEMPO`, or `DRAW`. Why you need to be naked to draw cards is a question we don't have time for.

    - Event hosts may respond to this in any number of ways.

    - Nipple
---
component: command
name: FISHIPPOCTOPLEASE
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF IMPULSE
costs:
- 0 PRESENCE
requirements:
- 4 INSTINCT
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- RESIST
- PRESENCE
description: |
    *"I need you to slow down. Please, stop panicking. All of your words are just jumbled together - I cannot understand you!"*

    Your inconsolable or distracted behavior is causing no end to chaos for everyone else.
effect: |
    Squish the first `3` of the dice in your `CHANCE` roll into a single huge number; Lose that much `PRESENCE` as a cost.

    Roll that number as a single die for **astral** damage

    - For example, if your `CHANCE` roll was `[2, 5, 4]` you would pay `254 PRESENCE` to roll `1d254` damage.
---
component: command
name: BUTTERFLY
nature: KINETIC
aspects:
-
paths:
- PATH OF IMPULSE
costs:
- 2 MOMENTUM
- 2 ETHERSURGE
- 8 ENERGY
- 2 TEMPO
- 2 DRAW
requirements:
- 4 INSTINCT
phases:
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- FRIEND
description: |
    Hypercognitive abilities are the stuff of legends; Movements that can break any attack, power that can react without thought, and cards to predict the future. But then there's ...

    Your allies. They possess none of these.

    It's not wise to be flighty. Getting sidetracked can cost them dearly. But if you use enough grace, it can work in their favor. With enough care and poise, even you can--...come back please.
effect: |
    - Call for an ally at just the right moment, causing them to miraculously move away from impending threats; Add `+[8, 16]` to their `SPEED` when performing a `DODGE` and reducing damage to `[25%, 10%]`.
---
component: command
name: SHINEY
nature: KINETIC
aspects:
-
paths:
- PATH OF IMPULSE
costs:
- 200 PRESENCE
requirements:
- 4 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
- PRESENCE
description: |
    Most techniques are about distracting your enemies, not yourself... but if it glints brightly enough...
effect: |
    - Move absurd, illogical distances of up to `500` meters without challenge to approach something sparkling. If this triggers any traps that can be bypassed with `SPEED`, then survive them with `1` `HEALTH` left at minimum.

    - Your comrades may not like this ...but now they saw the traps, so ...win, I guess?

    - There must be an available path to the target. You cannot pass through the ground or through impassable walls.
---
## `7200` **RESEARCH**
### REQUIRES `12` IN **PATH OF IMPULSE**
---
component: command
name: TURBOSENSE
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF IMPULSE
costs:
- 4 TEMPO
- 2 FATE
- 4 MOMENTUM
- 4 ETHERSURGE
requirements:
- 5 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- PERSIST
- SELF
- UNSTABLE
description: |
    You frequently leap to react without thinking, but if that skill could be honed - even summoned upon in a moment of need, it would be remarkably powerful...
effect: |
    - Until you have rolled `3` `1`'s on `CHANCE` cumulatively, `DODGE` reduces damage to `10%` at the cost of `1` `MOMENTUM` or `12 ENERGY`.
---
component: command
name: HYPERTHEME
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF IMPULSE
costs:
- 1 FATE
- 2 TEMPO
- 4 MOMENTUM
- 4 ETHERSURGE
- 2 DRAW
requirements:
- 5 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
- UNSTABLE
description: |
    You sneak through the night, what's that sound!? It's your own covert music; A rhythm in your head turns to movement in your hips, and you effortlessly dance, squish, slink, snake, jump, balance, and spring around with complete and utter disregard for gravity and physics.

    Perhaps you zoom about with magic, or did you draw a card that points to serendipity? Your feet could move like the wind. Bones collapse and contort, and weightlessness is almost flight.

    Whatever is going on, this is definitely the opening title song - not that ending theme that no one remembers. You have genuine marketability and the world would know it if you weren't so busy gloriously prancing through their well placed traps and pitfalls.
effect: |
    - You are considered `FLYING` against an effect.
    - You are considered `0` `SIZE` against an effect.
    - You take `0` damage from a `DODGE` or `RESIST`.
    - You may dance over liquid.
    - Your action does not raise the difficulty for hiding or evading notice.
---
component: command
name: DUELING
nature: KINETIC
aspects:
-
paths:
- PATH OF IMPULSE
costs:
- 2 FATE
- 3 TEMPO
- 4 MOMENTUM
- 4 ETHERSURGE
- 3 DRAW
requirements:
- 5 INSTINCT
phases:
- RESPONSE
tags:
- ATTACK
- ONCE
- HOSTILE
- UNSTABLE
- DANCE
- DEFEND
- DODGE
description: |
    Dizzying speed and blinding light, with golden haze your body zips and arches to avoid incoming blows. But it is unsuitable to just evade; One must be punished for impudence. They must be made to accept the consequences of such a poor strategy.

    Step inward and sweep; Use a spell to shift their weight. Draw a card and throw it at them - who knows what you're capable of! Pick up a rock and throw it or just shove them into cake frosting. They messed with you, and you're not going to let it go unchallenged.

    You may also slap them in the face for no cost or roll necessary.
effect: |
    - Retaliate by rolling `ATTACK`, then again for every `4` `ENERGY` you have, *(maximum `3x`)* and finish them off by adding their own `FORCE` `x2` to yours, and piling it into the final result as a **kinetic** attack.

    - This command allows a `RESPONSE`.