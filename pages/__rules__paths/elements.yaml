---
title: "Prime Elements"
url: "prime-elements"
group: "mystic"
---
# Path of Prime Elements

## `600` **RESEARCH**
### REQUIRES `0` IN **PATH OF PRIME ELEMENTS**
---
component: command
name: SCORCH
nature: MYSTIC
aspects:
- FIRE
paths:
- PATH OF PRIME ELEMENTS
costs:
- 10 MANA
- 1 ENERGY
requirements:
- "1 KNOWLEDGE: NATURE"
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- SELF
description: |
    The power of fire surrounds your body, spell, or weapon.
effect: |
    - Adds **fire** aspect.
---
component: command
name: STONE
nature: MYSTIC
aspects:
- EARTH
paths:
- PATH OF PRIME ELEMENTS
- PATH OF NATURE'S EMBRACE
- PATH OF SAGES
costs:
- 10 MANA
- 1 ENERGY
requirements:
- "1 KNOWLEDGE: NATURE"
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- SELF
description: |
    The strength of the earth encroaches upon your body, spell, or weapon.
effect: |
    - Adds **earth** aspect.
---
component: command
name: SPLASH
nature: MYSTIC
aspects:
- WATER
paths:
- PATH OF PRIME ELEMENTS
- PATH OF NATURE'S EMBRACE
- PATH OF SAGES
costs:
- 10 MANA
- 1 ENERGY
requirements:
- "1 KNOWLEDGE: NATURE"
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- SELF
description: |
    The motion of the tides ebbs in your body, spell, or weapon.
effect: |
    - Adds **water** aspect.
---
component: command
name: AEGIS
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF PRIME ELEMENTS
- PATH OF SAGES
- PATH OF STORMS
costs:
- 10 MANA
- 1 ENERGY
requirements:
- "1 KNOWLEDGE: NATURE"
phases:
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
description: |
    The power of the auralforce envelops you, and aether splashes outwards to repel other forces.
effect: |
    - An attack of any nature with **fire**, **water**, **wind**, **lightning**, **earth**, or **ice** aspect is reduced by `[4, 6, 12]`.
---
component: command
name: BURN
nature: MYSTIC
aspects:
- FIRE
paths:
- PATH OF PRIME ELEMENTS
- PATH OF SAGES
- PATH OF NATURE'S EMBRACE
costs:
- 10 MANA
- 1 ENERGY
requirements:
- "1 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
description: |
    You manifest a small sphere of fire above your palm that can light the way, or be hurled at a foe.
effect: |
    - Assail a target with `[1d8, 2d8, 3d8]` **mystic** damage with **fire** aspect.
    - You have a source of light and fire.
---
component: command
name: SOAK
nature: MYSTIC
aspects:
- WATER
paths:
- PATH OF PRIME ELEMENTS
- PATH OF SAGES
- PATH OF NATURE'S EMBRACE
costs:
- 10 MANA
- 1 ENERGY
requirements:
- "1 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
description: |
    You guide a gathered sphere of water through the air.
effect: |
    - Assail a target for `[3d4, 5d4, 7d4]` **mystic** damage with **water** aspect.
    - You have a small source of water.
---
component: command
name: DIRT
nature: MYSTIC
aspects:
- EARTH
paths:
- PATH OF PRIME ELEMENTS
- PATH OF SAGES
- PATH OF NATURE'S EMBRACE
costs:
- 10 MANA
- 1 ENERGY
requirements:
- "1 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
description: |
    You pull the dirt, mud, and rocks from the ground to heed your whims.
effect: |
    - Assail a target for `[1d12, 2d10, 3d8]` **mystic** damage with **earth** aspect.
    - You have a rock.
---
## `1200` **RESEARCH**
### REQUIRES `3` IN **PATH OF PRIME ELEMENTS**
---
component: command
name: FIRE
nature: MYSTIC
aspects:
- FIRE
paths:
- PATH OF PRIME ELEMENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
description: |
    Create a large, swirling eruption of flames upon a target.
effect: |
    - Assail a target for `[2d8, 4d10, 8d12, 10d16]` **mystic** **fire** damage.
---
component: command
name: EARTH
nature: MYSTIC
aspects:
- EARTH
paths:
- PATH OF PRIME ELEMENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
description: |
    Hurl a bevy of boulders, rocks, and spires at a target.
effect: |
    - Assail a target for `[3d6, 6d8, 9d10, 12d12]` **mystic** damage with **earth** aspect.
---
component: command
name: WATER
nature: MYSTIC
aspects:
- WATER
paths:
- PATH OF PRIME ELEMENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
description: |
    Drench a target with a sweeping wave of water.
effect: |
    - Assail a target for `[6d4, 8d6, 12d8, 14d10]` **mystic** damage with **water** aspect.
---
component: command
name: SMELT
nature: MYSTIC
aspects:
- FIRE
paths:
- PATH OF PRIME ELEMENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
description: |
    You heat up the area around your body and smolder with mystic energy.
effect: |
    - `+[2, 4, 6, 8]` **SPELLFORCE**
    - Evaporates small **environmental** sources of water.
---
component: command
name: DOUSE
nature: MYSTIC
aspects:
- WATER
paths:
- PATH OF PRIME ELEMENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
description: |
    You drench an object until it cools off, or a fire is put out.
effect: |
    - `+[1, 2, 3, 4]` **SPELLSTRIKE**
    - Extinguishes small **environmental** sources of fire.
---
component: command
name: WATERSTEP
nature: MYSTIC
aspects:
- WATER
paths:
- PATH OF PRIME ELEMENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- SELF
description: |
    Gather and control the water at your feet, so that you may walk across it.
effect: |
    - You may stand or move on water.
---
## `2400` **RESEARCH**
### REQUIRES `6` IN **PATH OF PRIME ELEMENTS**
---
component: command
name: ERUPT
nature: MYSTIC
aspects:
- FIRE
paths:
- PATH OF PRIME ELEMENTS
costs:
- 30 MANA
requirements:
- "3 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
description: |
    A searing wave of flames and burning air erupts beneath a target.
effect: |
    - Assail a target for `[4d4, 4d6, 4d8, 4d10]` **mystic** damage with **fire** aspect.
    - For each `3+` on your **CHANCE**, deal an additional `[12, 18, 26, 32]` damage.
---
component: command
name: MUDSLIDE
nature: MYSTIC
aspects:
- EARTH
paths:
- PATH OF PRIME ELEMENTS
costs:
- 30 MANA
requirements:
- "3 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- PROTECT
description: |
    Pull and maneuver the earth to crush and overwhelm a target.
effect: |
    - Assail a target for `[9d8, 6d24, 4d48, 2d96]` **mystic** damage with **earth** aspect.
    - For each `3+` on your **CHANCE**, add `[1, 2, 3, 4]` **SPELLSTRIKE**.
---
component: command
name: CASCADE
nature: MYSTIC
aspects:
- WATER
paths:
- PATH OF PRIME ELEMENTS
costs:
- 30 MANA
requirements:
- "3 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
description: |
    Consume a target with a rising tide of powerful, crushing waves.
effect: |
    - Assail a target for `[3d32, 3d48, 3d64, 3d72]` **mystic** damage with **water** aspect.
---
component: command
name: COMMUNE
nature: MYSTIC
aspects:
- FIRE
- EARTH
- WIND
- LIGHTNING
- WATER
- ICE
paths:
- PATH OF PRIME ELEMENTS
- PATH OF STORMS
- PATH OF SAGES
- PATH OF NATURE'S EMBRACE
costs:
- 30 MANA
- 3 ENERGY
requirements:
- "3 KNOWLEDGE: NATURE"
phases:
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
description: |
    Attempt an empathic connection with an element.
effect: |
    - Reduce a **mystic** attack with **fire**, **earth**, **water**, **wind**, **lightning**, or **ice** aspect by `[30, 50, 70, 90]`.
---
component: command
name: SPIRE
nature: MYSTIC
aspects:
- EARTH
paths:
- PATH OF PRIME ELEMENTS
- PATH OF SAGES
costs:
- 30 MANA
- 3 ENERGY
requirements:
- "3 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- SELF
description: |
    Beckon the earth to rise and change to your will.
effect: |
    - You may elevate the ground beneath your feet by up to `[6, 12, 24, 48]` meters.
---
## `7200` **RESEARCH**
### REQUIRES `12` IN **PATH OF PRIME ELEMENTS**
---
component: command
name: FLARE
nature: MYSTIC
aspects:
- FIRE
paths:
- PATH OF PRIME ELEMENTS
costs:
- 4 ETHERSURGE
requirements:
- "4 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- DODGE
- RESIST
- PROTECT
- UNSTABLE
description: |
    An ultimate explosion of fire, steam, and energy.
effect: |
    - Incinerate a target for `[12d24]` **mystic** **fire** damage.
---
component: command
name: FISSURE
nature: MYSTIC
aspects:
- EARTH
paths:
- PATH OF PRIME ELEMENTS
costs:
- 4 ETHERSURGE
requirements:
- "4 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- AREA
- HOSTILE
- DODGE
- PROTECT
- UNSTABLE
description: |
    An unsettling shift of tectonic energy swallows a target.
effect: |
    - Deal `[10d12]` **mystic** **earth** damage to all targets.
---
component: command
name: DROWN
nature: MYSTIC
aspects:
- WATER
paths:
- PATH OF PRIME ELEMENTS
costs:
- 4 ETHERSURGE
requirements:
- "4 KNOWLEDGE: NATURE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- UNSTABLE
description: |
    Encapsulate a target in a globe of suffocating water.
effect: |
    - Enact **DROWN** *(ADVANCE +4)* to a target and assail them with `4d16` **mystic** **water** damage.