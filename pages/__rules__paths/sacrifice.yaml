---
title: "Sacrifice"
url: "sacrifice"
group: "mystic"
---
# Path of Sacrifice

## `600` **RESEARCH**
### REQUIRES `0` IN **PATH OF SACRIFICE**
---
component: command
name: BLOODRUNE
nature: MYSTIC
role: RITUAL
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 30 HEALTH
requirements:
- 1 ANATOMY
phases:
- ACTION
tags:
- WOUND
- DIMINISH
description: |
    Write a sigil, script, glyph, or rune in your own blood.
effect: |
    - Draw a rune using your own blood that contains `[1]` ->ETHERSURGE<- or ->MOMENTUM<-.
---
component: command
name: BLOODLETTER
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 30 HEALTH
requirements:
- 1 ANATOMY
phases:
- ACTION
tags:
- PERSIST
- WOUND
description: |
    Using your blood, design a letter or re-arrange existing text.
effect: |
    - Create a message on a surface for `[1, 2, 3]` hours that others can find, or alter ink and blood written messages on a surface. Readers will always interpret the message in their native language.
---
component: command
name: HEMODAPTION
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 30 HEALTH
requirements:
- 1 ANATOMY
phases:
- ACTION
- LINKED
tags:
- ONCE
- WOUND
- DIMINISH
rapid: true
description: |
    Attempt to absorb the power placed into a **bloodrune**.
effect: |
    - Withdraw `[1]` ->MOMENTUM<- or ->ETHERSURGE<- from a ->BLOODRUNE<- placed by any character, including hostile ones. You may draw a resource different than what was declared within it.
    
    - The rune is dismissed even if you do not draw out the full amount placed into it.
---
component: command
name: ADRENALINE
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 30 HEALTH
requirements:
- 1 ANATOMY
phases:
- ACTION
- RESPONSE
- LINKED
rapid: true
tags:
- ONCE
- SELF
- WOUND
- DIMINISH
description: |
    Damage to your own body causes your senses to rush, filling you with strength for a brief moment.
effect: |
    - `+[1]` ->EFFORT<- to ->KINETIC<- or ->MYSTIC<- commands.
---
component: command
name: TRACE
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 30 HEALTH
requirements:
- 1 ANATOMY
phases:
- ACTION
- LINKED
tags:
- ONCE
- SELF
- WOUND
description: |
    Dispatch your own life force to try and latch onto another's, allowing you to track them more easily.
    
    Your blood will crawl around like a skittering spider, and seek out other sources of life.
effect: |
    - `+[2, 4, 6]` **EXPLORE** and **KNOWLEDGE: WORLD** and discern a target's blood type.
---
component: command
name: DONATE
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 30 HEALTH
requirements:
- 1 ANATOMY
phases:
- ACTION
tags:
- ONCE
- FRIEND
- WOUND
- OVERCOME
description: |
    Give life force to another.
effect: |
    - Grant a target `[1, 2]` ->AID<-.
---
## `1200` **RESEARCH**
### REQUIRES `3` IN **PATH OF SACRIFICE**
---
component: command
name: SACRIFICE
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 40 HEALTH
requirements:
- 2 ANATOMY
phases:
- ACTION
tags:
- ONCE
- SELF
- FRIEND
- WOUND
description: |
    Yield your own life force to recover the aether around you or an ally. This might get messy, though ...
effect: |
    - Replenish `[1d22, 2d22, 3d22, 4d22]` ->MANA<- for each `[5+, 4+, 4+, 3+]` on ->CHANCE<-
---
component: command
name: INFLICTION
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 40 HEALTH
requirements:
- 2 ANATOMY
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- PROTECT
- DODGE
- RESIST
- WOUND
description: |
    Suffer damage to your own body to mysteriously inflict pain upon others as aether ties you together.
effect: |
    - Assail a target for `[1d18, 2d18, 3d18, 4d18]` ->MYSTIC AURAL<- damage.
---
component: command
name: CONTRIBUTION
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 40 HEALTH
- 40 MANA
requirements:
- 2 ANATOMY
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
rapid: true
tags:
- ONCE
- SELF
- FRIEND
- WOUND
description: |
    Dispense your own life force to bolster your offenses, or an ally's.
effect: |
    - `+[2, 4, 6, 8]` ->FORCE<- or ->SPELLFORCE<- for each `2` ->SPELLFURY<- not used for ->CHANCE<-
---
component: command
name: EXHAUST
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 40 HEALTH
requirements:
- 2 ANATOMY
phases:
- ACTION
- LINKED
tags:
- ONCE
- SELF
- WOUND
description: |
    Your own life force oozes out of your body as your weaken your physical form to gain a frenzied panic.
effect: |
    - `+[2, 4, 6, 8]` ->SPELLSTRIKE<- or ->HASTE<- for each `[5+, 5+, 4+, 4+]` on ->CHANCE<-
---
component: command
name: INVIGORATE
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 40 HEALTH
requirements:
- 2 ANATOMY
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- SELF
- WOUND
description: |
    Not only are you damaged, but much of it is self inflicted; The feeling of manipulating your own blood is quite exhilarating, though ...
effect: |
    - Gain `+[1, 2]` ->MOMENTUM<- or ->ETHERSURGE<- and the one not chosen becomes `0`
---
## `2400` **RESEARCH**
### REQUIRES `6` IN **PATH OF SACRIFICE**
---
component: command
name: HEMOTURGEY
nature: MYSTIC
aspects:
- MIRRAL
paths:
- PATH OF SACRIFICE
costs:
- 60 HEALTH
requirements:
- 3 ANATOMY
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- RESIST
- PROTECT
- WOUND
description: |
    Taking damage yourself, grasp your enemy's own blood and twist their body - mutilating and tearing at their figure to your whims.
effect: |
    - Assail a target for `[6d26, 9d26, 12d26, 14d26]` ->MYSTIC MIRRAL<- damage
---
component: command
name: TRANSFUSE
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 60 HEALTH
requirements:
- 3 ANATOMY
phases:
- ACTION
- LINKED
tags:
- ONCE
- SELF
- WOUND
rapid: true
description: |
    Wound your own body and manipulate the blood to match your foe; Taking on their properties.
effect: |
    - Gain your **target's** `PROTECTION` or `DEFENSE` as `SPELLFORCE` for each `[6, 5+, 4+, 3+]` on `CHANCE`
---
component: command
name: RECONSTRUCT
nature: MYSTIC
role: RITUAL
phases: []
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- "10,000 SILVER"
requirements:
- 3 ANATOMY
tags:
- ONCE
- FRIEND
description: |
    As blood drips down your fingertips, you control it like puppet strings to grab and move objects on the battlefield, or in the medical tent.
effect: |
    - Reattach, regrow, or exchange a natural part of one's anatomy that has been excised.
---
component: command
name: ABSORPTION
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 60 HEALTH
requirements:
- 3 ANATOMY
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- WOUND
description: |
    Your mastery over blood extends to other's as well; Soak up the ambient mana and energy drifting through the air, and add it to your vitality.
effect: |
    - Steal `[1, 2, 3, 4]` of your target's ->MOMENTUM<- or ->ETHERSURGE<-. This may exceed your maximum by `2x`
---
component: command
name: CRYSTALIZE
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 60 HEALTH
requirements:
- 3 ANATOMY
phases:
- ACTION
tags:
- PERSIST
- WOUND
description: |
    Tear out your own blood and spread it across a liquid surface, turning the area into hard crystal.
effect: |
    - Turn a body of liquid spanning `[40, 80, 120]` meters, regardless of how volatile, hot, or toxic it is, into a surface that can be walked on for `[3, 6, 9]` hours. This works on clouds, as well.
---
## `7200` **RESEARCH**
### REQUIRES `12` IN **PATH OF SACRIFICE**
---
component: command
name: MASOCHIST
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 4 ETHERSURGE
requirements:
- 4 ANATOMY
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
- UNSTABLE
description: |
    Your inflicted wounds and fatigue have built up; Release all of it on one destructive burst.
effect: |
    - Gain **SPELLFORCE** equal to `10%` of your **missing** **HEALTH**
---
component: command
name: KAMIKAZE
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 4 ETHERSURGE
requirements:
- 4 ANATOMY
phases:
- ACTION
- LINKED
tags:
- ONCE
- HOSTILE
- UNSTABLE
description: |
    Abandon all defenses and precautions; Go for ultimate victory.
effect: |
    - **FAIL** on a single `1` on **CHANCE**
    - **CRITICAL** on a single `6` on **CHANCE**
    - `1`'s and `6`'s cancel one another out, if rolled together.
---
component: command
name: BLOODMOON
nature: MYSTIC
aspects:
- AURAL
paths:
- PATH OF SACRIFICE
costs:
- 4 ETHERSURGE
requirements:
- 4 ANATOMY
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
- UNSTABLE
description: |
    Your power peaks to its highest when the battlefield is covered in casualties to draw from.
effect: |
    - Gain the **SPELLFORCE** or **FORCE** of each fallen ally.
