---
title: "Ancients"
url: "ancients"
group: "mystic"
---
# Path of Ancients

## `600` **RESEARCH**
### REQUIRES `0` IN **PATH OF ANCIENTS**
---
component: command
name: DETECT REALM
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
- PATH OF ANCIENTS
- PATH OF STARS
costs:
- 10 MANA
requirements:
- 1 EMPATHY
phases:
- ACTION
tags:
- ONCE
description: |
    Focus your energy and mana, and reveal if any minions from other realms are nearby.
effect: |
    - Discern if there are any lower class minions from other realms within `[30, 60, 90]` meters.
---
component: command
name: REVEAL STARLIGHT
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
- PATH OF STARS
- PATH OF ANCIENTS
costs:
- 10 MANA
requirements:
- "1 KNOWLEDGE: WORLD"
phases:
- ACTION
tags:
- ONCE
description: |
    Gather up the energy in your body and release it, creating a small area covered in tiny starlit dots that represents the coming night sky.
effect: |
    - Create an area `[10, 20, 30]` meters in diameter that contains an interactive map of the night sky, and its constellations.
---
component: command
name: ACTIVATE STARLIGHT
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
- PATH OF STARS
- PATH OF ANCIENTS
costs:
- 10 MANA
requirements:
- "1 KNOWLEDGE: WORLD"
phases:
- ACTION
tags:
- ONCE
description: |
    Utilize the light to activate special switches and tablets found throughout the world.
effect: |
    - `+[3, 5, 9]` when attempting to activate a **Starsigil**.
---
component: command
name: AWAKEN
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
- PATH OF STARS
- PATH OF ANCIENTS
costs:
- 10 MANA
requirements:
- 1 EMPATHY
phases:
- ACTION
tags:
- ONCE
description: |
    Touch an object that can be awakened with **DIVINE** energy, and bestow life into it.
effect: |
    - `+[3, 5, 7]` when attempting to awaken artifacts or spellforges from **Antiquity**.
---
component: command
name: VOICE OF THE LOST
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 0 MANA
requirements:
- "1 INTERPRET"
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- SELF
- HOSTILE
- FRIEND
description: |
    The words of the ancient, lost beings known as the **Forgotten** still linger on in the world to some. Though few can understand them, it is known that speaking them aloud can cause the very world to cower.
effect: |
    - Allows the attached command to affect targets that cannot hear you, if the effect of it must be heard to function.
---
component: command
name: REVEAL SHACKLES
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 10 MANA
requirements:
- "1 INTERPRET"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- FRIEND
description: |
    The realms often intersect, and what we know as spectres and demons are typically bound to ours by some object, locale, or duty. It is possible to get a bearing on what tethers an entity to this realm.
effect: |
    - Discern the cardinal direction of an object, if one exists, that binds an entity to the **Midground** within `[30, 60, 90]` meters.
---
## `1200` **RESEARCH**
### REQUIRES `3` IN **PATH OF ANCIENTS**
---
component: command
name: SOLVIEW
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: MYSTIC"
phases:
- ACTION
tags:
- AREA
description: |
    The agents of **Vomorthas** are most abhorrent of the celestial figures that imprisoned them. So engulf the area with the closest one you can manage ...
effect: |
    - An area `[24, 48, 64, 128]` meters around you becomes **day** time, and the sun can be seen within that radius for each phase this is maintained.
---
component: command
name: SOLSTEAL
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: MYSTIC"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- FRIEND
description: |
    It's unclear whether you intend to help or harm, but you may literally take the sun away from those that depend on it.
effect: |
    - Inflict `1x` **DARKNESS** upon a target pending a `[13, 21, 36]` **MIND** save.
---
component: command
name: BEGONE
nature: ASTRAL
aspects:
- SOVAL
- VORAL
paths:
- PATH OF STARS
- PATH OF ANCIENTS
costs:
- 20 MANA
requirements:
- "2 KNOWLEDGE: MYSTIC"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
description: |
    The powers that haunt the realms, both good and evil, can oftentimes become a nuisance. Those articulated in the arts of ancient and celestial powers can sometimes ...be called upon to deal with them.
effect: |
    - Impose `[1x, 2x, 3x, 4x]` **BANISH** on a spectre, ghost, or other entity that is possessing a person or place - stopping any effects their presence has for the moment.
---
component: command
name: EXPERIMENTATION
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 2 ETHERSURGE
requirements:
- "2 KNOWLEDGE: MYSTIC"
phases:
- ACTION
- RESPONSE
- TETHER
tags:
- ONCE
- SELF
- FRIEND
- HOSTILE
description: |
    The **Forgotten** conducted horrible, life-changing research on the species of this realm. These inflictions can begin to mutate and twist denizens of mortal planes like butter if not removed.
effect: |
    - Prevent a **VORAL** mutation from occuring. This does not work after the infliction has already begun. It may be used to help allies, or prevent enemies from mutating themselves, etc.
---
component: command
name: DESTINATION
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 2,000 SILVER
requirements:
- "2 KNOWLEDGE: MYSTIC"
phases:
- ACTION
tags:
- ONCE
description: |
    Strange gateways, portals, and arches appear all over the world - and many of them have the markings of the **Forgotten** and the **Shattered**'s tampering. It is unclear where they lead, but stepping through them ... could be a disaster.
effect: |
    - Throw `[1, 2, 3, 4]` small gemstones infused with **voral** energy into an active portal, vortex, or void and roll `[1d6]`. If you roll lower than the number of stones thrown, a sigil will appear nearby indicating the realm the portal leads to.
---
component: command
name: DOORSTOP
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 2 ETHERSURGE
requirements:
- "2 KNOWLEDGE: MYSTIC"
phases:
- ACTION
tags:
- AREA
description: |
    The **Forgotten** and the **Shattered** come into the world through strange gateways, and as long as they remain open, defeating them is impossible. Until cut off from their source of power, certain agents of both are quite literally like gods in this realm.
effect: |
    - Impose `[1x, 2x]` **STASIS** on a gateway to or from another realm - stopping anything else from passing through it while this effect is in place.
---
## `2400` **RESEARCH**
### REQUIRES `6` IN **PATH OF ANCIENTS**
---
component: command
name: FORLORNED
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 30 MANA
requirements:
- "3 KNOWLEDGE: MYSTIC"
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- SELF
description: |
    Harness the despair of being lost to time; The agony of being forgotten and not recalling who you were as you strike with a weapon, spell, or ability.
effect: |
    - Adds **VORAL** aspect.
---
component: command
name: FALSE HOPE
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 30 MANA
requirements:
- "3 KNOWLEDGE: MYSTIC"
phases:
- ACTION
tags:
- AREA
- HOSTILE
- FRIEND
description: |
    The spirits, entities, spectres, and shadows that have had everything taken from them by the **Forgotten** and the **Shattered** are desperate to reclaim what was lost. If you exude the same energy, they will probably come running to you in hopes that you can restore them.
effect: |
    - Bait most weak-willed intangible entities such as ghosts, spectres, and wraiths within `[30, 50, 70, 90]` meters of you as long as you maintain this ability. They may attack you if given opportunity.
    - This will not work on physical creatures or spirits of reasonable intelligence.
---
component: command
name: FORGET
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 3,000 SILVER
requirements:
- "3 BLUFF"
- "3 PERSUASION"
- "3 INTIMIDATE"
phases:
- ACTION
tags:
- ONCE
- HOSTILE
- FRIEND
description: |
    The **Forgotten** were named as such because they were imprisoned and left behind, and through hatred and malice they became obsessed with memory and history. This also caused them to delve into the art of taking it from others.

    Utilizing darkened ink with a strange mineral crushed into it, you may paint words on a target's body that will begin to seep through their skin, and slowly eat their memories.
effect: |
    - Erode a *(NPC)* target's last `[2, 4, 8, 16]` minutes of memory. This does not work on a target that has a discernible **destiny** *(player characters)*.
    - This effect may be applied multiple times, but its potency quickly diminishes. Additionally, it will cause irreparable brain damage in subjects with **extremely** low intelligence to begin with, often rendering them to repetitive stuttering and **extreme** paranoia.
    - All targets will try to resist, and the method of such will vary by the will of the target.
    - This will not work on non-living entities, such as Spellforges.
    - It is **very** easy for even an elementary level sorcerer or scholar to discern when this has been done to someone, so it is not a good method for committing crimes. Though the damage is irreversible.
---
component: command
name: NARRATIVE
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 1 FATE
requirements:
- "3 BLUFF"
- "3 INTIMIDATE"
- "3 PERSUASION"
phases:
- ACTION
- LINKED
tags:
- ONCE
- HOSTILE
- FRIEND
description: |
    A large part of the world's history is lost to time because the **Forgotten** saw fit to erase it bit by bit. While not as potent as their methods, the ability to suggest your own history over what really happened does offer certain ...advantages.

    Speak out to a target whose mind is being eaten away, and as their memories are depleted, perhaps your words will take root and leave them with a pleasant (or unpleasant) replacement. It is unclear why, but this is more powerful if spoken in **Voroth**.
effect: |
    - Suggest a *(NPC)* target's last `[8, 16, 32, 48]` minutes of memory if it is 'vacant'. This does not work on a target that has a discernible **destiny** *(player characters)*.
    - This effect may be applied multiple times, but its potency quickly diminishes. Additionally, it will cause irreparable brain damage in subjects with **extremely** low intelligence to begin with, often rendering them to repetitive stuttering and **extreme** paranoia.
    - It is possible that if enough of a being's psyche is overwritten, you will reveal something that should have stayed supressed. Use with caution.
    - This will not work on non-living entities, such as Spellforges.
---
component: command
name: GUIDANCE
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 30 MANA
requirements:
- "3 KNOWLEDGE: MYSTIC"
phases:
- ACTION
tags:
- AREA
description: |
    The Forgotten erased pieces of the known world, and thus hiding a history from us that they do not want discovered. Because the technique is not perfect, there are echoes of this history that can still be revealed.
effect: |
    - Reveal a ghostly vision of an area that has been erased from time for `[64, 128, 256, 512]` meters as long as you maintain this command.
---
## `7200` **RESEARCH**
### REQUIRES `12` IN **PATH OF ANCIENTS**
---
component: command
name: DEVOID
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 4 ETHERSURGE
requirements:
- "4 KNOWLEDGE: MYSTIC"
phases:
- ACTION
- RESPONSE
- TETHER
tags:
- AREA
- UNSTABLE
description: |
    Focus your power inward and beckon your memories, use them to deny the putrid disgust of the Forgotten's miasma.
effect: |
    - Emit a beautiful white pulse from your body that will form a literal wall of starlight around the party, and prevent the advancement of any of the bale miasma from the Forgotten.
---
component: command
name: REMINDER
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 4 ETHERSURGE
requirements:
- "4 KNOWLEDGE: MYSTIC"
phases:
- ACTION
- RESPONSE
- TETHER
tags:
- HOSTILE
- UNSTABLE
description: |
    Many agents of the Forgotten, the Shattered, and the Blameless were once normal entities themselves, or elementals of some kind. Sometimes, having a split second to remember that can make all the difference ...
effect: |
    - Allow a target a fleeting glimpse of the memories they have lost. Certain targets may be more receptive to this effect than others.
---
component: command
name: DECISION
nature: ASTRAL
aspects:
- VORAL
paths:
- PATH OF ANCIENTS
costs:
- 4 FATE
requirements:
- "4 KNOWLEDGE: MYSTIC"
phases:
- ACTION
- RESPONSE
- TETHER
tags:
- HOSTILE
- UNSTABLE
description: |
    The beasts and monsters flooding the world come in many types, but the most terrifying are the **LEVIATHAN** classification. They are often under the control of one of the Forgotten, and while the retainer is not usually close at hand ... some of these creatures are not happy about their situation.
effect: |
    - Divide the shadows in the mind of a creature, and allow it to choose which path to take. The more intelligent the base creature is, the more likely it is to elect freedom. This does not mean it will become non-hostile, however.