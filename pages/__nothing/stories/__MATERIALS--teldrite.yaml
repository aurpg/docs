---
title: "TELDRITE"
url: "teldrite"
group: "materials"
order: 0000
---
# TELDRITE

Wrug was a rather simple-minded **Greenskin** that spent most of his life being told how 'slow' he was. The `3rd Age` was a pretty harsh time for most of the races that were not beginning to manifest an affinity in magic, and while some of the **Darukai** had very natural proficiency with the burgeoning powers of mana that overwhelmed the globe, there were races like the Greenskin that proved to have ...other talents.

While not the brightest tools in the shed, the leather-fleshed race of warriors and barbarians certainly had some of the **sharpest**. Though there were a few scattered mystics in the culture, the majority of the tribes consisted of either warriors, farmers, or artisans.

Greenskin and their ...slowly 'warming' up allies, **Gnasha**, were known the world over for incredible strength. Even more so were the largest of them, the **Goliath**. But thanks to the diminutive **Gobu'rhin**, there were brief periods of time when the culture itself experienced breakthroughs in crafting that made even the Gelvans envious.

Having little physical power, and even less intellectual prowess, Wrug had spent his days toiling with mining; A role that better utilized his slow, tedious nature. He was content to chip at rocks with large tools, feeling accomplished by the quality of his excavations and was generally well-liked in his tribe. But he often wondered what was done with all the ore he mined.

It happened that one day, in the little land of **Astria**, Wrug struck an unusual vein in the northern mountains, far beyond what would later become the range north of **Cromwell**. His pickaxe hit a metal that it glanced away from, with sparks and blue light spreading out in response. Attempting to strike the stubborn vein again, he yielded little advance, and started to investigate further.

Now, Wrug was not the most courageous person in the world ... and his instincts told him to go seek help from the rest of the tribe. But there was an allure in discovering something new, and the Greenskin seemed to have a sensation that could only be described as _fondness_ for the glinting light from this odd yellow vein.

It was not gold, even he could discern that. They had of course heard of **Topaz**, a radiant stone that carried interesting conductive properties. It was a favorite of the village shamans that actually harnessed magic or mysticism, as it was useful for exacerbating **lightning**; But this ... this wasn't quite that, either.

It glinted like silver, but with the color of gold. It seemed to have a sparkly luster like topaz, but felt more like sapphire to touch. His pickaxe was doing no good, so he started to dig around the mineral until he could tear a piece of the vein away; And it was a struggle to behold.

Finally, after `3 days` of mining, Wrug sat down with dwindling oil in his lamp, deep inside a mineshaft that would probably not be visited by anyone else for weeks, hungry and exhausted ... but staring down in fascination at this beautiful metal that seemed to have an oozing property akin to **mercury**. It did not exactly take the shape of his hand ... but the longer he held it, the more it seemed to just ..._fit_ in his grasp.

For a moment, Wrug was simply overwhelmed with the covetous nature that befalls most mortal races. _It could bring power_, _It could bring glory_ - the thoughts of any typical person ran through his mind ... but very quickly, there was a sensation that surprised even the leather-hide miner. He was overwhelmed with a familiar compassion for the stone.

Not a desire to protect it, but it felt as if he had held it before, and what was more it seemed to him that the stone almost held some kind of life. The Orc did not even notice that the oil in his lamp had gone out `18 days ago` as he kept staring at, and running this vein through his hands; Nor did he seem to realize his body was wasting away from malnourishment. He was lost in some fantastic fascination ...

Then, after `83 days`, Wrug stood up - only to find his legs weak and deteriorated, atrophy having set in. He almost dropped the precious metal, but then something remarkable happened; Wrug's weak and failing body, trying to take steps forward, at first stumbled like a toddler unable to learn balance ... and then a rush of excitement overtook Wrug's entirety.

He was somewhere else completely; The feeling of wind pouring over his body was magnificent - like a waterfall crashing around him, his feet touched fresh grass and he saw beautiful sunlight like it was his first time outside. He looked around only to realize that he had no idea where he was ... and hunger continued to drive his weariness, mortality a threat that no shiny metal seemed to abate.

He glanced around for food, and quickly noticed a tree with apples. Reaching out, he found it a fairly effortless process to grasp for him - as if they were simply bending down to be near him. He grabbed an apple and ate, and then another. Wrug lost track of time, but when he stopped moving from tree to tree, eating their fruit, he glanced back to realize that he had cleaned all the fruit from over `280` trees (_of course, he couldn't count that high, so it was just 'a lot'_).

But he felt his strength fairly returned, and invigorated with discovery, the Greenskin started off into the sunset to find his village. Once again, in an amazing rush of adrenaline, he found himself somewhere else entirely - this time, a land completely foreign. It looked like a snowy place, like **Skors'sommr** had often been described. And what was more, the Greenskin was hungry again... **very** hungry.

Wrug did not realize what he had found, and never would. For the next `4 years`, there were just reports of a blur that kept coming by and eating farmer's crops, stealing clothes, clogging up toilets, and no one ever really caught a glimpse of what it was. Wrug was wandering from place to place, thinking it was still the same week he had found that mineral.

Eventually, mortality would take Wrug's life - coupled with the **immense** strain on his metabolism that the mineral he found put. When his corpse was found in the `4th Age` by astrologist-archaeologists **Aki** and **Hatoku** during their many travels, they were quick to recognize the amazing light it put off and take it to master masons.

The material he had discovered would come to be known as **Teldrite**, and it was truly an incredible find. Teldrite was a glittering golden-like metal that melted easily, but while hardened resisted force. It was not particularly strong on its own, but it took any amount of energy from whatever it came in contact with, added it to itself, and then returned it a thousand fold.

Wrug had attained **unfathomable** speed by holding the metal for so long, and as such his entire perception of time was twisted and ruined. He had burned his whole remaining lifespan out in just `4 years`. But he had found a material that could be applied to every trade known to man for amazing results.

Eventually, Aki and Hatoku would take the corpse of this Greenskin with them until they indeed found a village in **Astria** that was able to identify it based on cracks in the bones where the tribe ritually injected certain spikes into their men when they came of age, as a rite of passage. His name would go down in history, and he received the recognition truly due him.

Fortunately, his techniques were passed on. These techniques are used for creating items that use **Teldrite**, and are primarily loved by archers and spellcasters for the ability the material has to draw out speed and agility.

It's said that arrows can be made that move **as** lightning through the sky; And it is known that the first practices of using Teldrite originated in Sar-Sadoth, lending credence to the idea that it might be useful for creating tools that can create or destroy barriers on a massive scale.

Teldrite, while a mineral, can be crushed or folded, and even thinned to the point of being as soft as cloth. It is an excellent material for both light, medium, and heavy armor and carries the ability to dramatically improve the wearer's physical qualities.

Artificers greatly love Teldrite for the way it can quickly conduct mana, and allow them to build extremely sophistocated machinations without needing to worry about the energy overloading it. It is also very good at absorbing impact from magical effects.