---
title: "Gelvan"
url: "gelvan"
group: "blessed"
order: 2002
---
# Gelvan
Gelvan are a race of knife-eared, long-lived beautiful humanoids that typically hail from **Geldithyr**. They are divided into `3` sub-cultures; The **Au'gelvan**, the **Syl'gelvan**, and the **Mel'gelvan**.

The Au'gelvan are oftentimes devoted to magic and research, and are the dominant culture of the race; They populate **Elithmar** quite densely and are snotty, pristine, and condescending of other races, typically.

The Syl'gelvan are typically non-magic addicted Gelvan that broke off from the Au'gelvan to live in the forests and wilds - they tend to be experts at physical combat and especially archery.

The Mel'gelvan are typically dark skinned Gelvan that originate from secretive societies and secluded locales. They made their biggest mark on civilization with the discovery of the unique material **Osnium**; A mineral that reflects neither light nor sound.

All of the Gelvan cultures are well known for expertise in culinary ability, and high quality craftsmanship.
* * *
- `3d6+1` **SPEED**
- `3` **MIND**
- `2` **BODY**
- `150` **MANA**
- `50` **HEALTH**
- `2` **WIELD**
- `1` **HASTE**
- `1` **INTELLECT**
- `1` **DEXTERITY**
- `1` **KNOWLEDGE: MYSTIC**
- `2d3` **ATTACK**
* * *
---
component: command
name: DIVIDED
nature: KINETIC
passive: true
description: |
    The Gelvan race has divided into three divergent cultures over time.
effect: |
        #### SELECT ONE AT CREATION
            - Au'gelvan
                - Often devoted to magic and study.
            - Syl'gelvan
                - Typically forest and wild dwellers focused on archery and defense.
            - Mel'gelvan
                - Secluded, sneaky types that frequently prefer isolation.
---
component: command
name: LITHE
nature: KINETIC
passive: true
description: |
    Gelvan are extremely nimble and lithe; They are quick to move, and very skilled at subterfuge.
effect: |
    - Gelvan may escape an encounter by remaining hidden for only `2` phases.
---
component: command
name: SCHOLARS
passive: true
nature: MYSTIC
description: |
    Gelvan are born with natural affinity for research; They have long lives and devote much of it to study, regardless of their culture.
effect: |
    - Accrue `50%` more *"score"* in paths of commands learned.
    - Require `50%` less **research** to learn commands.
---
component: command
name: PRACTICED
nature: KINETIC
passive: true
description: |
    Gelvan have spent extreme amounts of time practicing and honing their arts.
effect: |
    - Gelvan do not experience **FAILURE** when using **UNSTABLE** commands unless it is stated directly as an effect of the command
---
component: command
name: AURALIGHT
nature: MYSTIC
passive: true
description: |
    Gelvan naturally have a mysterious, mystic light in their eyes.
effect: |
    - Gelvan may see through **DARKNESS**.
---
component: command
name: FOCUSED
nature: MYSTIC
costs:
- 1 ETHERSURGE
- 1 MOMENTUM
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- HOSTILE
- FRIENDLY
- RACIAL
description: |
    The Gelvan people have studied various arts, both magical and martial, extensively, and have become experts in its use.
effect: |
    `+` `[3, 5, 7, 9]` **SPELLFORCE** or **FORCE** for every `3+` rolled on your **CHANCE** *(minimum `1`)*