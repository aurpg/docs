---
title: "LONGJAW"
url: "longjaw"
group: "forms"
order: 8006
---
# LONGJAW
>![w]
===
#### MAY BE APPLIED TO ...
`UNDINE` `HISSITH` `HAULA’FIN` `GOLIATH` `ESPER` `LIFEDRINKER` `ROCKBORNE` `BRAHAMATHIAN` `MAKKAR`
===
---
+![OBTAINING FORMS]
## FORMS
Forms may be thought of as extended or supplemental **races**. They are, at times, special races that re-use the template for an existing race, or an advanced version of an existing race that endows it with special properties. Forms may be equipped on the equipment page, but are not typically treated like 'equipment'. They are still regarded, in most context, as races themselves.

Forms inherit the general feel, theme, and overall style of the race they are placed on. For example, a Form that is placed on top of an **ESPER** race would exhibit golem-like properties of a construct, and one placed over an **UNDINE** race would be aquatic in nature.

Forms are generally available for purchase from the game's public market.

<++>
component: alert
color: blue
text: |
    While Forms do cost in-game silver, all players begin the game with `1 FORM VOUCHER` they can use to pick one for free.
<++>

<++>
component: item
name: "LONGJAW"
sale:
- GOLD: 64
- OR: "1 FORM VOUCHER"
slots:
- FORM
description: |
effect: |
    Adds the **LONGJAW** form to your account.
<++>
---
It's not quite clear how long the crocodilian race known as the **LONGJAW** have been around. It is known, however, that **100%** of the recorded accounts of them have been soiled with evil and malice.

It is believed the Longjaw were created by experiments during the **DUSK OF THE 2ND AGE** to emulate **SPELLFORGE** and **LIFEDRINKER** behavior in more primal races, like the **HISSITH**. Though accounts of them actually go much further back, leading some scholars to speculate that they were failed **PRIMALJA** experiments by the **FORGOTTEN**, in particular **SHALA-ROKE** who is famous for his brutal breeding and torture.

The Longjaw are essentially chimeric creatures that mimic the stature and size of some known races, but seem to be **excessively** spliced with **Hissith** blood to the point where they developed deformed features that resemble massive crocodiles. Long mouths and lidless eyes, bulky muscles, flared webbing between their fingers; Every non-human race was taken and tortured, experimented on, and their children forcibly bred to create these callous monsters.

Mindless and cruel, the Longjaw terrorized Nahauzca for much of its known lore. They would destroy, rape, pillage, plunder, and burn down everything in their path. The only exception seemed to be that they had strange awareness of souls - the Longjaw could be abated by allowing spirits to haunt an area, causing them to be easily spooked.

In the past thousand years however there has been a turnaround. It appears that whatever evolutionary process once graced the Hissith and Goliath has made its way to Nahauzca, and brilliant Longjaw began to surface. Very quickly the entire culture fractured in two.

Now, aware of their sordid past, there is a sect of the Longjaw that seeks to make amends and join the modern world. For obvious reasons, every culture on the planet is pretty hesitant to accept them.
* * *
You become the crocodilian-like ->LONGJAW<-, sporting vicious fangs and scales.