---
title: "ESPER"
url: "esper"
group: "ancient"
order: 1001
---
# ESPER
The enchanting **ESPER**, frequently called **AESOFORGES**, **SPELLFORGE**, and **GUARDIANS**, are constructed golem crafted to be living automatons. Originally crafted by the **EOSH** at the dawn of time, the first Esper was the world tree itself, ->AUDRASSIL<-. From its tears would come other races and it would give them instruction to manufacture new golems in their own likeness.

The Esper were the first race created, and as such, they pre-date the origin of **SOULS**. For that reason, they have no spirits of their own; But instead, they are simple constructions empowered by the realm of ->AESORIA<-. Espers original purpose were to be lore-keepers and holders of knowledge so that they could continuously pass it on to each generation of mortals. Over time, this purpose has been crudely twisted at many opportunities - but their original function seems to remain in some shape or form.

Espers come in many types, but most are modeled after male humanoids. Female styles do exist but are extremely rare due to how expensive and difficult they are to create. They also tend to have childish outlooks, being either very young or very old, with anachronistic levels of knowledge that betray their actual experiences. 

They are unable to reproduce, and while they are effectively regarded as immortal - no Esper except the dragon ->SCHALMARA<- and the world tree ->AUDRASSIL<- have ever been found to survive or function longer than `8,200` years. They typically expire at closer to `900` though. It is believed that they could live perpetually, were they maintained with enough care.
* * *
- `3d6+1` **SPEED**
- `4` **BODY**
- `2` **MIND**
- `85` **HEALTH**
- `85` **MANA**
- `2` **PROTECTION**
- `2` **DEFENSE**
- `3` **WIELD**
- `1` **STRENGTH**
- `2d3` **ATTACK**
* * *
---
component: command
name: FORGED
nature: KINETIC
passive: true
description: |
    Espers are created from materials available to the realm.
effect: |
    #### SELECT ONE AT CREATION
    - STONE
    - METAL
    - BONE
    - WOOD
    - CRYSTAL

    The materials an Esper is made of may be relevant during gameplay.

    Espers may not reproduce in **any** way.
---
component: command
name: METERED
nature: KINETIC
passive: true
description: |
    Espers have no muscles; They are built to achieve certain levels of motion and strength, and their glyphs will compensate to ensure this, within reason.
effect: |
    - Espers are always considered **BAREFOOT** and **NAKED**, even with equipment.
---
component: command
name: ARTIFICIAL
nature: MYSTIC
passive: true
description: |
    Espers are born out of intricate application of glyphology, artificing, and runescribing. They are incredible constructs of mana and energy.
effect: |
    - **IMMUNITY** to **breathing**
    - **IMMUNITY** to **diseases**, **plagues**, and **sickness**
    - **IMMUNITY** to **edible** **consumables**
---
component: command
name: RUNESCRIPTION
nature: MYSTIC
passive: true
description: |
    Espers are covered in glorious, perfectly crafted **runes** that amplify their power greatly.
effect: |
    - Espers do not fall `INERT` when reaching `0` `HEALTH` or `MANA`
    - When `MANA` or `HEALTH` is at `0`, an Esper may use the other pool instead; Effectively linking the two together.
---
component: command
name: EXPANSIONS
nature: MYSTIC
passive: true
description: |
    Espers, being powered by glyphs, are more adept at impressing them upon their bodies and getting them to work with existing runes and glyphs, as well as other types of trinkets that conduct mana or energy.
effect: |
    - Espers gain `2` extra **GLYPH** slots.
    - Espers gain `1` extra **ACCESSORY** slots.
    - `+` `1` **WIELD** each `75` Levels.
    - `+` `1` **DEFENSE** each `15` Levels.
---
component: command
name: REPAIRED
nature: KINETIC
passive: true
description: |
    Espers are artificial beings, and as such, they can be put back together if they are destroyed. But this can get pricey...
effect: |
    - Espers do not spend **FATE** to be resurrected.
    - Espers spend `300%` `SILVER` when being **resurrected** or when **MEND** is used.
---
component: command
name: ETHERA
nature: MYSTIC
passive: true
description: |
    An Esper may summon up the immense reserves of mana in their own bodies to hold themselves together even when they should logically be destroyed.
effect: |
    - Espers recover `24` `HEALTH` whenever they use a command.
---