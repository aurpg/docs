---
title: "DONATE"
url: "donate"
group: "support"
order: 9999
css: "mnu-bottom"
---

# DONATE

If you like the Auraverse, please consider supporting it. The game costs a considerable amount of money each year to run and develop.
---

>![i]
===
### YOU CAN DONATE AT [https://ko-fi.com/caterplease](https://ko-fi.com/caterplease)
===

I feel that if I am going to ask players for any of their assistance, they deserve to know what it is being spend on, so here are the game's expenses broken down.

# FUNDS GO TO ...

## KENDO UI
#### COMPONENT SUITE FOR ANGULAR

[KENDO UI](https://www.telerik.com/kendo-angular-ui/components) is a suite of tools for enhancing [ANGULAR](https://angular.io) applications. These tools reduce development time and add higher level functionality to the website, both new and old. The license is $800/year and gives us many of the controls we make use of today, and many more we'll be using on the new site, such as advanced grids, charts, and better drag and drop.

## AMAZON WEB SERVICES
#### PREMIUM WEB SERVICES
[AWS](https://aws.amazon.com) is a platform for hosting web services, applications, and content owned and operated by Amazon. The Auraverse is presently hosted across many servers for cost efficiency, but newer parts of the framework are being hosted on Amazon's EC2 containers (Ubuntu), S3, Glacier, RDBMS, and Lambda services for greater performance and functionality.

Good hosting is considerably expensive, and improves every facet of the game.


## RAVENDB
#### 2ND GENERATION NO-SQL
[RavenDB](https://ravendb.net) is the choice database for the Auraverse system. The database itself does not cost us anything, but the storage space and server to run it are considerably expensive.

The current site runs on a poorly designed Raven 3.5 system that consumes several terrabytes of space due to poor replication decisions and design flaws known as the ["God Object"](https://en.m.wikipedia.org/wiki/God_object) but the new site will be hosted on a sleek and optimized Raven 4.0+ instance on AWS using a much better designed model that is more maintainable.

Good database hosting determines how quickly the website can find information, and build your character sheets. It also directly impacts site performance in every other way.

## MEDIATR DATABASE
#### COMMAND MEDIATOR FOR .NET CORE
The new site features better performance by using new designs centering around programming patterns known as "mediators", making use of a popular library known as [**MEDIATR**](https://github.com/jbogard/MediatR). This does not require extra equipment, but is greatly improved by a database. This is an optional upgrade the the site will use when it's available, but will fall back to normal methods when it isn't.

The mediator database improves the website and tool's abilities to handle errors and how it falls back on practices that protect from data loss, and makes the development process more maintainable, as well as improves testing capability.

## HEROKU
#### MICROSERVICES FOR APPLICATIONS
We are *temporarily* using [HEROKU](https://www.heroku.com) for some services. Heroku is a weaker hosting platform for rapidly distributed code dispatch that gets updated frequently and plugs into programming environments. Presently, it runs our documentation engine. When I can afford to switch everything to AWS wholesale, this will not be necessary any longer.

Web services allow for the hosting of more complicated tools, such as the documentation compiler, the logs hosting, and the actual sheets compiler itself.

## JETBRAINS WEBSTORM / RIDER
#### WORLD CLASS DEVELOPMENT ENVIRONMENTS
[WebStorm](https://www.jetbrains.com/webstorm/) and [Rider](https://www.jetbrains.com/rider/) are my primary development tools, both featuring suites of utilities to make coding more efficient and less prone to errors. We also extensively make use of [dotCover](https://www.jetbrains.com/dotcover/), [dotPeek](https://www.jetbrains.com/decompiler/), [dotTrace](https://www.jetbrains.com/profiler/), and early beta builds of **datalore**. 

Development Environments are the core editors that programmers use to write software.

# HOW TO HELP
So please, I could use the help. I want to keep money out of the game, and avoid pay-to-win scenarios. But these costs are very much a big part of my life and all the assistance helps.

---
component: alert
color: red
text: |
    There are presently no in-game rewards for donating; Just our appreciation. We want to avoid a pay-to-win game model.
---



