---
title: "ACCESSORY"
url: "equipment/accessory"
group: "EQUIPMENT"
order: 2006
---
# PUBLIC MARKET

These items are freely available for purchase from the public markets in the game.

## ACCESSORY

Accessories typically fall into categories that nothing else can.

>![w]
===
Most characters have `1` **ACCESSORY** slot. Some races have different amounts.
===

---
component: item
name: "HOPEFUL BALLAST"
origin: "MADNESS OF VARSOGG"
sale:
- GOLD: 6
statistics:
- ATTACK : 1
- HASTE: 1
- HEALTH: 8
slots:
- ACCESSORY: 1
materials:
- TELDRITE
types:
role: ACCESSORY
description: |
    This accessory looks to be a very humble and minimalist rod that easily affixes to many kinds of surfaces but seems especially easy to hold in place against a body that generates its own source of **Mana**. It is made of **Teldrite**, a material that has the eerie effect of giving the affected a somewhat unique perception of time in extremely small bursts.
    
    The ballast seems inert and useless until held close to a source of mana, such as a person or an artifact that generates it. When reached for, it will consistently try to manifest a swirling buckle towards a moving object that possesses mana identical to what is powering it, acting like a chord that may be torn.
    
    When the chord is torn to the point of snapping, the energy generated between the ballast and the source of force will erupt around the wearer in a magnificent streak of silver shocks, causing their body to become further and further invigorated and dramatically boosting perception, confidence, accuracy, and strength.
    
    This was created by the **Kilniri** civilization in **Archea** as a means of helping the sickly recover strength more rapidly, and curing illness. While the discovery would go on to make them a very medically focused society, their early creations were fantastic tools for other purposes.
commands:
-   name: "HOPEFUL RESOLVE"
    css: command-nested
    role: passive
    description: |
        You feel at your peak physical condition for an instant when using this artifact, and confidence surges through you.
    effect: |
        - Recover `8` ->HEALTH<- for every `4+` on ->CHANCE<- when using a command.
---
component: item
name: "ELEMENTAL EFFUSION TANK: LIGHTNING"
origin: "MADNESS OF VARSOGG"
sale:
- GOLD: 2
statistics:
- FORCE : 1
- HEALTH: 16
slots:
- ACCESSORY: 1
aspects:
- LIGHTNING
materials:
- CHIRALITE
- TELDRITE
description: |
    This is a small canister of a strange liquid laced with **Chiralite** fragments and imbued with a surging pulse of **Lightning**. It burns with bright golden energy and crackles forever, never seeming to tire or exhaust as the storming bolts within launch around in endless gain of power and charge.
    
    When simply held, it causes one's body to feel invigorated and strong. But it is most effective when plugged into an **Elemental Effusion** firearm to augment it with **Lightning**.
effect: |
    Any **kinetic ranged** attacks from an `Elemental Effusion Device` always have `Lightning` aspect.
---
component: item
name: "ELEMENTAL EFFUSION TANK: FIRE"
origin: "MIRACLE"
sale:
- GOLD: 2
statistics:
- FORCE: 1
- HEALTH: 16
slots:
- ACCESSORY: 1
materials:
- CHIRALITE
- MESOTHIR
description: |
    This is a small canister of a strange liquid laced with **Chiralite** fragments and imbued with a surging pulse of **Fire**. It burns with bright crimson energy and warbles around like a lava lamp, never seeming to tire or exhaust as the blistering boils of bubbling heat pop and fume with gaseous repose.
    
    When simply held, it causes one's body to feel invigorated and strong. But it is most effective when plugged into an **Elemental Effusion** apparatus to augment it with **Fire**.
effect: |
    Any **kinetic ranged** attacks from an `Elemental Effusion Device` always have `Fire` aspect.
---
component: item
name: "HAUNTCLEAVER"
origin: "MADNESS OF VARSOGG"
sale:
- GOLD: 3
statistics:
- SPELLSTRIKE: 1
- SPELLFORCE: 1
- MANA: 8
slots:
- ACCESSORY: 1
materials:
- ORIHALCUM
description: |
    This small artifact looks suspiciously like a knife or a curved ulu, but the edge is dull and seems more spangled with some manner of dust and gripping treads than actual properties for cutting. It is found in a black sleeve with a silver buckle to unlatch it and has the fittings to fit easily on a belt or be tethered to the body in other manners.
    
    The Hauntcleaver is a strange tool carved from **Orihalcum**, a material that has properties to resist magic and temperature, but also interact heavily with spiritual realms. This item is a medical device invented by the **Kilniri** to deal with patients who became possessed by overzealous ghosts of the past looking to overtake the body or impose their will upon others.
    
    The artifact, when held, emits a sudden spark of red light as the grip on it is increased, and a pulse of crimson tears up and down the holder's arm, granting them a moment where they can touch the otherwise incorporeal. The tool is then used like a scalpel to surgically remove a ghost or specter from a possessed host without causing physical harm.
commands:
-   name: HAUNTCLEAVE
    css: command-nested
    aspects:
    - AURAL
    phases:
    - ACTION
    - LINKED
    costs:
    tags:
    - ONCE
    - FRIEND
    - HOSTILE
    - MELEE
    description: |
        Attempt to sunder the forced chains a specter has placed on a target.
    effect: |
        Remove ->HAUNTED<- effect from one target.
---
component: item
name: "BANDED ROPE"
origin: "STONESTHROW CLEARING"
sale:
- GOLD: 1
statistics:
- "KNOWLEDGE: NATURE" : 1
- ACROBATICS: 1
slots:
- ACCESSORY: 1
materials:
- COTTON
- LITHOSILK
description: |
    Every Adventure needs a rope, it has so many practical uses. You can climb with it, raise a tent with it, trap animals with it, tie people up with it, etc. The limits are endless! This rope is made of banded leather that is fortified with knots woven from Cotton and Lithosilk to form a material that is lightweight and easily thrown against the winds, as well as easy to wash and maintain a durable outer sheen.
effect: |
    The banded rope is plain, and it is not the longest rope available; Covering only about 250 meters in length, but it is practically indestructible unless directly damaged by sharp objects. The introduction of Lithosilk to the design makes it resistant to weight and so the chance of it fraying and snapping is almost zero.
---
component: item
name: "SQUEEZERNOODLE"
origin: "TEMPLE OF UR-YAHAD"
sale:
- GOLD: 2
statistics:
- DEFENSE : 3
- CHARM: 3
- FORCE: 3
slots:
- ACCESSORY: 1
materials:
- GLORIAN
- BOILBUTTER
- SALT
- SYLCLOTH
- AESOSTONE
story: |
    The **GNASHA** deal in all manner of trinkets and baubles with unusual effect that is the result of their ...unique ability to procure certain relics. They are especially fond of taking high paid requests and plunging families into bankruptcy to *"create"* new things that would otherwise be impossible to find.
    
    A common request among most mortal races are ones of vanity; A want to look younger or appear stronger - to woo that certain someone or clear up the skin, or sometimes bulk up rapidly in preparation for a tournament or feat of strength.
    
    Whatever the case may be, the Gnasha have at their disposal various substances with which to do the impossible. Having found a way to rapidly harvest **GLORIAN** from the **HOPELESS DROP** in **NAHAUZCA**, they combine this miracle thread with other mystical artifacts to create cloth and weave that may be dipped in **BOILBUTTER** to soak in the life-force of *whatever* the butter was made from.
    
    It's best you do not know what the butter was made from. You're going to get what you want, so just leave it alone.
description: |
    This is a slimming belt of **SYLCLOTH** and **GLORIAN** that takes on magnificent, glamorous style and effect of any color your mind wills upon locking it in place. The buckle is made of **AESOSTONE** and it has a slick sheen from being soaked 3 months in **BOILBUTTER** and then heavily salted to give it textures before cleaning.
    
    Accurately called the **SQUEEZERNOODLE**, this dope-rope makes you look younger, buffs your physique, clears your skin, and gets rid of blemishes. The buckle to the belt is stylish itself, able to be adorned with glittering glow and sparkling mystic effects if the buyer wishes.
    
    This is better than a diet; It's a magic noodle.
effect: |
commands:
-   name: "SLIMMING"
    css: command-nested
    passive: true
    description: |
        This belt will make you look more trim and fit, shaving years off in an instant as if it is magical!
    effect: |
        - Your age becomes `26` unless you are younger than `16` - in which case the belt must be affixed by a parent or guardian. It will not work on teenagers, but it could still look stylish.
        
        >![e]
        ===
        This effect may wear off if you run out of mana or FATE.
        ===
-   name: "BELLE"
    css: command-nested
    passive: true
    description: |
        This noodle will make your fit and svelte body perfectly prime and accent every feature with the utmost detail. You are more beautiful, more handsome, more eloquent, and even more intellectual.
    effect: |
        - `+2` ->EFFORT<- when performing commands that cost ->PRESENCE<-.
-   name: "UNAPPRECIATED HELP"
    css: command-nested
    passive: true
    curse: true
    description: |
        **UNKNOWN** to the wearer, this belt is taking these properties from someone else.
    effect: |
        - Someone in the world looks uglier, weaker, and loses confidence due to you. But you don’t know that. Your player does ...but not you. Oh ...such shame. You galavant in ignorant bliss with your perky breasts and clear skin - if only you knew.
---
component: item
name: "METESILK"
origin: "CISTERN OF ORIGINATION"
sale:
- GOLD: 28
statistics:
- HASTE : 1
- SPEED: 1
slots:
- ACCESSORY: 1
materials:
- CHRONOSILK
- SYLCLOTH
- OSILLIUM
- OSNIUM
types:
- CORSARIAN
- RIBBON
story: |
    Like all things in creation occur in rising and fall, day and night, dark and light, and other such reflections, so too do good and evil. While the origins of the **SPRITE** race are largely unknown, the resurgence of knowledge brought by the discovery of the **TABLETS OF UR-YAHAD** and the wisdom of **SONDRA ALAMARA** have shed new light on where these beautiful creatures hail.
    
    Just as the **FORGOTTEN** were formed from the sorrow and dismay of the realm **VOMORTHAS** being cast aside and re-written poorly - supposedly following instructions incorrectly given by the **REPLACED**, the same power from **MEOFRIN** that evolved the **PRIMALJA** and birthed the wicked **FORGOTTEN** took the good, hopeful emotions lost from Vomorthas and it too beheld a form all its own.
    
    The folktales that Sprites come from the stars themselves holds more merit than most anyone realizes; A fact that would not come to light until the return of Sondra Alamara from the World Tree, **AUDRASSIL**. Upon her presence returning to the normal plane, many great evils awoke in the world - hungry to snuff out knowledge she possessed.
    
    During the **DAWN OF ARCHAEA** - or rather, the first moments of time between the **DUSK OF PRIMERA** and the arrival of the **FORGOTTEN**, a great darkness descended upon the realm of **MIDGROUND** that appeared to the **DRAGONS** as a series of falling meteors. Unseen to all but **YLDRERTH** were the lights alongside them - and as the Forgotten plummeted into the realm, so too did a spangling of glorious white glitter splash out across time.
    
    While the Forgotten possess the power to retroactively remove moments in time, so too the Sprites would be inversely interwoven with the temporal the realm. They possess the ability to not only defy this power, but it is believed they can awaken the power to actually reverse it. This fact was learned much too late, and the method of ascension has never been discovered - **VARSOGG** imparted all he could to **PHIAL**, the then-Princess of the Sprites, before madness took him entirely.
    
    And then the dragon succumbed to the forgotten in utter depravity.  However, where great evils awaken, so too do great heroes. The **SOLGUARD**, celestial keepers of the secrets in the heavens, began to appear all over the world. These take two distinct types of Sprites. **SUNSET** - who possess dark skin akin to the night sky, with wings that glitter in the pattern of various constellations; And **SUNRISE** - who appear as sparkling lightning that dances and sings comparatively.
    
    The SOLGUARD tend the gates of **ELEXIN**, and therefore have the power to both foresee and even alter **fate** and **destiny** in ways no other creature throughout time ever has. Though sworn to secrecy, they are becoming hunted for this coveted power to defy creation - with the unfortunate **SUNSET SPRITE,** **"RIBBON"** being captured by a band of marauders from **CORSARIO** and forced to protect them during battle to keep her life.
    
    Ribbon was somewhat freed by one of the **UNHEARD** - but left in her prison until someone arrived to claim her from the attached specter. This particular branch of the Unheard was the dreams of a little girl that lived during the **2ND ABANDONMENT** and was the constant victim of pirate attacks - but had dreams of becoming an explorer.
    
    When this Unheard encountered a worthy vessel to embolden its dreams, it was able to find satisfaction and in turn, relinquished the sprite into their care.
    
    The grateful sprite is overjoyed to be rescued but try as one might - this merry murky pixie's bottle just can't appear clean. It's always such a hazed, inky black tint that matches the sprite's own aura.
    
    **RIBBON** is a **SUNSET** daughter of **PHIAL** - the fallen Princess of the **1ST AGE** that set **HOLLISTER** on his quest to unite the races and take a stand. While Hollister would not finish the war, he would cause the enemy to take a step back - a feat he achieved with Ribbon secretly whizzing around the battlefield like a mobile bullet, alongside his other companions.
description: |
    This is a **CORSARIAN RELIC** from the **DUSK OF ARCHAEA**. It is a fashionable ribbon of special pink and white silk that forges a bracelet or a belt. It is non-descriptive and plain until night time.
    
    During the hours of dusk until dawn, the silk resonates with infinitely brilliant gold and green light. The presence of it alone is enough to intimidate some smaller creatures, but it empowers the bearer with confidence and passion to unleash devastation upon the forces of darkness.
commands:
-   name: "SUNSET PIXIE"
    css: command-nested
    aspects:
    - MIRRAL
    nature: MYSTIC
    phases:
    - ACTION
    tags:
    - PERSIST
    - SELF
    description: |
        As a bearer of one of the sunset sprite ribbons, you may manifest that form at will.
    effect: |
        - You may activate this power to appear as a SPRITE that may hover above the ground at the same height as your normal race. 

        >![e]
        ===
        You cannot fly, however. You are still beholden to gravity.
        ===
---
component: item
name: "PRIMAL FIELD MEDICINE GUIDE"
origin: "STARLIGHT FESTIVAL"
sale:
- GOLD: 8
slots:
- ACCESSORY: 1
materials:
types:
story: |
    They're not very good with books, formulas, or even painkillers ...but if you've got a Riyuilian doctor, you're in good hands. It's going to hurt like hell, and you'll pass out ...probably scream many obscenities, and there's no question that you're going to be an enemy of the doctors by the time it's over.
    
    ...But you're maybe going to live! There's no substitute in the universe for sheer, unbridled, unfettered willpower. Nothing can stand in place of someone who just has such a strong desire to live that they completely, utterly reject the mere concept of failure.
description: |
    It might help if they could spell "failure". But that's okay! This guide has everything they need to know! They can see the bones, and both of your penises; Humans have two, right? Oh, no that's Hissith. Now the brain is in the head part correct?
    
    You're an Esper? Lifedrinker? This probably isn't the right kind of doctor for you. But hey, we can give it a try, right? What are you going do, die?
effect: |
    >![e]
    ===
    GRANTS YOU THE ABILITY TO USE `MEDICAL` ITEMS FREELY
    ===

    ^^This version is not spell checked^^
---
component: item
name: "ARTIFICER'S DOCUMENTATION"
origin: "STARLIGHT FESTIVAL"
sale:
- GOLD: 8
slots:
- ACCESSORY: 1
materials:
types:
story: |
    Repairing Espers and Runescribed devices are what an artificer lives for. They thrive on the challenge, they excel under pressure, and they get paid **well**. The delicious feeling of touching a smooth stone or metal chassis that whirs with musical magic as they play fingers across its glyphs...
    
    But it's not a job that just anyone can do! It takes dedication, coffee, sugar, long hours ...it's thankless, tiring, difficult. Sometimes there's the occasional reward, like getting to repair some drop-dead gorgeous lady-Esper that broke down because of 'overuse' - but most of these are going to be crap jobs.
    
    Make this cog turn better! Make this glyph flow better! "Hey, can you make a sandwich shoot lightning too?" **No**. No, you cannot make a sandwich shoot --...
    
    ... Well, it may not be **completely** impossible.
description: |
    This is a handy guide book for the novice artificer looking to enter the field. It is filled to the brim with technical drafts, blueprints, and all manner of acronyms.
effect: |
    >![e]
    ===
    GRANTS YOU THE ABILITY TO USE `ARTIFICER` ITEMS FREELY
    ===
---

* * *
# THE SPICY CARROT
---
component: item
name: "SPICY WEDGE G-STRING"
origin: "SPICY CARROT"
sale:
- GOLD: 8
statistics:
- ENTERTAIN : 1
- STAMINA: 2
- HEALTH: 28
slots:
- ACCESSORY: 1
materials:
- SILK
types:
role: ADULT
description: |
    Underwear with a glittering golden carrot emblazoned on the front and the ability to ride up between the sweet crevasse like curious raindrops, the choice deluxe spicy G-string can only be worn by the exotic dancers that have steeled their mind for the pain and "tightened" their resolve to endure both the physical pain and the plethora of stares and whistles that come from sporting the blushing bluster of pretty much presenting a bare ass to the world.
    
    Maybe you're born with those thighs; Maybe this fabric brings them out; Maybe you're hot; Maybe this is so tight that you discover assets thought lost to time and evoke the legend of your inner pudge-like buttocks.
    
    Dance, little Carrot! Show the world what you can do! Clench your cheeks and endure the ride-up, for the sake of all the eyes everywhere, entertainers everywhere are counting on you to demonstrate the great resolve that turns a regular Carrot into a **Spicy Carrot**.
effect: |
    It’s a g-string - what did you expect? Your ability to jiggle upon taking damage is magnified several fold. This is truly a powerful piece of equipment. Are you Spicy enough to wear it?
---