---
title: "GLYPHS"
url: "other/glyphs"
group: "other"
order: 5001
---
# PUBLIC MARKET

These items are freely available for purchase from the public markets in the game.

## GLYPHS

Glyphs, runes, and symbols may be inscribed on the body with various inks in order to augment one's abilities.

>![w]
===
Most characters have `2` **GLYPH** slots. Some races have different amounts.
===

---
component: item
name: "STARSEEKER GLYPH OF THE PYRESPRITE"
origin: "VIRITII CAVERNS"
sale:
- SILVER: 1600
statistics:
- WISDOM : 1
- PROTECTION: 1
- MANA: 24
slots:
- GLYPH: 1
story: |
    The constellation of the Sprite presides over the March Zodiac on the calendar, and comes in many styles and shapes. Like many of the constellations, it has a different representation each month - with the most prominent appearing in March.

    The art of capturing the vision of a constellation is known as Starscribing, a subset of Runescription - the skill of Starscribing is an extremely difficult one to master, and as such the most common type of glyphs in this art are the novice crafted Starseeker glyphs. These are generally very good quality, but lack the expert artisan touch to be considered flawless.
description: |
    This marking bears the perfect form of the **Sprite** as it appears at its most pronounced time in the sky and is glittered with a glittering red ink to create what is commonly referred to as a **Pyresprite**. It is a symbol worn by many miners to ward off the cold and help prevent hypothermia. When worn, it leaves a beautiful trail of embers in its wake - although they are extremely small and safe to touch.
---
component: item
name: "STARSEEKER GLYPH OF THE DEVOUT UNDINE"
origin: "VIRITII CAVERNS"
sale:
- SILVER: 1600
statistics:
- INTELLECT : 1
- CHARM: 1
- MANA: 24
slots:
- GLYPH: 1
story: |
    The constellation of the **Undine** presides over the **January** zodiac, and is seen as a majestic Undine with a trident at its side, posturing a stance akin to giving a vocal performance to the other stars at its most visible.
    
    The art of capturing the vision of a constellation is known as **Starscribing**, a subset of Runescription - the skill of Starscribing is an extremely difficult one to master, and as such the most common type of glyphs in this art are the novice crafted **Starseeker** glyphs. These are generally very good quality but lack the expert artisan touch to be considered flawless.
description: |
    This marking bears the perfect form of the **Undine **as it appears at its most pronounced time in the sky and is glittered with a glittering green ink to create the mark known as the **devout Undine** - considered a position in the sky when the Undine is thought to be singing a song to the Goddess **Aquapheas**. It is a symbol worn by many sailors to ward off curses from the seas and to deter Leviathan from attacking.
---
component: item
name: "STARSEEKER GLYPH OF THE MONSTROUS GOLIATH"
origin: "MAROKAI PLAGUE"
sale:
- SILVER: 2600
statistics:
- WIELD : 1
slots:
- GLYPH: 2
story: |
    The constellation of the **Goliath** presides over the **July Zodiac** on the calendar and comes in many styles and shapes. Like many of the constellations, it has a different representation each month - with the most prominent appearing in July.
    
    The art of capturing the vision of a constellation is known as **Starscribing**, a subset of Runescription - the skill of Starscribing is an extremely difficult one to master, and as such the most common type of glyphs in this art are the novice crafted **Starseeker** glyphs. These are generally very good quality but lack the expert artisan touch to be considered flawless.
description: |
    This marking bears the imperfect form of the **Goliath **as it appears at its most pronounced time in the sky and is glittered with a glittering silver ink to create what is commonly referred to as a **Monstrous Goliath**. It is a very large glyph that is often seen painted on the hulking warriors of the Darukai.
---
component: item
name: "STARSEEKER GLYPH OF THE DRAMATIC WIGGLY"
origin: "MAROKAI PLAGUE"
sale:
- SILVER: 2600
statistics:
- INSTINCT : 3
slots:
- GLYPH: 1
story: |
    The constellation of the **Wiggly** presides over the **February Zodiac** on the calendar and comes in many styles and shapes. Like many of the constellations, it has a different representation each month - with the most prominent appearing in July.
    
    The art of capturing the vision of a constellation is known as **Starscribing**, a subset of Runescription - the skill of Starscribing is an extremely difficult one to master, and as such the most common type of glyphs in this art are the novice crafted **Starseeker** glyphs. These are generally very good quality but lack the expert artisan touch to be considered flawless.
description: |
    This marking bears the imperfect form of the **Wiggly **as it appears at its most pronounced time in the sky and is glittered with a glittering ghost blue ink to create a pose of a caterpillar in a very dramatic pose with feelers outstretched to acclaim praise; This is known as the **Dramatic Wiggly**.
commands:
-   name: "HUMBLE DRAMATIC ATTENTION"
    css: command-nested
    role: passive
    description: |
        The wiggly are renown attention whores, and as such they soak up praise and compliments endlessly. They are unbelievably playful and precocious, but gentle and wise. Wiggly are the world’s menders, and as such all of their efforts are focused on recovery and rejuvenation.
    effect: |
        - Recover `3` ->HEALTH<- each time you use a command during your own action and response phases.
        
        ^^You also get a cute little spotlight when you critical. It’s adorable - make sure to pose well, everyone will be watching!^^
---
component: item
name: "STARSEEKER GLYPH OF THE STAMPEDE"
origin: "MADNESS OF VARSOGG"
sale:
- SILVER: 2600
statistics:
- DEXTERITY : 1
- SPEED: 1
- HEALTH: 24
slots:
- GLYPH: 1
materials:
- INK
types:
role: GLYPH
story: |
    The constellation of the **Ni'vana** presides over the **December Zodiac** on the calendar and comes in many styles and shapes. Like many of the constellations, it has a different representation each month - with the most prominent appearing in December.
    
    The art of capturing the vision of a constellation is known as **Starscribing**, a subset of Runescription - the skill of Starscribing is an extremely difficult one to master, and as such the most common type of glyphs in this art are the novice crafted **Starseeker** glyphs. These are generally very good quality but lack the expert artisan touch to be considered flawless.
description: |
    This marking bears the perfect form of the **Ni'vana** as it appears at its most pronounced time in the sky and is glittered with a glittering white ink to create what is commonly referred to as the **Stampede** arrangement - a mark that denotes when the Ni'vana looks to be running across the stars at high speed. It is a symbol worn to pronounce one's natural agility and dexterity.
---
component: item
name: "MAKAARI RUNE OF POWER"
origin: "VIRITTI CAVERNS"
sale:
- SILVER: 1600
statistics:
- STRENGTH : 1
- ATTACK: 2
- HEALTH: 12
slots:
- GLYPH: 1
story: |
    The Makaar are known for their intense brute strength, despite being one of the smaller races. Not a single culture in the world would dare snicker or laugh at the posturing of how much muscle a fit Makaar can present - even the Goliath respect them as equals. It is said that some of the ancient Makaar could go toe to toe with mighty Aesoforges in feats of strength and not back down.
    
    While it remains to be seen if these legends are completely true, the fact that they can erect structures of such indescribably grand magnitude and beautiful design in such short time, with artistry that rivals the Gelvan's own, is testament to the claims.
description: |
    This is a Makaari rune inscribed on the body with the ->TWILISI<- word `SAN` - meaning **power**.
    
    While it cannot be fully proven if this mark actually bolsters one’s actual strength or merely evokes the courage to bring it out, the patterns it possesses do indeed react to the magic in one’s body - and glow a bright gold and bronze upon being etched to the skin. The muscles bulge and throb with strength at their presence.
---
component: item
name: "PRIMAL ZODIAC: VØYEN"
origin: "TEMPLE OF UR-YAHAD"
sale:
- GOLD: 58
statistics:
- SPEED : 3
- HASTE: 3
- SPELLFURY: 3
- INSTINCT: "++"
slots:
- GLYPH: 2
- FEET: 1
materials:
- PRIDEBLOOD
- ROYALNERVE
types:
- ZODIAC
- EVOLUTION
story: |
    The world changes and evolves as the passage of time takes a toll; It is said the stars, too, have finite existence. There is belief that once, long ago, the skies above the Midground looked quite different than today (barring the dragon combat) and in fact, the people in that era even acknowledged a **completely** different Zodiac.
    
    There are drawings on ancient tablets of a constellation that the symbols remarked as **VØYEN**. Roughly translated, this would in time become the **SKORS'SOMMR** basis for what in common is **VALOR**.
    
    It's unclear what happened to these stars, or why the accounts of them were purged. The running theory is that they were ancient gods that were displeased with our realm and left it to pursue better endeavors.
description: |
    This is a glyph patterned to slightly resemble a **LION** head, mimicking the ancient findings of a primal zodiac constellation that was said to dominate the sky between **JULY 23 - AUGUST 22**.
    
    Upon the glyph being put to the body, the host's legs will begin to burn and seethe with ludicrous power. As if they could run across the heavens themselves, the host will discover their feet rapidly taking on properties like a lion's.
effect: |
commands:
-   name: "PRIMAL ZODIAC: VALOR"
    css: command-nested
    aspects:
    - MIRRAL
    nature: KINETIC
    phases:
    - ACTION
    - RESPONSE
    - LINKED
    tags:
    - ONCE
    - SELF
    description: |
        You move like the amazing behemoths, seeming to have the ability to defy the landscape and terrain, your courage swelling in the process.
    effect: |
        - When rolling ->SPEED<-, recover ->HEALTH<- equal to the result so long as you are not fleeing an encounter.
-   name: "FELILE"
    css: command-nested
    aspects:
    - MIRRAL
    nature: KINETIC
    rapid: true
    phases:
    - RESPONSE
    - LINKED
    tags:
    - ONCE
    - SELF
    description: |
        Your legs and lower body have become similar to a Lion’s; This infers the properties of one, as well.
    effect: |
        - Ignore damage from FALLING or a challenge to ->ACROBATICS<-.
-   name: "EVOLUTION"
    css: command-nested
    passive: true
    risk: true
    effect: |
        - The Lion’s blood may take hold of you too strongly, and even begin to challenge your perception. You may be unable to resist the charms and commands of real Lions - and may someday become one, if you lose sight of your humanity.
---

* * *
# THE SPICY CARROT
---
component: item
name: "HONORARY CARROT BRANDING"
origin: "SPICY CARROT"
sale:
- GOLD: 6
statistics:
- CHARM : 1
- PERSUADE: 1
- SPIRIT: 1
slots:
- GLYPH: 1
materials:
- INK
types:
role: ADULT
description: |
    All the dancers at the Spicy Carrot are given a temporary glyph to mark their entry into the inner circle of those tasked with the noble goal of satisfying the lust for Haula'fever. It is applied to the inside thigh in light incandescent ink and has a cute scent like a carrot.
    
    After a few years of service, this mark is upgraded to a full out branding that becomes permanent and bestows the mighty Haula'fin with a dancer's raw, unbridled power! And while you're not able to just buy into the legend of the thick thighed succulent satisfactory of being a true Carrot, you can at least purchase this applicable tattoo stencil that is applied with magical ink and gives you the confidence of a pudge-powered short stack in skimpiness and sultry winks.
    
    It is advised to use caution with this marking - the power of Haula'fever is said to be extremely strong and may overwhelm the lesser willed among us.
effect: |
    This glyph goes on the inner thigh - you know where. Yeah, right there… don’t think you’re getting away with putting it somewhere else. You also get free lap-dances on your birthday.
    
    >![e]
    ===
    “YOU MUST REGISTER YOUR ACTUAL BIRTHDAY WITH THE CARROT. YOU CANNOT JUST WALK IN AND CLAIM EVERY DAY IS YOUR BIRTHDAY”
    ===
---