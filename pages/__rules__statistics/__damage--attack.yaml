---
title: "ATTACK"
url: "attack"
group: "damage"
order: 3001
---

# ATTACK

A character's [->KINETIC<-](/#/rules/nature) **MELEE** damage capability.

## BEHAVIOR

When executing a `MELEE` attack, roll your `ATTACK` dice after [->CHANCE<-](/#/rules/chance).

## GET

All player characters start with an `ATTACK` based on their race. It improves by `+1` each `4 LEVELS` earned.

## RULES

The following rules must be observed, when using `ATTACK`.

- You may not roll `ATTACK` unless using a command with the `ATTACK` tag.

>![w]
===
All characters have a core command called `ATTACK` that fulfills this requirement, at minimum.
===

## WARNING

The following warnings should be observed with `ATTACK`.

- You may not roll `ATTACK` for a command with the `RANGED` tag.