---
title: "CRYSTALLING"
url: "crystalling"
group: "annual"
order: 10001
---

# CRYSTALLING

^RUMOR OF THE SHATTERED VROMSHALE^	
^^*AESORIC ADVENT*^^  
^^`OCTOBER`-`NOVEMBER`^^    
* * *

A world that has tapped into mystic forces tends to cause ripples across its own ecosystem; At times there manifest consequences of the acts of history frequently referred to as an **ADVENT** or **PHENOMENON** - a marked occurrence with rumored source or cause, but littered with conjecture and lack of empirical backing.

One such ADVENT intersects **MIDGROUND** and **AESORIA** at any point between the **CHILDREN'S MOON** (September/October) and the **MOTHER'S MOON** (October/November) that reveals a location known as the **VROMSHALE ESTATE **- an eerie plot of land that vanished inexplicably from **CORSARIO** at an untold point in time.

Legend portends that the Vromshale estate was a glorious, luxurious manor built on-top of the most fascinating plot of crystalline hillsides one could hope to witness in their lives. Stepping to the balcony of the manor's master bedroom and looking out, one would see sparkling rainbows spangling the whole of creation from the sheer morning dew collected on the naturally growing exotic flora.

The owners of the Vromshale estate remain unknown in name; Even the name of the actual family seems to have been stolen away to time - the zone receives its name from the saddening origins of the **CRYSTALLING ADVENT**.

Early in the **DAWN OF THE 5TH AGE**, in **ASTRIA**, a mother was traveling by her three small children along a relatively safe road between **MIDPORT** and **FLAMESPIRE** late at night. Their carriage inextricably collapsed and all four wheels seemed to bow to the point of risking damage to the family. Burdened with a small baby, and two hungry children aged 6 and 9, the mother needed shelter for the night.

To her alarm she looked up to see a dark, grim, imposing hillside of glinting black crystal - a shade somehow visible even in the depths of such a night. The sound of whispers invited her to safety, and ravenous howls of angry animals threatened to pursue if she did not accept. Without hesitation, the mother retreated into the household with her three offspring.

There they found no staff or life; An empty, clean manor fit with royal preparation. Meals were at their waiting and the rooms spotless - bright, cheerful, and entirely overflowing with luxury, the family discovered a hot meal like none they had ever known waiting to meet them. The children began to eat, but the mother was hesitant ...having once been a bit of an adventurer herself.

At last the mother's fears would become realization as she watched her eldest son of 9 falls into shattered glass and crystal before her - the food he had enjoyed taking away his mana, his soul, and eventually his body - to give more power to the **SPECTRAL SHATTERED** that formed, and reached out with eerie rock-like hands to grip the quivering arm of the 6-year-old daughter.

The mother prepared to fight, and clutched her newborn close as she drew concealed weapon from her ankle; But the **shattered** merely lifted a hand and dismissed them each, speaking only the word **"VROMSHALE"** before the doors opened and they were forcibly swept away - their carriage repaired, their son lost, and their hearts destroyed.

The manor vanished in an instant. A flicker of light - a glass-like screeching as nails on a chalkboard whilst it turned to but a vapor in front of them. The forlorn mother dedicated the rest of her life to this phenomenon.

It is known that somewhere in **ASTRIA**, the mother now lives - having recovered her son from the clutches of the SHATTERED with his life intact. But she is forever changed - every year, she prepares the people for the appearance of the Vromshale Manor - for every year, no less than **30** children are lost to it since then.

The mother's name is **ESCAROL**, and she lives as a secluded weaponsmith on the outskirts of **HORIZON**, producing weapons armed to rend ghosts and crystal - alongside her recovered son, who is still half crystal himself.

During this time of year, adventurers steel themselves to search for the estate - and aim to seek through its confines for children they may reclaim; Or clues to the Spectral Shattered itself. Some have, unfortunately, lost their lives encountering it directly.

Some citizens choose to participate from a safer venue; Instead doling out treats to children and families to bolster their spirits and courage during this time of year - either to fortify resolve, aid the cause, or boost morale. The CRYSTALLING ADVENT has become a seasonal occurrence in Astria.