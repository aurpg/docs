---
title: "LANGUAGES"
url: "languages"
icon: "translate"
group: "culture"
---
# LANGUAGES

There are numerous languages across the game world that characters may interact with.

| |  |
|:------|:-------:|
| `COMMON`   | STANDARD, NORMALIZED LANGUAGE USED BY MOST CIVILIZATIONS |
| `HONOSHIAN`   | TRADITIONAL DIALECT OF HONOSHIA |
| `FRYSTAL`   | TRADITIONAL DIALECT OF SKORS'SOMMR |
| `AURIN` | ELOQUENT LANGUAGE SPOKEN BY THE AU'GELVAN, SYL'GELVAN, AND MEL'GELVAN |
| `DUSQUE` | HARSH LANGUAGE SPOKEN ONLY BY THE MEL'GELVAN |
| `PETRASH` | TRADITIONAL LANGUAGE OF MOST MAKKAR |
| `JONTUN` | ARCHAIC LANGUAGE SPOKEN BY GIANTS, SUCH AS THE SKY'RYL |
| `CANTIN` | ENCRYPTED CHAIN-SYLLABLE LANGUAGE SPOKEN BY THE UNDERGROUND AND SCOUNDRELS |
| `GNASHA` | COMMON LANGUAGE OF THE DARUKAI |
| `GOBNASH` | COMMON LANGUAGE OF THE GOBU'RHIN THAT HAVE JOINED THE DARUKAI |
| `HAUFFLUENCY` | BASICALLY LANGUAGE OF NOTHING BUT FOOD ITEMS USED BY HAULA'FIN TO CONVINCE OTHERS THEY SHOULD STOP AND EAT |
| `THRGNASH` | TRIBAL SUBCULTURAL LANGUAGE OF DARUKAI THAT COME FROM NAHAUZCA |
| `HISTEEL` | COMMON HISSING LANGUAGE OF THE HISSITH, SSS |
| `KRITH` | KNOWN LANGUAGE OF COMMUNICATING WITH DRAGON CANDIDATES |
| `LUNAA` | KNOWN LANGUAGE OF SPRITES |

# ANTIQUITY

Some languages originate only from **ANTIQUITY**, the time before the `5TH AGE`, and even before the `1ST AGE` *(known as __ARCHAEA__)*. These are not commonly able to be learned from books and study, and are considered 'lost'. But some still exist in the world that can understand them, and certain artifacts grant insight to their tongue.

| |  |
|:------|:-------:|
| `AQUIEL`   | ANCIENT LANGUAGE OF AESORIA |
| `DRYDIA` | ANCIENT LANGUAGE OF THE CHLORYLL |
| `VALQUI` | LEGENDARY OTHERWORDLY LANGUAGE SPOKEN BY UNKNOWN BEINGS, OFTEN CONSIDERED 'DEMONIC' |
| `GARGISH` | ANCIENT LANGUAGE OF THE ROCKBORNE |
| `TWILLI` | EXTREMELY ANCIENT, LEGENDARY LANGUAGE OF THE EOSH |
| `SKITTER` | OBTUSE ANCIENT LANGUAGE OF THE WIGGLY, NOT LOST, BUT VERY DIFFICULT TO LEARN |

# WRITTEN

Some languages only exist in written form. They are considered languages of **ANTIQUITY** as well.

| |  |
|:------|:-------:|
| `COLMAW` | LANGUAGE OF ARCTIC CREATURES |

# UNWRITTEN

Some languages have no known written counterpart, and only exist in words and songs. They are considered languages of **ANTIQUITY** as well.

| |  |
|:------|:-------:|
| `ANTICHAIC` | LOST UNDATED LANGUAGE SOMETIMES CALLED 'FORGOTTEN TONGUE' |

# CELESTIAL

Some languages are sealed so far beyond the realm of mortals that it is believed to be forbidden to learn them. That does not mean nothing exists to let them try, though ...these are not impossible to learn, but there are almost no records of them.

| |  |
|:------|:-------:|
| `COLMAW` | CELESTIAL LANGUAGE OF ARCTIC CREATURES |
| `ENOCHIAN` | CELESTIAL LANGUAGE OF THE GODS AND THEIR MESSENGERS |
| `VOROTH` | CELESTIAL LANGUAGE OF VOMORTHAS |
| `SOVAL` | CELESTIAL LANGUAGE OF SOVALAH |
| `KIAWASH` | CELESTIAL LANGUAGE OF FIRE |

# ORIGINATION

There are few languages said to exist only at the dawn of time, and can never be learned again. They must be bestowed upon someone by a being that already knows them.

| |  |
|:------|:-------:|
| `HASPIRIN`   | TIMELESS LANGUAGE OF HAU'SPIRA |
| `TWILISI` | TIMELESS LANGUAGE OF THE EOSH |
| `SOLMETE` | TIMELESS LANGUAGE OF SPRITES |

---
component: alert
color: red
text: |
    THIS LIST WILL BE UPDATED AS MORE LANGUAGES ARE REVEALED
---