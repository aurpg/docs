---
title: "COMMANDS"
url: "commands"
icon: "device_hub"
order: 003
---
# COMMANDS, ABILITIES, SPELLS

Every *ability*, *power*, *technique*, *attack*, *spell*, etc; is called a **command**. During play, players may use these commands to perform statistical actions that interact with various game mechanics, such as gathering information, attacking an opponent, casting a spell, or hiding from a pursuer.


>![i]
===
Commands can easily be thought of like spells or abilities in a video game. They are your 'buttons' to cause things to happen.
===

# PHASES

All commands are marked with one or more **phases**. This determines when it may be used.

## ACTION

On a character's turn, or when not in combat, `1` command marked with the **ACTION** phase may be used.

## RESPONSE

When a character is the **target** of an **ACTION** command, they may react with `1` command marked with the **RESPONSE** phase.

## TETHER

When another character uses a command, it is possible to use `1` command marked with the **TETHER** tag that is **also** marked with the corresponding phase.

---
component: alert
color: red
text: |
    If multiple parties attempt to perform a **TETHER** command to the same target, they must roll `3d6+HASTE` to determine which succeeds. Resources are not spent for the failed commands.
---

# LINKED

Additionally, a character may attach `1` command marked as ->LINKED<- that is **also** marked with the corresponding phases. 

---
component: alert
color: red
text: |
    Commands marked in this manner may not be used on their own; They may only accompany a command that does not have the ->LINKED<- phase.
---
---
component: alert
color: red
text: |
    Attaching a **LINKED** command to a **TETHER** command requires it to have the **TETHER** phase as well.
---
component: command
example: true
name: NAME
nature: NATURE
aspects:
- ASPECT
- ASPECT
- ASPECT
paths:
-   PATH
-   PATH
costs:
- COST
- COST
requirements:
- REQUIREMENT
- REQUIREMENT
chances:
- "[1, 2, #]"
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- TAG
- TAG
- TAG
description: |
    The command's description is typically just flavor text, but it will sometimes offer more insight to the intended purpose. The description is usually more of the 'role-play' aspect of a command.
effect: |
    The command's effects are listed here, and if you see numbers appear in square brackets (**`[a, b, c, d]`**), it means that the command may be **flexed** to increase its effect.
---

# NATURE
The **nature** of the command's effect or behavior. This is the prominent property in deciding what kind of properties it can have. A command may only have one nature. 

---
+![SHOW EXAMPLE]

This command has its **NATURE** listed as ->MYSTIC<-. This implies that it is magical or spell based - it would use ->SPELLFORCE<- to increase its damage, for example, and may be mitigated by ->PROTECTION<-.

<++>
component: command
example: true
name: EXAMPLE FIREBALL
nature: MYSTIC
phases:
- ACTION
aspects:
- FIRE
<++>

>![i]
===
Please see the **MECHANICS** listing on [**NATURE**](http://docs.au-rpg.com/#/rules/nature) for further details.
===
---

>![i]
===
A command's nature will **always** be `KINETIC`, `MYSTIC`, or `ASTRAL`.
===
    
# ASPECTS
The **aspects** of the command's effect or behavior. This is typically auxiliary properties such as an element type, or the type of force it uses. A command may have multiple aspects. The aspects **do not change the nature**.

---
+![SHOW EXAMPLE]

This is a command that has **FIRE** aspect. Anything that states it interacts with something bearing the **FIRE** aspect will interact with this command.

<++>
component: command
example: true
name: EXAMPLE FIREBALL
nature: MYSTIC
phases:
- ACTION
aspects:
- FIRE
<++>

>![i]
===
Please see the **MECHANICS** listing on [**ASPECTS**](http://docs.au-rpg.com/#/rules/aspect) for further details.
===
---
    
# COSTS
The possible resources that may be spent to activate the command. It may be activated with __any__ one of the listed costs.
    
---
+![SHOW EXAMPLE]

This is a command that can be used by spending `20` ->MANA<- __**or**__ `2` ->ENERGY<-.

<++>
component: command
example: true
name: EXAMPLE FIREBALL
nature: MYSTIC
phases:
- ACTION
costs:
- 20 MANA
- 2 ENERGY
<++>
---
 
# FLEX
If a command has effects in square brackets (**`[a, b, c, d]`**) then you may spend `1x`, `2x`, `3x`, or `4x` of the corresponding cost to achieve the matching result.

---
+![SHOW EXAMPLE]

This is a command that costs `20` ->MANA<-, but shows `4` different **ranks** in its effect. So for `20` ->MANA<-, it would be `1d4`, for `40` ->MANA<-, it would be `1d6`, and so on. 

<++>
component: command
example: true
name: EXAMPLE FIREBALL
nature: MYSTIC
aspects:
- FIRE
costs:
- 20 MANA
phases:
- ACTION
- LINKED
description: |
    *Hurl a fireball at a target.*
effect: |
    Assail a target for `[1d4, 1d6, 1d8, 1d10]` **mystic fire** damage.
<++>
---

# MATCH
Some commands only occur when the `CHANCE` roll matches something specific; That is listed here. Matches are specific to dice order, and a `#` means any number is acceptable.

>![i]
===
For example, a `MATCH` of `[1, 2+, #]` would only work if the first die is **exactly** `1`, and the __second__ die is `2` or higher, and the third die can be anything.
===

---
+![SHOW EXAMPLE]

In this example, if your ->CHANCE<- roll's `1st` die is **exactly** `1`, the `2nd` die is **exactly** `2`, __or higher__, and the `3rd` die is anything, the command succeeds.

<++>
component: command
example: true
name: EXAMPLE FIREBALL
nature: MYSTIC
aspects:
- FIRE
chances:
- "[1, 2, #]"
phases:
- ACTION
- LINKED
description: |
    *Hurl a fireball at a target.*
effect: |
    Assail a target for `[1d4]` **mystic fire** damage.
<++>
---

# RAPID
There are *some* commands that may display a **golden bar** below their phases that marks them as ->RAPID<-. This is an advanced concept that indicates the command __does not consume the slot for the phase__. 
    
---
+![SHOW EXAMPLE]

In this example, this command may be cast without using up your phases ->LINKED<- slot during an ->ACTION<- phase.

<++>
component: command
example: true
name: EXAMPLE FIRE BONUS
nature: MYSTIC
aspects:
- FIRE
costs:
- 20 MANA
phases:
- ACTION
- LINKED
rapid: true
description: |
    *Increase the power of your spell with fire.*
effect: |
    Add an additional `+[1, 2, 3, 4]` ->SPELLFORCE<- to a ->MYSTIC<- attack.
<++>
---

>![i]
===
Rapid commands may not be stacked. You are limited to one at a time in a phase.
===

# TAGS
The tags that describe additional properties about the command, which further clarify questions about its use, application, ruling, and mechanics.

|||
|---:|:---|
| `ONCE`   | EFFECT ONLY OCCURS DURING THE TURN USED |
| `HOSTILE`   | EFFECT MAY BE USED ON HOSTILE TARGETS |
| `FRIEND`   | EFFECT MAY BE USED ON FRIENDLY TARGETS |
| `SELF` | EFFECT MAY BE USED ON ONESELF |
| `DODGE` | EFFECT MAY BE DODGED |
| `DEFEND` | EFFECT MAY BE MITIGATED BY DEFENSE |
| `PROTECT` | EFFECT MAY BE MITIGATED BY PROTECTION |
| `RESIST` | EFFECT MAY BE RESISTED |
| `LETHAL` | EFFECT MAY RESULT IN ARREST |
| `UNSTABLE` | EFFECT IS UNSTABLE |
| `PIERCE` | EFFECT IGNORES DEFENSE |
| `PENETRATE` | EFFECT IGNORES PROTECTION |
| `ORDERS` | EFFECT ISSUES COMMANDS TO COMPANIONS |
| `ATTACK` | EFFECT ALLOWS FOR ROLLING **ATTACK** |
| `SHIELD` | EFFECT REQUIRES A SHIELD |
| `RANGED` | EFFECT IS RANGED |
| `MELEE` | EFFECT IS MELEE |
| `SOUND` | EFFECT MUST BE HEARD |
| `MUSIC` | EFFECT IS MUSICAL |
| `DANCE` | EFFECT IS DANCING |
| `DRAW` | EFFECT ENACTS CARD DRAW |
| `RAGE` | EFFECT PHASE CANNOT **CRITICAL** |
| `NAKED` | EFFECT REQUIRES EMPTY BODY SLOT |
| `BAREFOOT` | EFFECT REQUIRES EMPTY FEET SLOT |
| `ARMED` | EQUIPMENT USES `1+` **WIELD** |
| `UNARMED` | EQUIPMENT USES `0` **WIELD** |
| `MOUNT` | EFFECT REQUIRES A TRANSPORT |
| `AREA` | EFFECT AFFECTS AN AREA |
| `THROW` | EFFECT TRIGGERS **THROW** |
| `CATCH` | EFFECT TRIGGERS **CATCH** |
| `LIGHT` | EFFECT MAY NOT WORK WITH `DARK` |
| `DARK` | EFFECT MAY NOT WORK WITH `LIGHT` |
| `OVERCOME` | EFFECT BYPASSES ->STRUGGLE<- |
| `REPEAT` | EFFECT MAY BE PERFORMED MULTIPLE TIMES FOR ITS COST |
| `FATIGUE`| EFFECT LEAVES USER WITH FATIGUE STATUS |
| `DIMINISH` | EFFECT COST INDEFINITELY TRIPLES EACH TIME AGAINST THE SAME TARGET |

---
component: alert
color: red
class: notice-lg
text: |
    You cannot roll **ATTACK** unless you use a command that has that tag.
---

---
+![ADVANCED]

# ADVANCED
Some of the data on commands is not as common, or is only relevant to selecting them for your character.

## PATHS
The **paths** that the command belongs to, if any.

## REQUIREMENTS
The possible requirements that may be met to use the command. It may be used if the character meets any of the listed requirements.
---

# REPLACEMENTS

There are times when the purpose a command serves is to replace the use of an existing command with better behavior. In this situation, it is redundant to declare the existing command's text in full again - and will be simply indicated with a bar to demonstrate this.

---
component: command
example: true
name: NAME
nature: NATURE
aspects:
- ASPECT
- ASPECT
- ASPECT
phases:
- RESPONSE
- LINKED
replace:
    name: COMMAND BEING REPLACED
    paths:
        - COMMAND SOURCE
---