---
title: "SLOTS"
url: "equipment"
group: "equipment"
order: 3002
---
# CHARACTER EQUIPMENT

A role-playing game is not complete without all the pomp and circumstance of equipment. True to form, items boost a character's abilities whether it is sensible or not; They come in hundreds of shapes, sizes, and descriptions.

Most characters start with the same amount of equipment slots, but the race you pick may have an impact on it.

### TYPICAL EQUIPMENT

- `x1` ->BODY<-
- `x1` ->FEET<-
- `x1` ->HEAD<-
- `x1` ->HANDS<-
- `x3` ->JEWELRY<-
- `x1` ->ACCESSORY<-
- `x2` ->GLYPH<-

### OTHER EQUIPMENT

- `x1` ->HOUSING<-
- `x1` ->FORM<-
- `x1` ->FORM+<-
- `x1` ->AURA<-
- `x1` ->STATUS<-
- `x1` ->CURSE<-
- `x2` ->COMPANION<-

# WEAPONS

Characters may hold up to `2` weapons as long as their ->WIELD<- meets or exceeds the total ->MASS<- present on both.

- You do not have to use all of your ->WIELD<-
- You may only equip `1` weapon if it is `RANGED`, even if you have the ->WIELD<- for more.

# RESTRICTED

Items that are marked with the label `RESTRICTED` mean that a player cannot equip anything else in that slot, even if they have extra slots or room. 

>![w]
===
All `RANGED` weapons are `RESTRICTED` by default.
===

# COMPANIONS

Ambient critters, caterpillars, and other pets are scattered everywhere; Unwitting victims to the hospitality of adventurers, each character may attain a massive assortment of available pets.

- `x2` ->COMPANION<- slots.
- Companions are considered equipment
- Companions may not be killed; Inversely, they are not valid actors in game play.
    - Just ...don't think about it very hard.
- These are non-combative companions

# AURA

The most rare slot to fill up is the ->AURA<- slot; This special equipment slot is reserved for items of notable esteem or reward, such as from contests, prizes, and heroic accomplishments.

Auras have no statistics, but often have wonderful effects. They are extremely rare.

# HOUSING

Characters will find they often wish to begin investing in real-estate within the game over time. Housing offers small passive bonuses to a character.

- Only `1` housing unit may be 'equipped' at a time.

>![w]
===
The housing system is under redevelopment and will become even more robust in the near future.
===

# AESTHETICS

Aesthetics are special items that typically have no statistics or effects, but are purely there for player enjoyment and flavor. They are frequently superfluous in nature - with grandiose glamor and showiness, long winded description and verbose detail.

They make items prettier.

>![w]
===
At this moment, the website is unable to display which aesthetics you have equipped.
===

# FORMS

Forms may be thought of as extended or supplemental **races**. They are, at times, special races that re-use the template for an existing race, or an advanced version of an existing race that endows it with special properties. Forms may be equipped on the equipment page, but are not typically treated like 'equipment'. They are still regarded, in most context, as races themselves.

Forms inherit the general feel, theme, and overall style of the race they are placed on. For example, a Form that is placed on top of an **ESPER** race would exhibit golem-like properties of a construct, and one placed over an **UNDINE** race would be aquatic in nature.

---
component: alert
color: red
text: |
    Beneath the <span class='au-term'>FORM</span> slot is an auxiliary slot called <span class='au-term'>FORM+</span> This is an equipment slot for the sole purpose of augmenting Forms and is extremely rare. It usually will only work with specific races or forms.
---

# STATUS
    
The ->STATUS<- is an equipment slot for **temporary** bonuses that a character possesses; These are often added to your sheet with an expiration date, or a limited number of uses. They are fairly rare, and are most often gained from social events and situations that leave a character in better health.

# CURSE

The ->CURSE<- slot exists only for cases when an on-going story event leaves a character with a condition that needs to be reflected on the character sheet. 

>![e]
===
You will be informed by the staff if this slot is ever necessary.
===
