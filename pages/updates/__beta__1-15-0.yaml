---
title: "v1.15 PREVIEW"
url  : "1-15-preview"
order: -2
---
# v1.15.0 PREVIEW

This is a minor update to showcase some cosmetic changes and preview the next build of the game, coming in update `1.15`.

# ACCESSIBILITY

The documentation has received a slight touch-up. The visuals are being tempered for more friendly use, accessibility, and convenience. Additionally, new features are being introduced.

### MENU

The documentation menu has received a tab that helps users know and access the sliding menu feature.

### ALERTS

>![i]
===
Alerts can draw attention to important things quickly.
===

>![s]
===
Alerts can draw attention to important things quickly.
===

>![e]
===
Alerts can draw attention to important things quickly.
===

>![w]
===
Alerts can draw attention to important things quickly.
===

### REGIONS

Click the area below to unfold a collapsed region.

---
<-- REGIONS -->

### COLLAPSIBLE
Regions of documentation can now be made to collapse, allowing for the viewer to bypass redundant or lengthy information that may not be what they are interested in; Such as verbose stories on items, and lengthy details on mechanics beyond the core rules.

>![s]
===
These collapsing areas can have all the normal features, too!
===
<-- END -->
---

## TAGS

A new feature that is sneaking into the documents, but isn't completely ready yet, is **tag cross-referencing**. When complete, you will see things like **TAGS**, **MECHANICS**, **ASPECTS**, **NATURES**, and **STATISTICS** appear with a symbol aside them on commands, items, etc.


When you touch this icon, a summary of it will appear; Making it easy to start wrangling information you may have forgotten without opening more pages. An example is seen below.

|||
|---:|:---|
| ->(ONCE)   | EFFECT ONLY OCCURS DURING THE TURN USED |
| ->(HOSTILE)   | EFFECT MAY BE USED ON HOSTILE TARGETS |
| ->(FRIEND)   | EFFECT MAY BE USED ON FRIENDLY TARGETS |

# UPCOMING

The next update coming soon will focus on the following;

- Minor item corrections
- Further refinement and cleanup to mechanics
- Further lore cleanup.
- Refinement to attribute selection from levels
- More typography polish.
- More cleanup to command cards
- **ACTUAL FRONT PAGE FOR THE DOCS**

---
<-- DISCIPLINE -->

## DISCIPLINE

The next update will introduce the category of mechanics known as **DISCIPLINES** to organize some of the nebulous elemental and behavioral functions.

- Scaling concerns and complexity of **AFFINITY**, **PROFICIENCY**, and **SAGE**
    - All collapsed into **DISCIPLINE**
    - Better progression introduced
    - Clearer mechanics
    - Slight normalization
    - New ranking structure
        - AFFINITY and PROFICIENCY
            - Will change to be clearer use
            - Slightly less damage mitigation, higher consistency, lower risk
            - Will reduce their cost on item budgets
            - Earthbound and Shift will get retouched a bit
            - Rockborne will probably get more broken, somehow.
        - ASPIRANT, ADEPT, MASTER, SAGE, GUARDIAN
            - Ranks will utilize affinity and proficiency within a structure.
                - The powers can still be obtained outside this structure
            - **MASTER** is Sage, but without global recognition in-character.
            - **SAGE** is considered a title, not really an evolution or power-up.
            - **GUARDIAN** is a player appointed power that cannot be given or put on items.
            - Details to come in the future.

<-- END -->
---

# FURTHER

The following are goals in the pipeline, but no given release date.

- [] Update main site to use command cards
- [] New command listing interface
- [] Update main site with new attribute selector
- [] Update main site with new inventory view
- [] Update main site with new equipment manager
- [] Update documentation with maps
- [] Update cross-references across both main site and documentation
- [] Provide "EDIT" button for players to submit edits to documentation for review
- [] Potentially preview the new crafting system, once it is certain the goals are within reach
- [] New **"ACCORDS"** and **"SELECTS"** system.
- [] Potential merger of **ATTENDANCE** and **PRESENCE**
- [] Potential preview of new **COMMAND** selection design.
- [] Remove system complexity at known choke-points
- [] More details to bug-bounty
- [] Potential preview of mount, pet, and beast redesign. 
- [] **PATH OF CHARISMA**
- [] **PATH OF INSPIRATION**
- [] **PATH OF LEADERSHIP**
- [] **PATH OF ARMAMENTS**