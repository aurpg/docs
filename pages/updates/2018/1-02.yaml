---
title: "v1.02 (2/21/2018)"
url: "1-02"
---
# v1.02
The following is the changelog for **AURAVERSE v1.02** on `2/21/2018`.

# DOCUMENTATION
The game's new documentation engine is finished and complete! These pages will be filling up quickly over the next few days, it's just a long task to transcribe information and polish it up.

For now, you can see the most improved documentation of all; The rebuilt commands data.
## [COMMANDS](http://docs.au-rpg.com/#/rules/commands)
---
component: alert
color: red
class: notice-xl
text: |
    To facilitate the dramatic changes, **all** path commands, racial commands, mastery commands, and talent commands will be slightly retouched, pruned, and clarified.
---
component: alert
color: blue
class: notice-xl
text: |
    Seriously they'll be fine. Don't panic.
---
component: alert
color: red
text: |    
    All players will receive a complete **RESEARCH** refund for their commands, as well as a refund of their talents and masteries, as the various parts are complete.
---
component: alert
color: red
text: |    
    This process will not take long, but there are only so many hours in the day.
---
# LORE
To further differentiate the *Auraverse* property, we have decided to rename the last two holdouts from previous game play.

- **JOURNEY** has been renamed to **HORIZON**
- **CROMWELL** has been renamed to **STONEPEAK**


# STATISTICS
- **VICIOUS STRIKE** has been removed from the game.
---
component: alert
color: green
class: lg
text: |
    This means that there is no more difficulty check to add **FORCE** or **SPELLFORCE** to a command. See the **MECHANICS** section on **NATURE** for further information.
---
---
component: alert
color: blue
text: |
    Vicious Strike will be taken off items and allocated to a new statistic, but not at this time. More details will follow as we get closer to launching the new statistic called **POTENTIAL**
---
- **MAGIC DEFENSE** has been renamed to **PROTECTION**

- **UNSTABLE MANA** has been renamed to **ETHERSURGE**
- **ETHERSURGE** now stacks up to `4`
- **MOMENTUM** now stacks up to `4`

## ETHERSURGE
Characters now generate `2` **ETHERSURGE** whenever they use **MANA** on their turn. This sets your **MOMENTUM** to `0`.

## MOMENTUM
Characters now generate `2` **MOMENTUM** whenever they use **ENERGY** on their turn. This sets your **ETHERSURGE** to `0`.

## UNSTABLE COMMANDS
Unstable commands now cost `4` **ETHERSURGE** or `4` **MOMENTUM** respectively.

## ENERGY
Energy now starts at `3` for all characters, and increases by `3` every turn up to `6`.
- Level 1 Melee/Physical Masteries increase this limit to `9`
- Level 2 Melee/Physical Masteries increase this limit to `12`
- Energy no longer resets on critical failure. More details on this will follow soon.
---
component: alert
color: red
text: |
    Energy will be streamlined in the next few days to also be simpler, and both **MOMENTUM** and **ETHERSURGE** will accrue more quickly.
---
# FEATURES
## [FATE](http://docs.au-rpg.com/#/rules/statistics/saves/fate)
At last, Fate makes its debut today! It has been added to the game and is now supported by the website.

# MECHANICS
- **FATE** has been added to the game.
- **LINKED** commands have been restricted to `1` for each phase.
- **TETHER** commands have been restricted to `1` for each target.
- **SPELLS** no longer require a `10` to hit their target.
---
component: alert
color: red
text: |
    All **TETHER** commands are being repurposed to use **MOMENTUM** or **ETHERSURGE**. Their effects will be retooled to compensate for this.
---
---
component: alert
color: red
text: |
    If multiple parties attempt to perform a **TETHER** command to the same target, they must roll `3d6+HASTE` to determine which succeeds. Resources are not spent for the failed commands.
---
- **LIMITED** command activation has been removed from the game entirely.
Skills, Attributes, and Saves listed on commands will be converted into **requirements**. Your character sheet must meet **one** of the listed requirements to use the command.
---
component: alert
color: green
class: notice-lg
text: |
    This behavior is currently **disabled** until the path commands have been rebuilt in the coming few days. We will try to update the site as quickly as we can to make this clear.
---
component: alert
color: red
text: |
    Effects and abilities that reduced a character's **LIMITER** will instead reduce the requirements for using a command. Further details to come shortly.
---
# TASKLIST
The following tasks are being pushed in the coming days, and will be checked off this list as they are complete.

- [x] **Fate**
- [x] Remove **Vicious Strike**
- [x] Commands documentation
- [x] Linked commands restructuring
- [x] Tether commands restructuring
- [x] Streamline **Energy**
- [x] Nature
- [x] Aspect
- [X] Retouch **miscasting**
- [X] Further documentation
- [X] Retouch **path** commands
- [X] Retouch **racial** commands
- [X] Retouch **mastery** commands
- [X] Retouch **talent** system
- [X] Kill **Kraig**
- [ ] Deal with zombie Kraig

