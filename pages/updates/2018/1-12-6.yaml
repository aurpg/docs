---
title: "v1.12.6 (8/8/2018)"
url: "1-12-6"
---
# v1.12.6

The following is the changelog for **AURAVERSE v1.12.6** on `8/8/2018`. This is a minor update with trivial fixes and slight additions to mechanics.

# STAFF

We are happy to announce that **AMY** is back on the staff, after having time to sort things out and clear some obstacles.

# WEBSITE

The following changes are happening to the website
- Bug that prevented Humans from getting the right amount of `WIELD` has been fixed.
- Bug that prevents updated commands from changing on sheets that have already picked them is being fixed, it's a challenging bug though.
- Bug that causes items to randomly disappear from Dani, and only Dani's sheet, has been found and hit repeatedly. We'll see if it cooperates.
- Bug that makes everything hate Sadie and Shol'ashy's sheet is still elusive; Hunting hard for it.

# CARDS

The first card was released recently; The next card will be the **EOSH**, coming with `PUBLIC MARKET UPDATE WAVE 2 (AUGUST 2018)` in the next few days.

- Please test the cards system if you can, feedback is needed
- Simulations are good, but only if the data is agnostic of intent.
- Simulation room will be provided in Discord shortly.
- More cards will appear more rapidly as we get more comfortable at creating them.
- Special thanks to `YURI` and `SHOL'ASHYS` and the entire staff for helping design the new Auraverse Zodiac!
    - The whole Zodiac will be revealed at the **STARLIGHT FESTIVAL** in August 2018.
- `STARLIGHT FESTIVAL` is winning the polls for the holiday. Don't like it? Change my mind
    - [CELEBRATION NAME](http://links.au-rpg.com/polls/celebration)
    - Special thanks to `DUSK` for collating the proposed names

# AESTHETICS

You can still submit your own custom aesthetics to appear at the upcoming holiday celebration! [AESTHETIC DESIGN](http://links.au-rpg.com/polls/aesthetics/2018)

# DOCUMENTATION

The documentation has received more polish and cleaning, as well as a slightly redesigned menu structure for accessibility. More polish to come soon.

# COMMANDS

A new field for `CHANCE` has been added to commands. At present it does not appear on any existing commands.

- This is considered a cost
- This field indicates that an ability occurs when the `CHANCE` roll matches something specific, for example, *`The first die must be a 5+`*
- Will probably not be an overly common thing
- Will be eased into slowly

# MECHANICS

The following mechanics have changed;

- `CATCH` has been added to mechanics and tags.
- `THROW` has been added to mechanics and tags.

.. What the hell is this?

- `BOND` has been 'added' to mechanics and tags, but is under construction.
    - An eerie new stat called `BOND` is also starting to appear on character sheets! What could that mean?

---
component: alert
color: red
text: |
    We're not telling.
---

# QUALITY OF LIFE

Players joining the game will now receive a default item called a `RACE AESTHETIC VOUCHER`

- May be redeemed for any one of the public market race aesthetics
- This allows us to introduce variant races more easily without penalizing players
- System may expand pending reception and effectiveness
- It will take some time for us to build aesthetic race-names into the site, but it is coming
- Will apply to the Riyuil themed transformation for Shift.
- Next race to get an aesthetic touch up is "Frost Giant"
    - `SKR'YRL` is the new "Frost Giant" name
    - Not married to the name, if you don't like it, pitch ideas.

---
component: divider
---

# NEXT

The following changes are slated to happen **very** soon.

Essentially, the combined `HEALTH` and `MANA` aspects of Spellforge and Lifedrinker are drastically more overhead than they give benefit for. They are **constantly** causing us to need to reword mechanics, clarify things to excrutiating detail, and provide awkward ways around cheating.

While it's been a fun idea, it's just not proving useful. We can get the same effect in more interesting ways.

- `SPELLFORGED AUTOMATON` is going to be renamed to simply `SPELLFORGE`
    - Tired of typing the whole thing out
    - Spellforges are getting `HEALTH` and `MANA` back, `POWER` is going away completely.
    - Will be healable with magic again
    - Still can't eat edibles
    - Racials will be redesigned
    - Will be able to use `HEALTH` or `MANA` for commands, effectively pseudo-joining the two.
    - Will probably lose the `50%` cost reduction and get something more interesting.
    - Won't fall `KO` after hitting `0` of either `HEALTH` or `MANA`
- `LIFEDRINKER` is getting retooled
    - Losing the `BLOOD` mechanic and returning them to `HEALTH` and `MANA`.
    - Will be healable with magic again
    - Still can't eat edibles
    - Racials will be redesigned
    - Will be able to use `HEALTH` or `MANA` for commands, effectively pseudo-joining the two.
    - Will probably lose the `50%` cost reduction and get something more interesting.
    - Won't fall `KO` after hitting `0` of either `HEALTH` or `MANA`
    - Extra damage when leaning, perhaps?
- `EOSH` will be next player race released
    - Race is done, just needs testing
    - Will tie in to story events

## MECHANICS

The following mechanics are getting worked on soon.

- `DROWN` is staying
- `DRENCH` is being added, will not suck like `DROWN`
- `DROWN` will be fixed to not suck.
- `STONE` **may** be added. Petrification-type effect
- Mechanics documentation will get more organized by type, and easier to navigate.
- Clarifications on things like `ZONE` effects will be written up.

# UPCOMING

The following are happening very soon, but may not make the next minor update specifically.

## ENERGY

Energy is being looked at for efficiency; Entertaining starting at `4` and increasing by `+4` each turn, instead of `3/3`. This would have people reaching maximum much faster.

## INTERESTS

Interests are still being retouched, sorry for the delay.

- Why does this take so long?
    - That's a secret

## HOUSING

Housing zones are finished, the only delay is finishing polish on the map and some forward planning. It's coming extremely soon.

- No plans to dramatically 'nerf' existing housing, though 'active' bonuses are dumb as hell because literally they can almost never be used, so we'll be culling that out and cleaning up some wording.
- The Library will still be rage-inducing broken.
- Housing will have its prices drastically cut
- `PRESENCE` will be used as a way to further cut housing costs, more details to follow.

## PATHS

The following is the plan for paths going forward.

- Most commands with a `CHANCE` mechanic are getting a *minimum* success rate.
    - Some are getting a minimum in **addition** to anything that triggers
    - e.g `LUMINAIRE`

### PATH OF STORMS

Path of Storms is slated for the following updates

- `TORANDO` and `BLIZZARD` will be differentiated more
- `LUMINARE` will be upgraded to `3d` over `2d`.
- Super happy with `COMET` redesign. Feedback requested.

### PATH OF DISTRACTION

... ~~is being dropped!~~ Is being merged with plans from `PATH OF DECEPTION` and some other secret ideas. It's now in the works again. It will now be called ...**PATH OF CONTROL**

- Will see the return of **Riposte** and **Dueling**
- Will have sneakery
- Will have a little flirting
- Will have a little face shredding
- It's going to be great. Bring a cheese grater.

### PATH OF CHARISMA

A new path is on the docket, but a release date is not available.

- Will be social and diplomacy based
- Will utilize extremely different ideas than other paths
- Will not have traditional unstables, but something else
- Will not be all based on Persuade, Charm, and Spirit like people are dreading.
- Will probably take a few revisions to get right.
- Is surprisingly far along in development, but hard to type out. Base ideas need a lot of testing to ensure they are streamlined properly.
- Is probably going to be after `PATH OF INSPIRATION`.
- May come before `PATH OF INSPIRATION` if answers suddenly hit us.
- Will actually have some combat use, amazingly.
    - I'm as surprised as the rest of you.
- Will not rely on `HEALTH` and `MANA` for many things
- Will not soley rely on `CHANCE`, it will have on-demand function.
    - Will use a bit of `CHANCE` stuff.

### PATH OF INSPIRATION

`PATH OF INSPIRATION` is still in the works, but is very hard to nail down due to its abstract nature. No ETA at this time, but actively working on it.

- Would like to get more cards out for `PATH OF STARS` first.
- Was heavily delayed due to depression and stress.

### PATH OF GRAVES

I am remiss to announce that `PATH OF GRAVES` is going to be retired in a future update, but its useful functionality will be moved around and rolled into other things; As well as some inclusion in `PATH OF CONTROL`.

Please let us know soon what abilities from Path of Graves you feel are important.

- Undead pets are not going away
- Yes, clearly there is a bigger plan at work
- Don't panic, it's good stuff. Graves is a barrier blocking better things
- New Necromancy abilities will surface

### PATH OF VIGILANCE

Path of Vigilance is getting some retouching

- `ARRAY` and `MIRRORFORCE` will be bumped to `[40%, 80%]`
- Possible removal of bloated `5 MOMENTUM` cost on Unstables.
    - This isn't working as planned.

### PATH OF MINDFORCE

Path of Mindforce is going to get some examination

- `PSYCHOADRENALINE` sucks