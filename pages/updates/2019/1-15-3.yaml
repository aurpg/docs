---
title: "1.15.3 - 12/10/2018"
url  : "1-15-3"
---
# v1.15.3

The following is the changelog for **AURAVERSE v1.15.3** on `12/10/2018`. This is a critical update to fix broken features.

> We deeply appreciate the feedback from players who want to improve the quality of the game. Please continue to help us refine the gameplay with your testing and ideas! The following changes are due to planned feature updates, and feedback to update `1.15.2`. Because this update replaces `1.15.2`, we have moved all of the relevant text from it into this document.

# FOREWORD

It's been an interesting `2018` and things have changed a lot! We've done well to have so few changes after each path was launched, and have steadily introduced new features to the game without the need to greatly diminish the over `260+` items released between `MARCH` and `DECEMBER` of this year!

We will be making small changes in preparation for upcoming content, and the `2019` calendar year - as well as paving the way for more streamlined game play and simpler mechanics.

Players have often expressed unrest about the rate of changes when we began this venture in early March. We hope to ease a few concerns with this outline below.

# ADJUSTMENTS

The following changes are effective immediately.

---
+![PATH OF IMPULSE]
## PATH OF IMPULSE

In the previous update, we reduced several of the tools that were considered essential for generating `TEMPO`. Due to player feedback, we evaluated this and determined that there were not enough options available. A few changes have been made to help remedy this.

The **PATH OF IMPULSE** command **`NO, RIGHT!`** has been removed. In its place is **`FLOWING`**; 

- Helps produce more `TEMPO`
- Lacks the `DANCE` tag so it can be specific.
- The previous command felt too same-same with others.
    - This will make `BUTTERFLY` more attractive, too.
- Wanted generating `TEMPO` to be easy.
    - Utilized the `RAPID` status to help achieve this.
    - Players should have a lot of versatility in their build with this.

<++>
component: command
name: FLOWING
nature: KINETIC
aspects:
-
paths:
- PATH OF IMPULSE
costs:
- 3 ENERGY
- 30 MANA
requirements:
- 3 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
rapid: true
description: |
    While in motion, you are not only more difficult to hit, but when you obtain a certain rhythm, your sequential movements each begin to feed into one another. This flowing movement is the sign of greater power, and amplifies many traits both offensively and defensively.
effect: |
    - Generate `1 TEMPO`.
<++>

The command **`SOLICIT`** has been simplified with the introduction of **rapid** commands. It can now be simpler to read.

- Please note, you cannot stack **rapid** commands.

<++>
component: command
name: SOLICIT
nature: KINETIC
aspects:
paths:
- PATH OF IMPULSE
costs:
- 300 PRESENCE
requirements:
- 4 INSTINCT
phases:
- ACTION
- RESPONSE
- TETHER
tags:
- ONCE
- SELF
- PRESENCE
rapid: true
description: |
    The merchant before you has closed his shop. He is not interested in your money, time, or even the quest you are on to save your long-lost sister from the dark clutches of a chicken cabal seeking to sacrifice her like rye. He eyes you with suspicion and tears swell.

    *"Do you know **who** I am?"* You implore, but it falls on deaf ears.

    And then like a glorious gleaming golden ray of indelible sunshine, the heavens glow as your clothe parts ways; A veritible dead-see of fabric to *very* firmly assert the query once more. *"DO YOU KNOW **WHO** I AM!?"*
effect: |
    - You are now considered `NAKED`, `BAREFOOT`, and `UNARMED` - despite not being any of those, and gain `+1` `ETHERSURGE`, `MOMENTUM`, `TEMPO`, or `DRAW`. Why you need to be naked to draw cards is a question we don't have time for.

    - Event hosts may respond to this in any number of ways.

    - Nipple
<++>

The command `TURBOSENSE` is fantastic, but it has a few key flaws.

- It self-feeds itself infinite `TEMPO`.
- The rate of failure is virtually `0` due to the length of encounters.
- We feel we went a bit too far by giving so many things the ability to negate damage to `0` easily.
- The damage mitigation was changed to only `10%` in order to avoid trivializing content.
    - Brings it in line with `BUTTERFLY` for alternative cost
    - The `PERSIST` tag adds extra value to `TURBOSENSE`
    - The rate of failure is unchanged, so it is very efficient
    - Players begin with `2 TEMPO`, so it is still quickly available.
- To make it synergize with other game effects better, the special effect was coupled to `MOMENTUM` instead, so that players may continue their normal command rotations after activating it.
    - `ENERGY` added as a *(costly)* alternative so that players are not left completely without options in niche situations.
- Rather than hammer it into the ground with higher cost, we decided to make it reward strategy more effectively.
- The penalty to `TEMPO` was removed.
- It no longer refills `TEMPO` to full each phase. This was egregiously broken.
- The `TEMPO` cost was kept unchanged in order to better reward players who trigger it with `6 TEMPO`.

<++>
component: command
name: TURBOSENSE
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF IMPULSE
costs:
- 4 TEMPO
- 2 FATE
- 4 MOMENTUM
- 4 ETHERSURGE
requirements:
- 5 INSTINCT
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- PERSIST
- SELF
- UNSTABLE
description: |
    You frequently leap to react without thinking, but if that skill could be honed - even summoned upon in a moment of need, it would be remarkably powerful...
effect: |
    - Until you have rolled `3` `1`'s on `CHANCE` cumulatively, ~~DODGE reduces damage to `0` , and your `TEMPO` fills to maximum every phase. It becomes `0` when the effect ends~~. `DODGE` reduces damage to `10%` at the cost of `1` `MOMENTUM` or `12 ENERGY`.
<++>

---
+![PATH OF THE WILD]

## PATH OF THE WILD

Often, it is hard to build out both ranged and melee abilities at the same time. We have examined a few under performing commands in **PATH OF THE WILD** and replaced them with refreshed candidates.

The **PATH OF WILD** command **`COMFORT!`** has been removed. In its place is **`LAUNCH`**.

- No one ever used `COMFORT`.
- No one even understood it.
- Most animals try to kill us.
- Trying to soothe animals has always failed.
    - We have better ideas for that in the future.
    - For years, we've tried to tackle the problem from a traditional standpoint.
        - We will take a very different angle, players will enjoy it.
- There are so few commands that grant `EFFORT`.
- We wanted it to be simple, low-level, and attractive.

<++>
component: command
name: LAUNCH
nature: KINETIC
aspects:
-
paths:
- PATH OF THE WILD
costs:
- 1 ENERGY
- 10 MANA
requirements:
- 1 EMPATHY
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
- RANGED
description: |
    Velocity aids your abilities, as range has the advantage of compounding force.
effect: |
    - `+[1] EFFORT` when using `RANGED` commands.
<++>

The **PATH OF WILD** command **`PRECISE`** has been removed. In its place is **`ACCLIMATE`**.

- The command `PRECISE` was just a worse version of `SNIPE`.
- Need more reason for `BAREFOOT` and `NAKED` to matter.
- The `ENERGY` cost is kept moderate to avoid abuse, but still allow flexing.
    - We didn't want people pumping in `2+ EFFORT` in the first phase without consumables.

<++>
component: command
name: ACCLIMATE
nature: KINETIC
paths:
- PATH OF THE WILD
costs:
- 6 ENERGY
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
- RANGED
- BAREFOOT
- NAKED
rapid: true
description: |
    Your adjustment to nature and the various environs has granted you exceptional comfort in most surroundings.
effect: |
    - `+[1, 2] EFFORT` to `RANGED` commands.
<++>

The **PATH OF WILD** command **`AURAMENTAL`** has been removed. In its place is **`THREADING`**.

- No one really understood `AURAMENTAL` well.
- It felt like a bad version of `HEDGEHOG`.
    - It was obtuse to use as well.
    - It was also hard to say.
- There isn't enough to spend `ETHERSURGE` and `MOMENTUM` on for ranged.
- Wanted to make it different in some way.
    - Utilizing the new **REPLACES** data on commands.
    - This makes such commands very unique.

<++>
component: command
name: THREADING
nature: KINETIC
paths:
- PATH OF THE WILD
costs:
- 2 MOMENTUM
- 2 ETHERSURGE
requirements:
- "3 KNOWLEDGE: NATURE"
phases:
- RESPONSE
- LINKED
tags:
- ONCE
- RANGED
- HOSTILE
- DEFEND
replace:
    name: DODGE
    paths:
        - CORE COMMAND
description: |
    You attempt to fire back at oncoming assaults, threading your shot through the eye of a storm of chaos like a master hunter.
effect: |
    - Upon a successful **DODGE** roll **RANGED ATTACK** to retaliate with a **kinetic ranged attack**, the target does not get a response
<++>

The **PATH OF THE WILD** command **`RECOIL`** has been redesigned to utilize the new **rapid** command system.

- Wanted a more powerful version of **`THREADING`**.
- Wanted it to be harder for a target to survive.
    - This one also adds `FORCE`.
    - Will continue to observe and evaluate.
    
<++>
component: command
name: RECOIL
nature: KINETIC
paths:
- PATH OF THE WILD
costs:
- 4 MOMENTUM
- 4 ETHERSURGE
requirements:
- "4 KNOWLEDGE: NATURE"
phases:
- RESPONSE
tags:
- ONCE
- UNSTABLE
- RANGED
- HOSTILE
- DEFEND
replace:
    name: DODGE
    paths:
        - CORE COMMAND
description: |
    You're extremely efficient at moving rapidly and reciprocating assaults with shots to the face.
effect: |
    - Upon a successful **DODGE** roll **RANGED ATTACK+FORCE** to retaliate with a **kinetic ranged attack**, the target does not get a response.
<++>

---
+![PATH OF MINDFORCE]

## PATH OF MINDFORCE

The command **PSYCHOADRENALINE** has never worked. It just needed to be rebuilt.

The **PATH OF MINDFORCE** command **`PSYCHOADRENALINE`** has been completely redesigned.
- Rebuilt to be a bit more modest
- Utilizing the `RAGE` tag to make it simpler.
- Utilizing `EFFORTS` to provide value.
- Utilizing the new `RAPID` command status to make it viable in more places.
- Removed multiplier that caused most of its problems.

<++>
component: command
name: PSYCHOADRENALINE
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF MINDFORCE
costs:
- 1 ETHERSURGE
- 1 MOMENTUM
requirements:
- 4 PERCEPTION
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- UNSTABLE
- RAGE
rapid: true
description: |
    Your entire body becomes consumed with power; Your sweat burns gold and blue energy seeps from every pore, the mindforce permeates your every motion.
effect: |
    - Add `+[1, 2, 3, 4] EFFORT` so long as you do not roll a `FAILURE`.
<++>
---
+![PATH OF GLADIATORS]

## PATH OF GLADIATORS

To help spread out some of the utility, and encourage some different builds of more interesting design, we elected to seed `TEMPO` into the kinetic paths. The `WARMACHINE` unstable has also been a subject of confusion for a while.

The **PATH OF GLADIATORS**, **PATH OF KINETICS**, and **PATH OF VIGILANCE** command **`TENSE`** has been renamed to **`BURST`**, and retooled.
- This command now has the `DANCE` tag.
- The theme of the command has been improved to encompass more archetypes.

<++>
component: command
name: BURST
nature: KINETIC
paths:
- PATH OF GLADIATORS
- PATH OF KINETICS
- PATH OF VIGILANCE
costs:
- 1 ENERGY
requirements:
- 1 ACROBATICS
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- SELF
- DANCE
description: |
    Your muscles tense, and power surrounds you. A surge of energy bursts through your blows. You move with your own energy to execute perfect, timed, and focused strikes with rhythm and clarity.
effect: |
    - Damage targets normally immune to **kinetic** nature attacks.
<++>

The **PATH OF GLADIATORS** command **`WARMACHINE`** has been redesigned to require less math and scale more correctly. It also has the `RAGE` tag.
- The command should be extremely powerful
- The power should carry a heavy cost
- The command should not require as much math
- The `FATIGUE` status wraps up better behavior to interact with.
    - For example, now something that cures `FATIGUE` is more valuable to someone with this ability.
- The command does not require as long, drawn out encounters now.
- We wanted `WARMACHINE` to remain a `RESPONSE` command.

<++>
component: command
name: WARMACHINE
nature: KINETIC
paths:
- PATH OF GLADIATORS
costs:
- 4 MOMENTUM
requirements:
- 4 ACROBATICS
phases:
- RESPONSE
tags:
- ONCE
- HOSTILE
- UNSTABLE
- MELEE
- RAGE
description: |
    Gather all of the power at your disposal, unleashing it at the cost of your own life.
effect: |
    - Roll **ATTACK** for each `2+` you roll on `CHANCE`. You are afflicted with **FATIGUE**.
<++>
---
+![PATH OF SAGES]

## PATH OF SAGES
The **PATH OF SAGES** is one of the paths that feels the most deprecated as new game elements have been introduced. Due to its hybrid nature, it has been one of the fastest to lose some of its early scaling benefit. We are making a few adjustments to it today.

The **PATH OF SAGES** and **PATH OF KINETICS** command **`BLINDING`** has been retooled.

- This should scale better, and be viable for builds early in the game.
- It should utilize the `CHANCE` mechanic to give more varied value.
- Should be attractive for hybrids, low-level characters, and heavy mage builds.

<++>
component: command
name: BLINDING
nature: KINETIC
aspects:
- LIGHTNING
paths:
- PATH OF SAGES
- PATH OF KINETICS
costs:
- 20 MANA
- 2 ENERGY
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- RESPONSE
- LINKED
tags:
- ONCE
- HOSTILE
description: |
    Your kinetic attacks are accompanied by bright flashes of lightning, making it difficult for an opponent to see.
effect: |
    - Reduce incoming attack's `HASTE` by `-[1, -1, -2, -2]` for each `[6, 5+, 4+, 3+]` on `CHANCE`. ^^(minimum 1)^^
<++>

The **PATH OF SAGES** command **`STONESTRIKE`** has been slightly retooled.
- This should be an attractive command for hybrids of all type.
- It should scale well for frequent spellcasters that gear for `SPELLSTRIKE` and `SPELLFURY`.
- It should scale **incredibly** well for `SPELLFURY`.
- Because of its low cost, increased flexes do not improve the rate.

<++>
component: command
name: STONESTRIKE
nature: KINETIC
aspects:
- EARTH
paths:
- PATH OF SAGES
costs:
- 20 MANA
- 2 ENERGY
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
- LINKED
tags:
- ONCE
- HOSTILE
description: |
    Your kinetic attacks become surrounded by sharp shards of rock, impaling an opponent for extra damage.
effect: |
    - `+[3, 5, 7, 9] FORCE` and `SPELLFORCE` for each `3+` on `CHANCE`. ^^(minimum 1)^^
<++>

The **PATH OF SAGES** command **`SEETHE`** has been retooled to include new game features introduced since it was created.

- First foray into letting players utilize the `SINGE` mechanic.
- Kept tame to avoid abuse.
- We want it to be very hard to pull off such a behavior.
- Other status afflictions may follow suit, depending on how this works out.

<++>
component: command
name: SEETHE
nature: KINETIC
aspects:
- FIRE
paths:
- PATH OF SAGES
costs:
- 2 ENERGY
- 20 MANA
chances:
- "[6, 4+, 4+]"
requirements:
- "2 KNOWLEDGE: NATURE"
phases:
- ACTION
- LINKED
tags:
- ONCE
- HOSTILE
- RESIST
description: |
    Fire seethes around your body and consumes your strike viciously.
effect: |
    - Inflict `SINGE` on a target if `CHANCE` match succeeds.
<++>
---
+![ITEMS]
## BRIKLOK

The item **`BRIKLOK`** has received a new command for use with its unlocked form.

- This command is identical to its existing command, merely setup for **RANGED**.
    - Because commands are not factored into the sheet, some that give **`ATTACK`** fall short for ranged.
    - To help with this, we merely factored in the mastery ahead of time.
        - It may be possible to use without the mastery, but your other ranged things will suck, so we're not too worried.

<++>
component: command
name: MUDLAUNCHER
nature: KINETIC
aspects:
- EARTH
costs:
- 2 ENERGY
requirements:
- BRIKLOK Ω
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- HOSTILE
- RANGED
description: |
    This peculiar weapon will infuse every blast with powerful energies from the planet when it is fully unlocked.
effect: |
    - `+[12, 20, 32, 48, 64, 84]` ATTACK
<++>

## GLORYWOVEN SPINETUNIC

This item has had its command **AESOFORGED SECRET STRENGTH** modified, to coincide with changes to **COMET** and similar effects.

<++>
component: command
name: AESOFORGED SECRET STRENGTH
nature: KINETIC
aspects:
- QYRAL MIRRAL
costs:
- 8 ENERGY
- 1 MOMENTUM
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- HOSTILE
- MELEE
- ATTACK
- RAGE
description: |
    These robes are decidedly cloth-like, but it appears there may be a secret to them. As if beckoning power from a dark place, the HISSINEW scales clicking and clinking together - locking into place to create something ghastly and almost ...abominable. As if your body is wrapped in a suit of Aesoforged parts for a moment, you become nigh superhuman.
    
    Made for a mage; Strong enough for a murderer. There is an evil echo in your voice when this power ignites.
effect: |
    - Roll **ATTACK+FORCE** as normal. Additionally, multiply **FORCE** by `[2x, 3x, 4x, 4x]` for each `[5+, 5+, 4+, 3+]` on your **CHANCE** roll. ^^(minimum 2x if no rolls trigger this effect)^^
<++>
---
+![PATH OF STORMS]
## PATH OF STORMS

- [x]  `COMET` and abilities like it scale a bit out of control rapidly due to the convenient availability of `MANA` and `ENERGY`. This is a common problem introduced whenever abilities have a multiplier effect.
    - In order to try and curb some of the excessive stacking, they have been adjusted.
    - We wanted to eliminate some of these abilities from coupling so easily.
    - Mana is plentiful and Energy is promised, making them poor throttles for abilities of this nature.
    - They were slightly over-budgeted, and valued closer to unstable commands - so they should cost appropriately.
    - On `COMET` costing more
        - `MANA` is not entirely finite, and is easily replenished.
        - `ENERGY` is exactly finite, and is metered
        - `MANA` can appear on equipment
        - `AESOFORGED SECRET STRENGTH` requires a specific item to use, and it consumes multiple equipment slots
        - `COMET` is a natural command.
        - It is theoretically possible to use `COMET` for healing as well as damage.

<++>
component: command
name: COMET
nature: MYSTIC
aspects:
- ICE
paths:
- PATH OF STORMS
costs:
- 120 MANA
- 1 ETHERSURGE
requirements:
- "3 KNOWLEDGE: NATURE"
phases:
- ACTION
- LINKED
tags:
- ONCE
- HOSTILE
- RAGE
description: |
    Beckon an enormous chunk of ice upon a target.
effect: |
    - Multiply **SPELLFORCE** by `[3x, 4x, 5x, 5x]` for each `[5+, 5+, 4+, 3+]` on your **CHANCE** roll. ^^*(minimum `3x` if no rolls trigger this effect)*^^
<++>
---

# STATE OF THE GAME

As we prepare for our final few events of the year, I've taken time to reflect on `YEAR 1` of the Auraverse since it's more 'official' launch. This is the overarching state of the game, as the Game Master sees it, at this time.

- New looting system is working **very well**
    - Better than we could have hoped for.
- Story and lore seem to be marginally well received
    - I believe we can do more with local lore, such as the cities
    - I feel information is still obtuse to obtain
    - Item lore needs to get onto the documentation better.
- The public market is `100%` a success in every conceivable way.
- The changes to limit `LINKED` slots and removed the deprecated `LIMITED` behavior have proven good.
- Getting rid of `EDGES` was one of the smartest moves we ever made.
- Getting rid of old items has been so healthy for world building and item design.
- Players still seem to struggle with some of the tags.
    - Plans in the works to improve this.
    - The documentation needs examples of things better.
- The **CHANCE** mechanic has proven exemplary.
    - Solved many of the problems it set out to address
- Numbers are high, but consistent.
    - Players are doing impressive things, but they are gated behind items and behaviors accessible to everyone.
    - The presence of `FATE` has greatly put an end to some of the problem areas.
    - `TEMPO` and `DRAW` are working very well.
        - Some tweaks need to happen, but very few.
    - Damage **to** players is still a problem area.
        - Too much `SPEED` on items. We were too liberal.
        - Too many `0% DAMAGE` commands. Oops!
- New paths are coming in `2019`
    - The **PATH OF LEADERSHIP** and **PATH OF RUIN** are highest on the docket.
        - Path of Ruin is taking a long time because of the same problems with **PATH OF ARMAMENTS**
        - The **PATH OF CHARISMA** may be rolled into Leadership for efficiency. It worked well with Impulse.
- Community morale has improved.
    - Though we purged a lot, it was needed for healing.
    - No one seems broken up about it.
- Website still in the works.
    - *"soon"*
- Recruiting is about to begin!
- Was very happy with **CRYSTALLING** advent.
    - Maybe a bit too heavy handed on how easy the last phase was.
    - Dropped the ball on **SOLFEL**
        - Was just super exhausted. Needed the break.
        - Next Solfel will be better.
        - Plans for pie will happen.
    - The next advent, **WINTERLUDE** will be **awesome**.
        - We have a social coordinations staff member now!
        - She has great plans.
    - The advent after Winterlude has not been named.
        - It has been named but Ciel is unhappy with her names, so she's calling it unnamed.
    - The **VALENTINE'S DAY** advent is slated to be called `LIFEBOUND`
        - Will change it if that is determined to be stupid.
- There are over `20` different major story arcs in the pipeline.
    - Content is running according to plan.
    - Players have diverged `6` times from the original path set out.
        - This is not a bad thing.
    - Players have been surprisingly less bloodthirsty than expected.

### WHAT TO EXPECT

- Minor tweaks to a few gameplay fundamentals
    - We are particularly looking at `AFFINITY` and `SAGE` for retooling.
    - We are still evaluating `FATE`, and are very happy with it.
        - Perhaps the ability to so effortlessly gain `4 MOMENTUM` or `4 ETHERSURGE` is too readily accessible. It hasn't been decided.
        - Currently happy with the rate of `FATE` replenishment.
    - We will be making tweaks to a few commands.
        - Mostly commands targeting `DODGE` or those with multipliers.
        - No plans to heavily nerf overall gameplay.
    - Minor tweaks to items as needed. They are performing as expected for the most part.
        - Very few are broken. Less than `1%`.
    - We think the way players select commands, and how many they can select, can be improved.
        - The current process is a bit linear.
        - Plans to improve the options.
        - This will be a net gain to players, not a nerf.
        - This is a long term project, and will not likely be seen very soon.
            - Slated for late `2019`.