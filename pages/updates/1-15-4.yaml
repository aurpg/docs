---
title: "1.15.4 - 1/3/2019"
url  : "1-15-4"
group: "latest"
---
# v1.15.4

The following is the changelog for **AURAVERSE v1.15.4** on `1/3/2019`. This is a critical update to fix broken features.

> We deeply appreciate the feedback from players who want to improve the quality of the game. Please continue to help us refine the gameplay with your testing and ideas!

## DOCUMENTATION

- The instructions for commands have been clarified to remark that ->LINKED<- commands may not be substituted for primary ones.
- The instructions for commands now clarify ->RAPID<- command usage.
- Further documentation improvements
    - Still more in the works

# EQUIPMENT

Players will notice new equipment slots present on their sheets.

- ->FORM<- and ->FORM+<- are related to the new racial skins.
- ->STATUS<- is a new slot being introduced that will contain interesting, temporary changes to a character.

The new ->STATUS<- equipment slot will be used for special conditions that are temporary, such as having a limited use - or expire and vanish from the character sheet after a certain period of time. You will first see these introduced with **WINTERLUDE** and **NEWSONG**.

# DEATH

The mechanics for ->DEATH<- have been retooled slightly. Every attempt is being made to keep this simple, but death has become nearly impossible due to the number of options available to players. Even simply knocking characters unconscious is implausible due to the overwhelming availability of defensive abilities.

While it is good for players to have options, when it is not possible to construct challenges for them, it becomes time to examine the core of the risk mechanics and start tightening reigns a little bit.

For that reason, a few changes are being made; As well as two new ones added - **STRUGGLE** and **AID**.

---
+![WHY IS DEATH CHANGING?]

We have performed a critical analysis of the state of death in the game, and this is our findings.

- 1. The mechanics for ->INERT<- are too open-ended right now.
    - An ->INERT<- character is unconscious, and as such, should not be able to do things a conscious character can.
    - An ->INERT<- character should represent someone who is struggling for consciousnesses. The term **inert** was chosen for brevity, instead of **unconscious**, so it should reflect that.
    - We did not want ->INERT<- to be *exclusively* tied to death, because sometimes we just want to incapacitate a character.
        - It will be better to just create a new status affliction for this purpose. There is too much complication in trying to keep ->INERT<- and death coupled so much.
        - Therefore, the term ->INERT<- will be simple, and we will create the term ->STRUGGLE<- to encapsulate death mechanics.

- 2. It should be harder to heal characters that fall in battle.
    - It is literally so trivial to heal people right now, there is no actual consequence for hitting `0` ->HEALTH<- or ->MANA<-.
    - People hit `0` ->HEALTH<- and then someone shoves entire steak dinners down their throat with **tethers** and **potions**. 
    - Healers are almost pointless, consumables were made too easy to abuse.

- 3. The purpose of requiring ->BODY<- to be depleted is to prevent one-hit kills.
    - Sometimes, content is over-tuned, or players try to grief others.
    - New players can be easily discouraged if there is no chance to survive.
    - The current behavior is too generous though; Equipment has now become properly normalized.
    - ->HEALTH<- is reaching normalized levels. It is okay to begin increasing certain challenges ^^(carefully)^^

- 4. We believe ->FATE<- is the best saving throw against death, since it is an equalizer.
    - Racial behaviors will become more valuable with our new approach.
    - It will be easier to die, but still very hard to die in a single hit.
    - It will not be possible to imbibe potions after being knocked out.
    - It will be challenging to heal unconscious characters, making it more rewarding when it succeeds.
        - Because of the new approach, healing can become more dynamic and the paths for it more interesting
        - They feel bland right now, as they were very much a struggle to fill out the requisite number of commands without being too powerful.
        - Abilities to go around the concept of *struggling to survive* will encourage better healing builds.
---

---
+![WHAT DOES THIS MEAN FOR ME?]

This has meaning for every type of player.

- 1. Enemies may be tuned better without needing to account for gross overages of consumables.
    - Enemy damage can be better controlled
    - ->HEALTH<- will be valued less heavily on items, making it more common.
    - ->MANA<- will start receiving a better budget on items.
- 2. It is still hard for a player to be one-shot
    - The chances of passing a `DC15` against a `3d6+6` are not trivial.
- 3. More interesting healing interactions and racials.
    - The healing paths, and the racials can be retooled slightly.
    - This will make healers relevant again.
    - The various paths will be looked over for good candidates to bypass ->STRUGGLE<-.
- 4. Defending-type characters will be higher valued.
    - Because it is harder to heal up from being knocked out, preventing damage weighs more.
    - ->PROTECTION<- will soon apply to things that target ->MANA<-
    - ->DEFENSE<- will be budgeted better on items as well.
- 5. Bloated saving throws will not trivialize content.
    - Players have a more tempered playing field for survival.
- 6. We can allow ->FATE<- to recover in new ways.
    - With better uses for it out, it does not just equate to damage.
---

---
+![STRUGGLE]

## STRUGGLE

When a character's ->HEALTH<- or ->MANA<- is reduced to `0`,  it **stays at `0`** and they fall [->INERT<-](/#/rules/inert).
    
At that time, and then in place of their own ->ACTION<- phases, they begin to ->STRUGGLE<- to survive. To pass a ->STRUGGLE<-, roll `3d6+FATE` against a difficulty of `15`.

- Failing a ->STRUGGLE<- results in [->DEATH<-](/#/rules/death)-.
- Passing a ->STRUGGLE<- results in your status remaining unchanged.
- A ->CRITICAL<- on a ->STRUGGLE<- returns you to `1` ->HEALTH<- or ->MANA<- ^^(whichever was depleted)^^.

<++>
component: alert
color: blue
text: |
    Characters must ->STRUGGLE<- in place of their own ->ACTION<- phases, or whenever damage would hit them.
<++>
<++>
component: alert
color    : red
text     : |
    Only commands that declare they bypass ->STRUGGLE<- may recover ->HEALTH<- or ->MANA<- in this condition. This is done with the tag ->OVERCOME<-.
<++>
---
---
+![AID]
## AID

When a character is receiving aide, either physically or magically, they are considered ->AID<-ED and their challenge against [->STRUGGLE<-](/#/rules/struggle) is reduced.

By default, each time [->AID<-](/#/rules/aid) is applied to a target, the challenge is reduced by `1`. This may stack.

<++>
component: alert
color: blue
text: |
    Some races will get a different bonus from being affected by ->AID<-
<++>
---

* * *
# FATE
    
The behaviors regarding ->FATE<- have been modified to prevent abuse, add new options, and slightly improve game performance.
 
| |  |
|---:|:---|
| `1 FATE`   | PASS A ->STRUGGLE<- |
| `1 FATE`   | EXECUTE AN ADDITIONAL `LINKED` COMMAND |
| `1 FATE`   | RESPOND TO A `CRITICAL` |
| ~~`1 FATE`~~   | ~~SUBSTITUTE UNLIMITED MANA FOR `1` PHASE~~ |
| ~~`1 FATE`~~   | ~~SUBSTITUTE UNLIMITED ENERGY FOR `1` PHASE~~ |
| `1 FATE`   | SUBSTITUTE `1` `MOMENTUM` FOR `6` ->ENERGY<- |
| `1 FATE`   | SUBSTITUTE `4` `DRAW` FOR `9` ->ENERGY<- |
| `1 FATE`   | SUBSTITUTE `4` `TEMPO` FOR `9` ->ENERGY<- |
| `1 FATE`   | SUBSTITUTE `1` `ETHERSURGE` FOR `60` ->MANA<- |
| `1 FATE`   | SUBSTITUTE `4` `DRAW` FOR `60` ->MANA<- |
| `1 FATE`   | SUBSTITUTE `4` `TEMPO` FOR `60` ->MANA<- |
| `2 FATE`   | SUBSTITUTE UNLIMITED MANA FOR `1` PHASE |
| `2 FATE`   | SUBSTITUTE UNLIMITED ENERGY FOR `1` PHASE |
| ~~`2 FATE`~~   | ~~SUBSTITUTE `4` `ETHERSURGE`~~ |
| ~~`2 FATE`~~   | ~~SUBSTITUTE `4` `MOMENTUM`~~ |

---
+![WHY IS FATE CHANGING]

We have noticed that the nature of how easy it is to substitute ->FATE<- for ->ETHERSURGE<- and ->MOMENTUM<- is egregiously game breaking. To that end, we are throttling that behavior just a bit, and adding some new options to make it more rewarding for different archetypes.

The biggest offender to breaking ->FATE<- is the ability to ramp up to unlimited ->ETHERSURGE<- or ->MOMENTUM<- at any moment. We want to introduce ways to do this, but believe the full-throttle method available with such a low-cost behavior is too convenient.

These costs will be examined as the game continues on.
---

* * *

# DRAW

To match the behavior of ->TEMPO<-, accumulating ->DRAW<- is now mutually exclusive with other resources. You cannot accumulate other resources such as ->ETHERSURGE<- or ->MOMENTUM<- when generating it.

# TAGS

The following tags, and therefore accompanying behaviors, have been added to the game and may be seen on commands.

| |  |
|---:|:---|
| `OVERCOME` | EFFECT BYPASSES ->STRUGGLE<- |
| `REPEAT` | EFFECT MAY BE PERFORMED MULTIPLE TIMES FOR ITS COST |
| `FATIGUE`| EFFECT LEAVES USER WITH FATIGUE STATUS |
| `DIMINISH` | EFFECT COST INDEFINITELY TRIPLES EACH TIME AGAINST THE SAME TARGET |

These are further explained in detail below, for interested parties.
---
+![TAGS]

## OVERCOME

This tag may indicate a command that bypasses the mechanics of [STRUGGLE](/#/rules/struggle).
    
## REPEAT

This tag indicates a player may use a command multiple times, all in the same phase, without consuming extra slots; However they must pay its full cost ^^(per rank used)^^ for each time repeated. 
- This is intended to appear on commands that involve a heavy gamble.
- The resource is spent regardless of the outcome.
- The resource may never be substituted with ->FATE<-.

## FATIGUE

This tag indicates a player is left with the [FATIGUE](/#/rules/status/fatigue) upon resolution.
- This occurs regardless of the outcome.

<++>
component: alert
color: red
text: |
    This tag will intends to simplify adding a penalty to some high-yield abilities in a fashion that reduces verbosity.
<++>

## DIMINISH

This tag indicates that using the command on the same target more than once warrants its cost increasing by `3x` **consecutively**.
- This stacks without end, each time.

<++>
component: alert
color: red
text: |
    This is intended to be put on commands that need to have **diminishing returns** so a player may not over-use the ability due to the objective nature of role-play. For example, an ability that could be used on a stationary target is not meant to be attempted until successful.
<++>
---

# COMMANDS

In order to account for the dramatic changes to death, and fix underlying game balance concerns, many commands have been adjusted.

---
+![COMMANDS]

The following commands are being adjusted.

## SAGES, STORMS, ELEMENTS, SACRIFICE

It has been determined that `PATH OF SAGES`, `PATH OF STORMS`, `PATH OF SACRIFICE`, and `PATH OF PRIME ELEMENTS` are in need of heavy review.

- Changes will include more `LINKED` behavior
    - The command ->COMET<- is just poorly made.
    - Pretty much all of `PATH OF STORMS` is in bad shape.
    - Magic is in an odd place right now. It needs some work.
    - These were the first three paths created under the new system, and show the most fatigue.
    - Sacrifice feels dated and synergizes poorly with the changes to death and ->MANA<-.


## PATH OF DAWN

Various commands in the `PATH OF DAWN` have been given additional optional costs of `PRESENCE`, `SILVER`, or other reagents in order to signify things such as devotion to a religion, deity, or chosen path of life.

<++>
component: alert
color: green
text: |
    The costs attached take the current game economy into account, and will change as the game evolves. They also take into consideration the high volume of cost-reduction behaviors available.
<++>

The command ->DAWNBRINGER<- has been updated to reflect the addition of ->STRUGGLE<- and ->RAPID COMMANDS<-. It is now used to help healing behaviors bypass the ->STRUGGLE<- mechanic, and costs ->ETHERSURGE<- instead of ->MANA<-.

<++>
component: command
name: DAWNBRINGER
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
costs:
- 2 ETHERSURGE
- 6 DRAW
- 2,000 PRESENCE
- 6,000 SILVER
requirements:
- 2 EMPATHY
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
- FRIEND
- OVERCOME
rapid: true
description: |
    Empower your attacks with celestial fury!
effect: |
    - Add the ->OVERCOME<- tag to **mystic** effects that recover ->HEALTH<- or ->MANA<-.
<++>

The command ->STARFIELD<- has been slightly updated to clarify its usage scenarios. This ability has had its ->MANA<- cost removed and been given a **reagent** cost instead, signifying it as a ritual as intended. The cost is spent by the user of the ability, but may be cast on either end of the ritual ^^(the person teleporting, or the person someone is teleporting to)^^.

<++>
component: command
name: STARFIELD
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
- PATH OF STARS
costs:
- 12,000 SILVER
- 9,000 PRESENCE
- 4 CHIRALMOTE
- 4 HORRORTEAR
- 6 KULIMI MOTE
- 1 MISTEAR
requirements:
- "3 KNOWLEDGE: WORLD"
phases:
- RITUAL
tags:
- ONCE
- SELF
- FRIEND
description: |
    Pick a star, any star! You are taken into the void for a moment, and deposited back safely. Your body literally dematerializes for an instant and then restructures.
effect: |
    - Move to an ally's side that is `[1km, 10km, 100km, 1000km]` away __that performs a ritual to accept your arrival__.
    
    >![e]
    ===
    This ritual may be performed by either the person teleporting or the person being teleported to, and its cost is consumed by the user.
    ===
      
    >![e]
    ===
    This ritual may not be performed during combat.
    ===
<++>

The command ->DESPERATION<- has been updated to reflect the addition of the ->STRUGGLE<- mechanic by giving it the ->OVERCOME<- tag.
- This indicates a command bypasses ->STRUGGLE<-, and other mechanics that declare they are bypassed by ->OVERCOME<-.

<++>
component: command
name: DESPERATION
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
costs:
- 30 MANA
- 1,000 PRESENCE
- 3,000 SILVER
requirements:
- 3 EMPATHY
phases:
- ACTION
tags:
- ONCE
- OVERCOME
description: |
    An incredible surge of light scatters over an ally.
effect: |
    - Heal an ally for `3d12` and an additional `[2d12, 4d10, 6d8, 8d6]` for each `[6, 5+, 4+, 3+]` on **CHANCE**
<++>

The command ->CONVICTION<- may now also be used as an ->ACTION<- command to assist those who are undergoing a ->STRUGGLE<-. Its cost has been changed to ->ETHERSURGE<- and proportionate ->MANA<- for its effect.
- Note that the tag ->ONCE<- is now replaced with ->PERSIST<-, allowing this effect to remain for the duration of an encounter.
- This implies it may be used pre-emptively as a precaution.

<++>
component: command
name: CONVICTION
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
costs:
- 2 ETHERSURGE
- 200 MANA
- 2,000 PRESENCE
- 6,000 SILVER
requirements:
- 3 EMPATHY
phases:
- ACTION
- RESPONSE
tags:
- PERSIST
- SELF
- FRIEND
- OVERCOME
description: |
    Your resolve and blessings can stave off death. If the healer goes down ... well, let's hope it never comes to that.
effect: |
    - Bless yourself or a target with `[1x, 2x]` ->AID<- status.
<++>

The command ->REKINDLE<- has been updated to account for the new ->STRUGGLE<- mechanic.

<++>
component: command
name: REKINDLE
nature: MYSTIC
aspects:
- DAWN
paths:
- PATH OF DAWN
costs:
- 30 MANA
requirements:
- 3 EMPATHY
phases:
- ACTION
- LINKED
rapid: true
tags:
- ONCE
- FRIEND
description: |
    Bless a fallen comrade with starlight, in hopes that they will recover.
effect: |
    - Remove `[1, 2, 3, 4]` **affliction(s)** that cause the target's challenge against ->STRUGGLE<- to be increased.
<++>

The command ->RECOLLECTION<- has been updated to account for the new ->STRUGGLE<- mechanic.

<++>
component: command
name: RECOLLECTION
nature: ASTRAL
aspects:
- DAWN
paths:
- PATH OF DAWN
costs:
- 4 ETHERSURGE
requirements:
- 4 EMPATHY
phases:
- ACTION
tags:
- FRIEND
- UNSTABLE
- OVERCOME
rapid: true
description: |
    Call down the power of the heavens and the sun to bathe a target in cleansing light.
effect: |
    - Purify the mind of a target afflicted with ->MADNESS<- or ->STRUGGLE<-, giving them `[6x]` ->AID<- effect as well as removing any **VORAL**, **QYRAL**, **MIRRAL**, **SOVAL**, **AURAL**, or **ELIXIN** aspect status afflictions.
<++>

## PATH OF STARS

The command ->DESTINY<- is being updated to reflect the addition of ->STRUGGLE<- and ->AID<-.

<++>
component: command
name: DESTINY
nature: ASTRAL
aspects:
- SOVAL
paths:
- PATH OF STARS
costs:
- 6 DRAW
requirements:
- "4 CONSIDERATION"
phases:
- ACTION
- RESPONSE
- TETHER
tags:
- ONCE
- FRIEND
- UNSTABLE
- OVERCOME
description: |
    Believe hard enough, and you too can make miracles happen. Turn `6` of your cards face up, and read them to predict a target's future. Let's hope it's a good one.
effect: |
    - Grant an ally an extra `1d` to ->FATE<- or ->STRUGGLE<-
<++>

The command ->OBSERVER<- is being updated to reflect the addition of ->STRUGGLE<-.

<++>
component: command
name: OBSERVER
nature: ASTRAL
aspects:
- SOVAL
paths:
- PATH OF STARS
costs:
- 4 FATE
requirements:
- "4 KNOWLEDGE: MYSTIC"
phases:
- ACTION
tags:
- AREA
- FRIEND
- UNSTABLE
- OVERCOME
description: |
    Reach to the heavens, and grasp them. Take hold of the stars like baubles to be obtained and tear them straight from the skies to bring down a celestial observer.
effect: |
    - Summon forth the current **zodiac** or **warden** constellation and the current host will add and control an avatar of a celestial, starlit **NPC** ally to your party. The entity summoned will vary each time and persists until dawn.
    
    - This is an avatar of a constellation - not an actual deity or celestial entity. It can't answer your questions about the meaning of life or history.
    
    - All allies suffering a ->STRUGGLE<- are safely wrapped in a cocoon of stars with `6,200 HEALTH` that protects them from any further damage for `1 hour` or the end of the present encounter ^^(first occurrance)^^.
<++>

## PATH OF MIRACLES

The command ->PLACEBO<- has been adjusted for the addition of ->STRUGGLE<-. It now utilizes reagents instead of ->MANA<-.

<++>
component: command
name: PLACEBO
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF MIRACLES
costs:
- 2,000 SILVER
- 2,000 PRESENCE
- 1 CHIRALMOTE
- 1 HORRORTEAR
requirements:
- 2 BLUFF
phases:
- ACTION
tags:
- ONCE
- FRIEND
description: |
    Convince an ally that they are healed!
effect: |
    - Apply `[1, 2, 3]` ->AID<- to an ally and allow them to recover `[1d4, 1d6, 1d12]` ->HEALTH<- and ->MANA<- if they succeed the save.
<++>

The command ->DOUBLE TAKE<- has been adjusted for usefulness. It has been given the ->ACTION<- and ->LINKED<- status, and is now a `RAPID` command that is used to influence ->CHANCE<-.

<++>
component: command
name: DOUBLE TAKE
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF MIRACLES
costs:
- 1 ETHERSURGE
- 1,000 PRESENCE
- 3,000 SILVER
requirements:
- 2 BLUFF
phases:
- ACTION
- RESPONSE
- LINKED
tags:
- ONCE
rapid: true
description: |
    Your illusions cause a target to rethink the truth of what they just saw.
effect: |
    - `+[1, 2, 3]` up or down to any of the first `3` dice in your ->CHANCE<- roll.
      
    >![r]
    ===
    THIS CAN CAUSE EFFECTS TO TRIGGER, BUT CANNOT CAUSE A `CRITICAL`.
    ===
<++>

The command ->MIRROR<- has been adjusted for balance concerns. Its range and event utility has been improved, but its risk increased. It now costs reagents ^^(silver)^^ or ->PRESENCE<- to use, instead of ->MANA<-. Its duration is now indefinite.

<++>
component: command
name: MIRROR
paths:
- PATH OF MIRACLES
costs:
- 6,000 SILVER
- 6,000 PRESENCE
- 1 CHIRALMOTE
- 1 HORROTEAR
requirements:
- 3 BLUFF
ritual: true
tags:
- RITUAL
description: |
    Create a complex doppleganger that looks and acts just like you.
effect: |
    - This doppleganger may act independently up to `[60, 80, 120, 240]` meters from you. It can trigger traps, gather information, hear, see, and feel.
    - The doppleganger is dispelled if you afflicted with a change in ->STATUS<- or use another ->COMMAND<-.
    
    >![e]
    ===
    Damage done to this doppleganger applies to your `MANA` and immediately destroys it.
    ===
      
    Upon being dispelled or destroyed, you will instantly attain all the knowledge and experiences it had.
<++>

The command ->RELOCATE<- has been adjusted for logical concerns. It has had its radius reduced due to its cost, and may only apply to visible destinations.

<++>
component: command
name: RELOCATE
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF MIRACLES
costs:
- 12,000 PRESENCE
- 24,000 SILVER
requirements:
- 3 BLUFF
phases:
- ACTION
tags:
- ONCE
- AREA
- FRIEND
description: |
    Move willing allies a safe distance quickly.
effect: |
    - For some reason, you can teleport `[1, 2, 3, 4]` allies up to `[10, 20, 30, 40]` meters away __to a destination that you may see__.
    
    >![i]
    ===
    Allies that are undergoing a [STRUGGLE](/#/rules/struggle) are not moved unless an `OVERCOME` tag is somehow added.
    ===
<++>

The command ->DELUSIONAL<- is being updated to account for the addition of ->STRUGGLE<- and ->AID<-.

<++>
component: command
name: DELUSIONAL
nature: ASTRAL
aspects:
- MINDFORCE
paths:
- PATH OF MIRACLES
costs:
- 4 ETHERSURGE
- 8,000 PRESENCE
requirements:
- 4 BLUFF
phases:
- ACTION
tags:
- PERSIST
- OVERCOME
- FRIEND
- UNSTABLE
description: |
    Convince a dead ally that they're just whining and shouldn't complain so much.
effect: |
    - The target believes they have `+[3]` ->AID<- and recovers `[3d8]` ->HEALTH<- and ->MANA<- if they succeed the save.
<++>

## PATH OF STORMS

Comet has been corrected to multiply ->SPELLFORCE<- by `[2x, 3x, 4x, 4x]` for each `[5+, 5+, 4+, 3+]` on your ->CHANCE<- roll ^^(minimum. `2x` if no rolls trigger this effect).^^

<++>
component: command
name: COMET
nature: MYSTIC
aspects:
- ICE
paths:
- PATH OF STORMS
costs:
- 120 MANA
- 1 ETHERSURGE
requirements:
- "3 KNOWLEDGE: NATURE"
phases:
- ACTION
- LINKED
tags:
- ONCE
- HOSTILE
- RAGE
description: |
    Beckon an enormous chunk of ice upon a target.
effect: |
    - multiply ->SPELLFORCE<- by `[2x, 3x, 4x, 4x]` for each `[5+, 5+, 4+, 3+]` on your ->CHANCE<- roll ^^(minimum. `2x` if no rolls trigger this effect).^^
<++>

---

# RACES

Because of the changes to the game's death behaviors, a few races have received slight adjustments.

---
+![RACIALS]

The following racials will be changed shortly.

## GREENSKIN

The command ->HEAD ON<- has been redesigned.
<++>
component : command
name: HEAD ON
nature: KINETIC
passive: true
description: |
    Greenskin are segmented into various tribes across the world, and while outsiders often see them as a fairly savage, barbaric race, they have a strict code and high respect for power and strength. They refuse to be defeated unless they are facing their attacker.
effect: |
    - Greenskin have a reduced difficulty on ->STRUGGLE<- by `1` for each `12` **combined** ->STAMINA<- and ->STRENGTH<-
<++>

## LIFEDRINKER

The command ->LIFELESS<- has been redesigned.

<++>
component: command
name: LIFELESS
nature: KINETIC
passive: true
description: |
    Lifedrinkers rely greatly on their ability to draw in the life around them, they are not quite living, nor dead, and as such exist in an eerie state.
effect: |
    - ->IMMUNE<- to challenges involving ->BREATHING<-
    - ->IMMUNE<- to ->CONSUMABLES<- that are ->EDIBLE<-
    - Reduced difficulty on ->STRUGGLE<- by `1` for each `12` **combined** ->STAMINA<- and ->STRENGTH<-
<++>

## HUMAN

The command ->RESILIENT<- has been redesigned.

<++>
component: command
name: RESILIENT
nature: ASTRAL
passive: true
description: |
    Humans are frequently underestimated, and put down by the other races for their shorter life spans and often limited cultural acceptance.
effect: |
    - Reduced difficulty on ->STRUGGLE<- by `1` for each `12` **combined** ->SPIRIT<- and ->INSTINCT<-
<++>

---

# ITEMS

The following items have been tuned for balance purposes;

---
+![ITEMS]

## GLORYWOVEN SPINETUNIC

This item has had its command **AESOFORGED SECRET STRENGTH** modified to be given the ->LIGHT<- tag; Making it incompatible with commands featuring the ->DARK<- tag.

<++>
component: command
name: AESOFORGED SECRET STRENGTH
nature: KINETIC
aspects:
- MIRRAL
costs:
- 8 ENERGY
- 1 MOMENTUM
phases:
- ACTION
- RESPONSE
- LINKED
- TETHER
tags:
- ONCE
- HOSTILE
- MELEE
- ATTACK
- RAGE
- LIGHT
replace:
    name: ATTACK
    paths:
        - CORE COMMAND
description: |
    These robes are decidedly cloth-like, but it appears there may be a secret to them. As if beckoning power from a dark place, the HISSINEW scales clicking and clinking together - locking into place to create something ghastly and almost ...abominable. As if your body is wrapped in a suit of Aesoforged parts for a moment, you become nigh superhuman.
    
    Made for a mage; Strong enough for a murderer. There is an evil echo in your voice when this power ignites.
effect: |
    - Roll **ATTACK+FORCE** as normal. Additionally, multiply **FORCE** by `[2x, 3x, 4x, 4x]` for each `[5+, 5+, 4+, 3+]` on your **CHANCE** roll. ^^(minimum 2x if no rolls trigger this effect)^^
<++>

## GRANITE GREAVES

The command **NON-SENESCENT** on this item has been reduced to `4` ->HEALTH<- and `4` ->MANA<- recovered, down from `8`. Has had ->BOND<- removed from the effect, as it will no longer be relevant to the behavior of the item due to the way it has progressed in development.

<++>
component: command
name: NON-SENESCENT
nature: ASTRAL
aspects: MIRRAL
passive: true
tags:
- ONCE
- SELF
- LIGHT
description: |
    This item grants you power, so long as your determination does not fail.
effect: |
    - Recover `4` ->HEALTH<- and `4` ->MANA<- for each `4` ->SPIRIT<- you have whenever you spend, give, gain, or convert ->MOMENTUM<-, ->ETHERSURGE<-, ->TEMPO<- or ->DRAW<-.
<++>

## STARSEEKER GLYPH OF THE DRAMATIC WIGGLY

The command **HUMBLE DRAMATIC ATTENTION** has been reduced to recover only `3` ->HEALTH<- whenever commands are spent __during the bearer's phase__ *(not ->TETHER<- commands)*.

## STARCALLER GLYPH OF THE DRAMATIC WIGGLY

The command **DRAMATIC ATTENTION** has been reduced to recover only `4` ->HEALTH<- and `4` ->MANA<- whenever commands are spent __during the bearer's phase__ *(not ->TETHER<- commands)*.
---


* * *
# STATISTICS

Two new statistics have begun to appear in game; ->BOND<- and ->FEAR<-. What these do is so far not revealed. But we will provide the following clues;

### BOND AND FEAR

- The ->BOND<- statistic will relate to beastmastery and pets.
- The ->FEAR<- statistic will relate to necromancy.

Both of these behaviors are still in development, but will begin slowly creeping into game content over time.

These will function in an interesting way that will let us develop the two systems in new directions that were previously too complicated to delve into due to the nature of turn based combat and double-dipping with command architecture.

>![e]
===
NOTHING PRESENTLY USES EITHER OF THESE STATISTICS
===

* * *

# FUTURE

To touch on some common questions we get asked;

### WHAT HAPPENED TO PATH OF BEASTS AND GRAVES?
    
>![i]
===
We feel that pets needed to be thought up from the ground up again, and did not want to try and shoehorn them into a system that worked on deprecated ideas carried over from table-top behavior.

Looking back over the history of the game, the very idea of how pets, undead and normal, work, was fundamentally flawed from several key vectors.

The attempts to fix this with things like the *TOLERANCE* behavior showed certain relief, but failed to address the overarching problem.

For that reason, the entire idea was taken back to formula in early `2018` and has been steadily being rebuilt. It is nearing a point where seeds are ready to bloom.
===

### WILL MY PETS AND MOUNTS BE HONORED?

>![i]
===
Yes. The plan is still, as always, to converge the existing pets and mounts into a unified system **when it is ready**.
===

### WHAT IS TAKING SO LONG?

>![i]
===
The redesign of pets and mounts is a monumental feat from a technical perspective, and we do not want to go forward with it until we are `100% READY` to service the whole player base correctly.
===

### IS THIS WHY CRAFTING IS TAKING SO LONG TOO?

>![i]
===
Yes
===

### WHAT HAPPENED TO THE EOSH RACE?
    
>![i]
===
Players advanced through the story content at a pace that makes the Eosh an inopportune race to reveal as they had been presented. Decisions were made and key shifts to the naming convention of lore points, the Eosh will be called `EOSHIR` when they are released, and are still on the docket.

That said, we do not believe there is a very captive audience for the type of gameplay they offer - so they are slightly lesser priority than more immediate problems.
===

### ARE THE WIGGLY STILL ON THE DOCKET?

>![i]
===
Yes
===

### ARE THERE ANY OTHER RACES COMING?

>![i]
===
It is too early in the year to reveal that.
===

### WHY DO THINGS COST SO MUCH PRESENCE AND GOLD?

>![i]
===
The economy for both is inflated and we are working on the problem. We will begin to decrease costs now that we have succeeded in normalizing gains.
===

### WILL THE WEBSITE STOP DISPLAYING COMMANDS AND ITEMS SO TERRIBAD?

>![i]
===
We're working on that.
===
