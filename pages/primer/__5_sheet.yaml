---
title: "5. STATISTICS"
url: "sheet"
order: 50
group: "primer"
---

# CHARACTER SHEETS

The game's mechanics are played out using the data and numbers present on your `character sheet`, which may be accessed through your [DASHBOARD](http://au-rpg.com/Member).

To fully understand the game's rules and mechanics will take patience, however this is an extremely brief run-through of them.

These are terms you need to be familiar with intimately.

# NATURES

Natures specify what kind of effect something is.

- [NATURE](http://docs.au-rpg.com/#/rules/nature)
    - `KINETIC`
        - Physical
            - Melee, ranged, etc.
    - `MYSTIC`
        - Unnatural, magical, elemental.
            - Magic
            - Shamanism
            - Divinity
    - `ASTRAL`
        - Intangible or Ethereal; Spooky.
            - Psychic
            - Divination
            - Telekenesis
---
component: divider
---
# ACTIONS

These are terms you will see **very** frequently, and are at the heart of the entire game system. They are further detailed in the links provided by clicking on each.

- [COMMAND](http://docs.au-rpg.com/#/rules/commands)
    - An ability that may be used.
- [PHASE](http://docs.au-rpg.com/#/rules/commands)
    - A character's turn to make a choice, either as the actor or the responder.
- [CHANCE](http://docs.au-rpg.com/#/rules/chance)
    - A roll to determine success, failure, and other outcomes;
        - `3d6` if not stated
            - `3` `6`'s is a **critical**
            - `3` `1`'s is a **failure**
---
component: divider
---
# VITALITY

These are the key statistics that determine if a character is alive and well.

- `HEALTH` and `MANA`
    - Physical and mental acuity or status; If either hits `0` you are unconscious.
    - Unable to fall below `0`.
- `ENERGY`, `ETHERSURGE`, and `MOMENTUM`
    - Advanced topics; These are resources used for certain commands.
---
component: divider
---
# TRAITS

These are the key statistics that make up everyday use of game mechanics.

- `SPEED`
    - Rate of movement for challenges, such as `DODGE`
- `INITIATIVE`
    - Ability to cut in line and go first; Used to determine turn order.

---
component: divider
---

These are the key statistics related to *kinetic* effects and gameplay.

- KINETIC
    - `ATTACK`
        - Your kinetic `MELEE` offensive capability.
    - `RANGED`
        - Your kinetic `RANGED` offensive capability.
    - `WIELD`
        - Representation of your ability to hold weapons.
        - Weapons have a value called `MASS`.
            - `1 MASS` requires `1 WIELD`
        - You may only hold up to `2` weapons, no matter how much Wield.
        - You may only hold `1` weapon if it is `RANGED`.
    - `FORCE`
        - Your kinetic bonus damage.
    - `HASTE`
        - Your modifier to kinetic attacks against opponents trying to `DODGE` them.
    - `DEFENSE`
        - Reduces kinetic damage to you

---
component: divider
---

These are the key statistics related to *mystic* effects and gameplay.

- MYSTIC
    - `SPELLFORCE`
        - Your mystic bonus damage or healing.
    - `SPELLSTRIKE`
        - Your modifier to mystic attacks against opponents trying to `RESIST` or `DODGE` them.
    - `SPELLFURY`
        - Advanced topic
    - `PROTECTION`
        - Reduces mystic damage to you.
