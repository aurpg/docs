---
title: "4. PLACES"
url: "places"
order: 40
group: "primer"
---

# PLACES

The most daunting task with a world of this size is discerning where to begin; The volumes of lore, game rules, items, and input from other players can become overwhelming quite suddenly.

The following are some key pieces of information to help players rapidly acclimate to the game's environment and get started as quickly as possible.

- The game takes place on a continent named `ASTRIA`, within the realm `MIDGROUND`.

---
+![MAP]
    
The presently known continent of Astria is as presented here.

![responsive](https://res.cloudinary.com/aurpg/image/upload/v1549731266/maps/astria.jpg)

---

---
+![HISTORY]

The game has a vast dearth of lore to explore.

- History is divided into segments called `AGES`
- The game is currently in the `5th AGE`.
- Time before the `4th AGE` can be safely called `ANTIQUITY`.
---

---
+![CITIES]

Astria is populated by 5 major settlements, as well as the domain known as the `BLUE MESA`. It also serves as home to the stronghold of the `DARUKAI` civilizations, though relations with them are often strained.

## MIDPORT

The primary city of Astria; A rustic port town with wide harbors and respected trade. 

<++>
component: alert
color: green
text: |
    Midport is governed by a collective of wealthy founders known as `THE ENCLAVE` - a secretive society that may only be identified by those already within.
<++>

## FLAMESPIRE

A religious capitol that has become a mecca for scholars and scribes. Precious documents are frequently hand-copied here by commission from many nations.

<++>
component: alert
color: green
text: |
    Flamespire is under the council of a presiding legislature known as the `AURAVIZED`; A scholarly group of learned individuals who see to it that knowledge is both kept safe for the future, and made available freely.
<++>

## STONEPEAK

A mountain-trenched city rich in minerals and artisans. Stonepeak was once only a quarry, but it bloomed into a massive haven of mineral and prosperity.

<++>
component: alert
color: green
text: |
    Stonepeak is protected, as well as governed, by an elite legion known the `VANGUARD`.
<++>

## HORIZON

An adventurer's haven bordering the rim of the `THRESHOLD`; Horizon often sees heroes off on their journey, or greets them when they return in failure or fortune. 

<++>
component: alert
color: green
text: |
    Horizon is round-table controlled by seasoned adventurers who have been recognized as the `PARAGONS` the world over.
<++>

## STONESTHROW

A small, miniscule encampment north of Stonepeak - Stonesthrow began as a new settlement aimed to expand closer to `FROSTVIEW` and provide new opportunities for a fledgeling community.

<++>
component: alert
color: green
text: |
    Due to its small size, Stonesthrow is presently only governed by a small Sherif's department.
<++>

## BLUE MESA

The Blue Mesa is a paradise under water, home to the ->UNDINE<- and their kind. They are part of the ->STARLIGHT ACCORD<- with Astria's cities, and therefore strong allies.

<++>
component: alert
color: green
text: |
    The Blue Mesa is governed by the Undine Princess **Wyr'na**, who sends representatives as she sees fit.
<++>
---

---
+![FOES]

The game's primary antagonists are called the `FORGOTTEN`.

- The Forgotten are ink-like entities from a realm known as `VOMORTHAS`.
- The Forgotten are capable of retroactively removing things from having existed, or happened.

There are many other antagonists within the game. You may learn about them at your leisure in the more in-depth lore section of the website.
---

---
+![POINTS OF INTEREST]

There are several locations of note within the game.

## THRESHOLD

The Threshold is a swirling fog of miasma that covers a large area of Astria.

- Trying to leave the Threshold demands a payment of memories.
- Failure to appease the debt transforms entities into creatures called `HARVESTERS`.
- Flight, teleportation, and the like do not circumvent the Threshold.

## BRANCHLING FOREST

The Branchling Forest is a hostile forest in Astria.

- Attempting to enter presently redirects adventurers back out.
- There is no known record or stories about the area, suspiciously.
- Characters do not seem to recall that it exists.

## DARUKAI TERRITORY

The Darukai tribes populate a large section of Astria.

- Presently non-hostile, but not freely accessible.
- Trade with the Darukai is good, at present.
---
