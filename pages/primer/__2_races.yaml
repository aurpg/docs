---
title: "2. RACES"
url: "races"
order: 11
group: "primer"
---

# CHARACTER RACES

The races of the Auraverse are vast and rich - coming in many varieties and categories. You may select from any of them without restriction.

---
+![BLESSED]

# BLESSED

The primary races were the first to emerge **after** the creation of **AUDRASSIL** and the land of **CHIRALIS**; It is unclear who created them or how, but they are considered the most **BLESSED** of the races.

## HUMAN

Among the first races created after the origination of the universe and the establishment of the realm ->MIDGROUND<-. Humans are resilient and ambitious. They are considered blessed by the deities.

## GELVAN

Knife-eared and fair skinned, the Gelvan are long-lived scholars that begin as hand-made oracles and scribes to mortals who ascended and became deities. They are an advanced civilization with eerie powers rooted in the power of words - and all other manner of mysticism.

---

---
+![VISIONARY]

# VISIONARY

Some races were created with clear goals and purpose in mind; These are known as **VISIONARY** races.

## SOVALIN

Beautiful beings often mistaken for the paranormal - Sovalin are spirits that were unable or unwilling to ascend to the realm of ->SOVALAH<- after death, and were born again into a mortal host. They possess spectral features and are sometimes mistakenly the root of mythos like angels, liches, and even demons.

## UNDINE

Beautiful, female water spirits that became the root of mythos for mermaids and siren. The undine are a fish-like race that draw power from the realm of ->AESORIA<-.

## SHIFT

Transforming from man to beast, the Shift are blessed by the goddess ->MAGNAGEIDA<- with the power of the realm's animals. Humanoid in nature, they transform rapidly into impressive forms both beautiful and monstrous.

## LIFEDRINKER

The pursuit of immortality has created a race that is neither living nor dead. The Lifedrinker subsist on the energy, soul, blood, and mana of others by constantly pulling it from the world around them in secret - or plain sight, if they choose to be gruesome. These denizens of the dusk are considered very feared.

---

---
+![ANCIENT]

# ANCIENT

The most ancient and legendary of all races were the ones created at the conception of time and destiny; Hand crafted by the **EOSH**, the creators of the universe and time itself. These are the **ANCIENT** races.

## MAKKAR

Created by the world tree, ->AUDRASSIL<-, these stout - short in stature and frequently bearded muscle workers are handy with all crafts. The dwarf-sized Makkar were made to be the construction force of the creation engine.

<++>
component: alert
color: red
text: |
    MAKKAR ARE STILL UNDER CONSTRUCTION AS A RACIAL TEMPLATE. PLEASE SELECT 'EARTHBOUND' IN THE MEANTIME TO PLAY ONE.
<++>

## ESPER

Constructed golems built by mortal hands; The original Esper was the world tree ->AUDRASSIL<- itself - but such splendor has been lost to time. These automatons are unparalleled in complex glyph work.

## CHLORYLL

Cultivated plant-like beings fashioned by the original Eosh and the Makkar. They are typically kind hearted and shy, powerful, and wise.

## ROCKBORNE

Between gargoyle and automaton - the ancient Rockborne are sentient effigies that turn to and from stone and move like lightning. They possess grand wings and bat-like traits.

---
+![INVITED]

# INVITED

Other races do not originate from **MIDGROUND**, and are therefore invited in by denizens, deities, and guardians.

## HAULA'FIN

The diminutive Haula'fin know little about their origins; They were once from a realm called ->HASPIRIN<-, but knowledge of how they arrived in ->MIDGROUND<- is scarce. They are under `1 meter` in height and often rather pudgy, obsessed with food, and very flighty. Regarding the tall races as *"Cloudbreathers"*, Haula'fin are **insanely** intense artisans at pretty much any trade they adopt - due to an innate curious nature.

## SPRITE

The small, minute Sprites are light in the darkness - the result of twilight that created darkness begat the realm with radiance. Thought to hail from the stars themselves, Sprites are the mite-sized and possess insect-like wings. They are so fast and small that some cultures do not even know they exist.

---

---
+![DARUKAI]

# DARUKAI

The `DARUKAI` races are various assortments of beast-like humanoids that populate the `MIDGROUND` under a unified banner.

## EARTHBOUND

All manner of humanoid life with animal properties that has yet to be fully researched or discovered its own unique culture is classified as ->EARTHBOUND<-. These are beast-men of any kind, and possess fascinating traits.

## GREENSKIN

Leathery brutes with muscle and blood that oozes power. Greenskin are rich in culture and fueled by strength, divided up into tribes. They are considered the most powerful of all races, physically.

## HISSITH

Formed from the forgotten times, the Hissith are spires of Serpentine power; Snakes that have managed to adopt human traits, such as arms and speech. Massive and strong, they have a long history of mystic studies and are often sage-like.

## RAJIRA

The panther-like Rajira are large cats that stand upright and use arms like humans. Wild with deep seeded feral culture and unknown pasts, they have adopted lives of wisdom and patience. They are frequently diplomats and hunters.

## GOLIATH

Monsters of every kind; Originally molded from the ->GREENSKIN<- and ->ROCKBORNE<-, the Goliath are hulking terrors of muscle and strength. Gorgeous, fantastic species of all kinds - from leathery brutes to pachyderms and mollusks; They are chimeric in many ways, and rival the Esper or Greenskin in physical strength.

## BRAHMATHIAN

Male-steer that walk upright with extra sets of feet, the Brahmathian are muscular monsters that gallop and protect. Blazing fast and guardians to the ->NI'VANA<-, they are created from the mud and given life. They are considered typically hostile, but civilized.

## NI'VANA

Originally earth elementals, the goat-like Ni'vana are faun-shaped researchers and keepers of lore, secrets, and culture. They are considered endangered, and are often guarded by the ->BRAHMATHIAN<-. It is unknown why, but they are the only race that can breathe life into mud to create Brahmathian.

---

---
+![FORMS]

# FORMS

Beyond races, it is possible to extend some of these races with **FORMS**. These are like extensions of races, and are optional. Every player is able to pick `1` for free at any time - additional ones must be earned within the game.

- ->AEROK<-
    - A nomadic race of bird people from lost civilizations in the sky.
- ->VULPIRIN<-
    - Fox-like spirits that possess intense mystical or elemental power.
- ->SILENE<-
    - Very human-like entities with the features of small cats that possess extremely unique kinds of souls.
- ->UNIRI<-
    - Rarely seen, but recorded male ->UNDINE<- usually have legs instead of tails, but glossy blue skin with fish-like heads.
- ->LONGJAW<-
    - Crocodilan races of unknown origin seem to greatly resemble the ->HISSITH<-. They are violent and hated across the world - but recently a sect has been reformed and begun to join main society.
- ->GLADEHOOF<-
    - Mystical male and female Brahmathian that appear more horse-like than as bovine.

---






